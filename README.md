# App

This application contains a number of data vizualizations based on NBA data. For each game there are charts showing when each player was on the court overlayed with point differential throughout the game. Shot charts showing where each player / team was credited with a shot, assist, or turnover. As well as some statistics from the game parsed from play by play data not available in the typical box score.

In addition to the game pages there are pages with vizualizations for: aggregate player rotations for the entire season, the distribution of time it take for a player to shoot after a possession change (as well as how efficiently they score for each time increment), and how they score by play type relative to the rest of the league.

All data has been scraped from the stats.nba.com API, but is store in this repository to remove the need to reduntantly hit the API each time the database is built.

The app is not currently being hosted publicly. Closer to the season I plan to bring it back online [nbaviz.ninja](http://www.nbaviz.ninja).

As an example, the final game of the 2019 NBA Finals

![2019 NBA Finals](https://i.imgur.com/NFhLQXQ.png)

Scoring comparison by play type

![Scoring Compairson](https://i.imgur.com/MhACKuS.png)


# Technology Stack

PostgreSQL 9.6 Database

Python 3.6 - Flask API

NodeJs 11.14

Angular 7 + Angular Material + D3js

## Postgres DB

The Python Flask API will require a local postgres database named `nba_db`

## Python Flask API

In the `api` directory:

The required python modules are listed in the `requirements.txt`. To install with pip run `pip install -r requirements.txt`. You may also choose to create a virtualenv for the project or use pipenv with the included `Pipfile`.

Once the database has been created and the necessary packages have been installed run `flask db update` to update the database with the necessary schema.

Once the requirements are installed and the database is set up, run `python app.py` to run the flask app (port 5000). Once started the application will start creating the necessary data and adding it to the database (this process will take some time to complete).

## Angular Web App

In the `web` directory:

Run `npm install` to install the required node modules

Run `npm install -g @angular\cli` to install the angular cli. With that you can run `ng serve` to run the web app. The web app will run on port 4200. Navigate to `localhost:4200` to see the web app in your browser.

# Docker

Rather than installing all of the dependencies, you can run the app with Docker Compose by simply running `docker-compose up`

# Kubernetes

Kubernetes deployments, services, and secrets files exist in the `kubernetes` directory and will allow the application to be deployed to a Kubernetes cluster.

## Resources

[Google Cloud SDK](https://cloud.google.com/sdk/docs/quickstarts) 
[KubeCtl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
[Google Kubernetes Engine](https://cloud.google.com/kubernetes-engine/)