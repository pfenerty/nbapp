gcloud config set project nbaviz-219205
gcloud config set compute/zone us-central1-a

gcloud container clusters create nbapp --num-nodes=2

kubectl create -f secrets.yaml
kubectl create -f db-deployment.yaml
kubectl create -f db-service.yaml
kubectl create -f api-deployment.yaml
kubectl create -f api-service.yaml
kubectl create -f web-deployment.yaml
kubectl create -f web-service.yaml
