#!/usr/bin/env bash
sleep 20
pipenv run flask db upgrade
pipenv run gunicorn -b :5000 app:app --timeout=500
