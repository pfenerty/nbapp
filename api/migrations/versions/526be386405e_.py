"""empty message

Revision ID: 526be386405e
Revises: 0c9c8b4879c2
Create Date: 2019-10-07 04:03:08.591922

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '526be386405e'
down_revision = '0c9c8b4879c2'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('game', sa.Column('season', sa.Integer(), nullable=True))
    op.add_column('game', sa.Column('season_type', sa.String(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('game', 'season_type')
    op.drop_column('game', 'season')
    # ### end Alembic commands ###
