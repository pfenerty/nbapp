import os
from typing import List

import pandas as pd

from app.database.util import get_season_game_id_prefix
from app.models import GameEvent


def get_type_stats(df: pd.DataFrame, event_type: str, event_types: List[str]):
    team_type_df = df[df['previousPossessionEnd'].isin(event_types)]
    return {
        f'{event_type}_avg_time': team_type_df['timeSincePossChange'].mean(),
        f'{event_type}_eff': team_type_df['scoreChange'].abs().mean(),
        f'{event_type}_pct': len(team_type_df) / len(df) * 100
    }


def get_range_stats(df: pd.DataFrame, range_name: str, range_range: List[int]):
    range_df = df[(df['timeSincePossChange'] >= range_range[0]) & (df['timeSincePossChange'] <= range_range[1])]
    return {
        f'{range_name}_shots': len(range_df),
        f'{range_name}_pct_shots': len(range_df) / len(df),
        f'{range_name}_eff': range_df['scoreChange'].abs().mean()
    }


def get_shot_time_report(season_str: str):
    season_type = 'Regular Season'

    season_prefix = get_season_game_id_prefix(season_str, season_type)
    teams = [g.serialize['team'] for g in GameEvent.query.distinct(GameEvent.offensive_team).all() if
             g.serialize['team'] is not None]
    event_types = ['Made Shot', 'Rebound', 'Turnover']
    live_ball_event_types = ['Live Rebound', 'Live Ball Tov']
    dead_ball_event_types = ['Made Shot', 'Team Rebound', 'Period Start', 'Dead Ball Tov']

    file_location = os.path.dirname(os.path.realpath(__file__)) + f'/data/shots.csv'

    query = GameEvent.query.filter(
        (
                (GameEvent.game_id >= season_prefix * 100000) & (GameEvent.game_id < (season_prefix + 1) * 100000) &
                ((GameEvent.type == 'shot') | (GameEvent.type == 'ft')) &
                (GameEvent.is_after_oreb == False) & (GameEvent.is_after_timeout == False)
        )
    )
    shots = pd.DataFrame([s.serialize for s in query.all()])

    shots_df = pd.DataFrame(shots)
    # df.to_csv(file_location)

    # df = pd.read_csv(file_location)

    # teams_data = list()
    # for team in teams:
    #     team_df = df[df['team'] == team]
    #     team_data = dict()
    #
    #     team_data['team'] = team
    #     team_data['overall_avg_time'] = team_df['timeSincePossChange'].mean()
    #     team_data['overall_eff'] = team_df['scoreChange'].abs().mean()
    #     team_data = {**team_data, **get_type_stats(team_df, 'live_ball', live_ball_event_types)}
    #     team_data = {**team_data, **get_type_stats(team_df, 'dead_ball', dead_ball_event_types)}
    #
    #     teams_data.append(team_data)
    #
    # teams_df = pd.DataFrame(teams_data).sort_values(by='overall_avg_time')
    # teams_df = teams_df.round(2)
    # teams_df.to_csv(os.path.dirname(os.path.realpath(__file__)) + f'/data/teams_shots.csv')

    players = shots_df['player1'].unique()
    granular_ranges = {
        'super_early': [0, 2],
        'very_early': [3, 6],
        'early': [7, 9],
        'average': [10, 17],
        'late': [17, 21],
        'very_late': [21, 24]
    }
    coarse_ranges = {
        'early': [0, 9],
        'average': [10, 17],
        'late': [17, 24]
    }
    ranges_data = []
    for p in players:
        player_df = shots_df[shots_df['player1'] == p]
        player_data = {
            'player': p,
            'season': season_str
        }
        for r in granular_ranges:
            range_dict = get_range_stats(player_df, r, granular_ranges[r])
            player_data = {**player_data, **range_dict}
        ranges_data.append(player_data)

    return pd.DataFrame(ranges_data).sort_values(by='early_shots', ascending=False).round(2)


if __name__ == "__main__":
    df = pd.DataFrame()
    for season in range(2016, 2019):
        df = df.append(get_shot_time_report(str(season)))
    df.to_csv(os.path.dirname(os.path.realpath(__file__)) + f'/data/coarse.csv')
