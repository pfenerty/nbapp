import math
from typing import List, Dict

import pandas as pd

from app.models import PlayType


def get_similarities_for_season(season: int) -> None:
    query = PlayType.query.filter(
        PlayType.season == season
    )
    query_results: List[Dict] = [x.serialize for x in query.all()]
    df: pd.DataFrame = pd.DataFrame(query_results)

    play_types: List[str] = df['play_type'].unique()

    player_poss_df = df.groupby('player_name').sum()[['poss']]
    players: List[str] = player_poss_df[player_poss_df['poss'] >= 500].index.to_list()

    player_diff_data: List[Dict] = []

    for i in range(0, len(players)):
        for j in range(i + 1, len(players)):
            player1: str = players[i]
            player2: str = players[j]

            player1_df: pd.DataFrame = df[df['player_name'] == player1]
            player2_df: pd.DataFrame = df[df['player_name'] == player2]

            diff: float = 0

            for play_type in play_types:
                player1_pt_df = player1_df[player1_df['play_type'] == play_type]
                player2_pt_df = player2_df[player2_df['play_type'] == play_type]

                if len(player1_pt_df) == 0 and len(player2_pt_df) == 0:
                    continue
                elif len(player1_pt_df) == 0:
                    diff += player2_pt_df.iloc[0]['poss_pct']
                elif len(player2_pt_df) == 0:
                    diff += player1_pt_df.iloc[0]['poss_pct']
                else:
                    player1_freq: float = player1_pt_df.iloc[0]['poss_pct']
                    player2_freq: float = player2_pt_df.iloc[0]['poss_pct']

                    player1_ppp: float = player1_pt_df.iloc[0]['ppp']
                    player2_ppp: float = player2_pt_df.iloc[0]['ppp']

                    diff += math.sqrt((player1_freq - player2_freq)**2 + (player1_ppp - player2_ppp)**2)

            player_diff_data.append(
                {
                    'player1': player1,
                    'player2': player2,
                    'diff': diff
                }
            )

    player_diff_df: pd.DataFrame = pd.DataFrame(player_diff_data).sort_values(by='diff', ascending=True)
    player_diff_df.to_csv('player_diff.csv')
    player_diff_df = pd.read_csv('player_diff.csv')

    uniqueness_data: List[Dict] = list()
    for player in players:
        player_df = player_diff_df[(player_diff_df['player1'] == player) | (player_diff_df['player2'] == player)]
        smallest_diff = player_df['diff'].min()
        total_diff = player_df['diff'].sum()
        uniqueness_data.append(
            {
                'player': player,
                'smallest_diff': smallest_diff,
                'total_diff': total_diff
            }
        )

    uniqueness_df: pd.DataFrame = pd.DataFrame(uniqueness_data).sort_values(by='smallest_diff', ascending=False)
    uniqueness_df.to_csv('player_uniqueness.csv')


if __name__ == "__main__":
    get_similarities_for_season(2018)
