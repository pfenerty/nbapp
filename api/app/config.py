import os


class Config(object):
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevConfig(Config):
    DB_NAME = 'nba_db'
    DB_USER = 'postgres'
    DB_PASS = 'postgres'
    SQLALCHEMY_DATABASE_URI = 'postgresql://{0}:{1}@localhost:5432/{2}'.format(DB_USER, DB_PASS, DB_NAME)


class DockerConfig(Config):
    SECRET_KEY = os.environ.get('SECRET_KEY')
    DB_NAME = os.environ.get('POSTGRES_DB')
    DB_USER = os.environ.get('POSTGRES_USER')
    DB_PASS = os.environ.get('POSTGRES_PASSWORD')
    DB_SERVICE = os.environ.get('DB_SERVICE')
    DB_PORT = os.environ.get('DB_PORT')
    SQLALCHEMY_DATABASE_URI = 'postgresql://{0}:{1}@{2}:{3}/{4}'.format(DB_USER, DB_PASS, DB_SERVICE, DB_PORT, DB_NAME)
