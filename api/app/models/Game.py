from app import db


class Game(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    date = db.Column(db.Date)
    home = db.Column(db.String(3))
    away = db.Column(db.String(3))
    home_score = db.Column(db.Integer)
    away_score = db.Column(db.Integer)
    season = db.Column(db.Integer)
    season_type = db.Column(db.String)

    @property
    def serialize(self):
        return {
            'id': self.id,
            'date': self.date,
            'home': self.home,
            'away': self.away,
            'home_score': self.home_score,
            'away_score': self.away_score,
            'season': self.season,
            'season_type': self.season_type
        }
