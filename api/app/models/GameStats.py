from sqlalchemy.orm import column_property

from app import db


class GameStats(db.Model):
    game_id = db.Column(db.Integer, primary_key=True)
    player = db.Column(db.String, primary_key=True)
    team = db.Column(db.String, primary_key=True)
    is_team = db.Column(db.Boolean)
    seconds_played = db.Column(db.Float)
    points = db.Column(db.Integer)
    fg2a = db.Column(db.Integer)
    fg2m = db.Column(db.Integer)
    fg3a = db.Column(db.Integer)
    fg3m = db.Column(db.Integer)
    fta = db.Column(db.Integer)
    ftm = db.Column(db.Integer)
    and_one = db.Column(db.Integer)
    two_pt_fouls = db.Column(db.Integer)
    three_pt_fouls = db.Column(db.Integer)
    efg = db.Column(db.Float)
    ts_pct = db.Column(db.Float)
    ast = db.Column(db.Integer)
    ast_pts = db.Column(db.Integer)
    reb = db.Column(db.Integer)
    oreb = db.Column(db.Integer)
    dreb = db.Column(db.Integer)
    tov = db.Column(db.Integer)
    stl = db.Column(db.Integer)
    blk = db.Column(db.Integer)
    oposs = db.Column(db.Integer)
    opoints = db.Column(db.Integer)
    ortg = db.Column(db.Float)
    dposs = db.Column(db.Integer)
    dpoints = db.Column(db.Integer)
    drtg = db.Column(db.Float)
    season = column_property(db.cast((game_id % 10000000 / 100000), db.String))

    @property
    def serialize(self):
        return {
            'player': self.player,
            'team': self.team,
            'game_id': self.game_id,
            'season': self.season,
            'is_team': self.is_team,
            'seconds_played': self.seconds_played,
            'scoring': {
                'points': self.points,
                'fg2a': self.fg2a,
                'fg2m': self.fg2m,
                'fg3a': self.fg3a,
                'fg3m': self.fg3m,
                'fta': self.fta,
                'ftm': self.ftm,
                'and_ones': self.and_one,
                'two_pt_fouls': self.two_pt_fouls,
                'three_pt_fouls': self.three_pt_fouls,
                'efg': self.efg,
                'ts_pct': self.ts_pct
            },
            'passing': {
                'assists': self.ast,
                'players_assisted': [],
                'ast_pts': self.ast_pts
            },
            'rebounding': {
                'total': self.reb,
                'off': self.oreb,
                'def': self.dreb
            },
            'tov': self.tov,
            'stl': self.stl,
            'blk': self.blk,
            'poss': {
                'off': {
                    'poss': self.oposs,
                    'points': self.opoints,
                    'rtg': self.ortg
                },
                'def': {
                    'poss': self.dposs,
                    'points': self.dpoints,
                    'rtg': self.drtg
                },
            }
        }

    @property
    def serialize_menu(self):
        return {
            'player': self.player,
            'team': self.team,
            'season': int(self.season)
        }
