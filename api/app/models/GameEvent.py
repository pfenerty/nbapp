from app import db


class GameEvent(db.Model):
    game_id = db.Column(db.BigInteger, primary_key=True)
    event_id = db.Column(db.Integer, primary_key=True)
    season = db.Column(db.Integer)
    season_type = db.Column(db.String)

    possession = db.Column(db.BigInteger)
    player1 = db.Column(db.String)
    player2 = db.Column(db.String)
    player3 = db.Column(db.String)
    offensive_team = db.Column(db.String(3))
    defensive_team = db.Column(db.String(3))
    type = db.Column(db.String)
    x = db.Column(db.Integer)
    y = db.Column(db.Integer)
    zone = db.Column(db.String)
    time = db.Column(db.Float)
    score_diff = db.Column(db.Integer)
    score_change = db.Column(db.Integer)
    off_player1 = db.Column(db.String)
    off_player2 = db.Column(db.String)
    off_player3 = db.Column(db.String)
    off_player4 = db.Column(db.String)
    off_player5 = db.Column(db.String)
    def_player1 = db.Column(db.String)
    def_player2 = db.Column(db.String)
    def_player3 = db.Column(db.String)
    def_player4 = db.Column(db.String)
    def_player5 = db.Column(db.String)

    shot_value = db.Column(db.Integer)

    fts_taken = db.Column(db.Integer)
    fts_made = db.Column(db.Integer)

    is_oreb = db.Column(db.Boolean)

    tov_type = db.Column(db.String)

    time_since_possession_change = db.Column(db.Integer)

    is_after_oreb = db.Column(db.Boolean)
    time_since_oreb = db.Column(db.Integer)

    is_after_timeout = db.Column(db.Boolean)

    previous_possession_end = db.Column(db.String)

    @property
    def serialize(self):
        return {
            'game_id': self.game_id,
            'event_id': self.event_id,
            'possession': self.possession,
            'player1': self.player1,
            'player2': self.player2,
            'player3': self.player3,
            'offensive_team': self.offensive_team,
            'defensive_team': self.defensive_team,
            'type': self.type,
            'x': self.x,
            'y': self.y,
            'zone': self.zone,
            'time': self.time,
            'score_change': self.score_change,
            'score_diff': self.score_diff,
            'off_player1': self.off_player1,
            'off_player2': self.off_player2,
            'off_player3': self.off_player3,
            'off_player4': self.off_player4,
            'off_player5': self.off_player5,
            'def_player1': self.def_player1,
            'def_player2': self.def_player2,
            'def_player3': self.def_player3,
            'def_player4': self.def_player4,
            'def_player5': self.def_player5,
            'shot_value': self.shot_value,
            'fts_made': self.fts_made,
            'fts_taken': self.fts_taken,
            'is_oreb': self.is_oreb,
            'tov_type': self.tov_type,
            'time_since_possession_change': self.time_since_possession_change,
            'is_after_oreb': self.is_after_oreb,
            'time_since_oreb': self.time_since_oreb,
            'is_after_timeout': self.is_after_timeout,
            'previous_possession_end': self.previous_possession_end
        }

    @property
    def serialize_menu(self):
        return {
            'player': self.player1,
            'team': self.offensive_team,
            'season': self.season,
            'season_type': self.season_type
        }
