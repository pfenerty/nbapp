from app import db
from typing import Dict


class PlayType(db.Model):
    player_id = db.Column(db.BigInteger, primary_key=True)
    player_name = db.Column(db.String)
    play_type = db.Column(db.String, primary_key=True)
    season = db.Column(db.Integer, primary_key=True)
    team_abbreviation = db.Column(db.String, primary_key=True)
    efg_pct = db.Column(db.Float)
    fga = db.Column(db.Integer)
    fgm = db.Column(db.Integer)
    fg_pct = db.Column(db.Float)
    ft_poss_pct = db.Column(db.Float)
    gp = db.Column(db.Integer)
    percentile = db.Column(db.Float)
    plusone_poss_pct = db.Column(db.Float)
    poss = db.Column(db.Integer)
    poss_pct = db.Column(db.Float)
    ppp = db.Column(db.Float)
    pts = db.Column(db.Integer)
    score_poss_pct = db.Column(db.Float)
    season_id = db.Column(db.Integer)
    sf_poss_pct = db.Column(db.Float)
    team_id = db.Column(db.BigInteger)
    team_name = db.Column(db.String)
    tov_poss_pct = db.Column(db.Float)
    type_grouping = db.Column(db.String)

    @property
    def serialize(self) -> Dict:
        return {
            'player_id': self.player_id,
            'player_name': self.player_name,
            'team': self.team_abbreviation,
            'season': self.season,
            'play_type': self.play_type,
            'efg_pct': self.efg_pct,
            'fga': self.fga,
            'fgm': self.fgm,
            'percentile': self.percentile,
            'poss': self.poss,
            'poss_pct': self.poss_pct,
            'ppp': self.ppp,
            'pts': self.pts,
            'type_grouping': self.type_grouping
        }
