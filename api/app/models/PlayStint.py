from app import db


class PlayerStint(db.Model):
    game_id = db.Column(db.BigInteger, primary_key=True)
    season = db.Column(db.Integer)
    season_type = db.Column(db.String)
    sub_id = db.Column(db.Integer, primary_key=True)
    player = db.Column(db.String)
    team = db.Column(db.String)
    start = db.Column(db.Float)
    end = db.Column(db.Float)
    score_change = db.Column(db.Integer)

    @property
    def serialize(self):
        return {
            'game_id': self.game_id,
            'player': self.player,
            'team': self.team,
            'start': self.start,
            'end': self.end,
            'score_change': self.score_change
        }

    @property
    def serialize_menu(self):
        return {
            'player': self.player,
            'team': self.team,
            'season': self.season,
            'season_type': self.season_type
        }