from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_apscheduler.scheduler import BackgroundScheduler
import os
import datetime
import logging

from app.config import DevConfig, DockerConfig

app = Flask(__name__)
app.config.from_object(DockerConfig if os.environ.get('CONFIG') == 'docker' else DevConfig)

logging.basicConfig(format='%(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

db = SQLAlchemy(app)
migrate = Migrate(app, db)

CORS(app)

from app import models
from app.routes import menu

from app.database.update_db import initialize_pbp, update_pbp
from app.database.update_play_type_db import initialize_play_type, update_play_type

scheduler = BackgroundScheduler()

if os.environ.get('CONFIG') == 'docker':
    now = datetime.datetime.utcnow()
else:
    now = datetime.datetime.now()
end = now + datetime.timedelta(days=0, seconds=40)
scheduler.add_job(initialize_pbp, trigger='interval', seconds=30, start_date=now, end_date=end)
scheduler.add_job(initialize_play_type, trigger='interval', seconds=30, start_date=now, end_date=end)
scheduler.add_job(update_pbp, trigger='cron', hour=8, minute=20)
scheduler.add_job(update_play_type, trigger='cron', hour=9, minute=00)
scheduler.start()
