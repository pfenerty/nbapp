from typing import Any, List

import pandas as pd
from flask import request, jsonify

from app import app
from app.models import PlayerStint

from app.routes.util import convert_time, get_index, last_names

import logging

logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True


@app.route('/api/rotations', methods=['POST'])
def get_rotations() -> Any:
    post = request.get_json()
    season: str = post.get('season')
    season_type: str = post.get('seasonType')
    team: str = post.get('team')
    game_ids: List = post.get('gameIds')

    query = PlayerStint.query.filter(
                        (PlayerStint.game_id.in_(game_ids)) & (PlayerStint.team == team) & (PlayerStint.start < 2880))

    return jsonify([r.serialize for r in query.all()])


@app.route('/api/starters', methods=['POST'])
def get_team_starters():
    post = request.get_json()
    season = post['season']
    season_type = post['seasonType']
    team = post['team']

    starting_stints_query = PlayerStint.query.filter(
        (PlayerStint.season == season) & (PlayerStint.season_type == season_type) & (
                PlayerStint.team == team) & (PlayerStint.start == 0)
    )
    starting_stints = [s.serialize for s in starting_stints_query.all()]
    game_ids = list(set([s['game_id'] for s in starting_stints]))
    starting_lineups = dict()
    for g in game_ids:
        starters = [s['player'] for s in starting_stints if s['game_id'] == g]
        starters.sort()
        starters = str(starters)
        if starters in list(starting_lineups.keys()):
            starting_lineups[starters].append(g)
        else:
            starting_lineups[starters] = [g]
    return jsonify([
        {'players': s.replace('[', '').replace(']', '').replace('"', '').replace('\'', '').split(', '),
         'games': starting_lineups[s]}
        for s in starting_lineups])


def get_team_rotations_for_game(pbp, team, game_id):
    rotations = list()
    for period in range(0, len(pbp)):
        period_pbp = pbp[period]['pla']
        if len(period_pbp) > 0:
            try:
                rotations.extend(get_rotations_for_team_period(period_pbp, team, period + 1))
            except ValueError as e:
                logging.debug("{} in {}'s period {} game {}".format(e, team, period, game_id))
    return rotations


def get_rotations_for_team_period(pbp, team, period):
    pbp_df = pd.DataFrame(pbp)

    starters = get_starters(pbp_df, team)
    subs = get_team_subs(pbp_df, team)
    rotations = list()
    player_in_time = dict()

    end_time = convert_time(pbp_df.iloc[-1]['cl'], period)

    for starter in starters:
        if period > 4:
            player_in_time[starter] = convert_time('5:00', period)
        else:
            player_in_time[starter] = convert_time('12:00', period)

    for ix, sub in subs.iterrows():
        [player_out, player_in] = get_players_out_in(sub)
        try:
            rotations.append({
                'player': player_out,
                'start': player_in_time[player_out],
                'end': convert_time(sub.cl, period),
                'team': team
            })
        except KeyError as e:
            continue
        del player_in_time[player_out]
        player_in_time[player_in] = convert_time(sub.cl, period)

    for player in player_in_time.keys():
        rotations.append({
            'player': player,
            'start': player_in_time[player],
            'team': team,
            'end': end_time
        })

    return rotations


def get_starters(pbp_df, team):
    team_df = pbp_df[pbp_df['de'].str.contains(team)]

    starters = list()
    subs = list()
    for ix, play in team_df.iterrows():
        try:
            player = play['de'].split('] ')[1].split(' ')[0]
        except IndexError:
            continue
        possible_last_name = play['de'].split('] ')[1].split(' ')[1]
        if get_index(last_names, possible_last_name) >= 0:
            player = player + ' ' + possible_last_name
        if 'Technical' in play['de']:
            continue
        if 'Substitution' in play['de']:
            sub = play['de'].split('replaced by ')[1]
            subs.append(sub)
        if get_index(starters, player) < 0 and player != 'Team' and get_index(subs, player) < 0:
            starters.append(player)
        if len(starters) == 5:
            return starters

    return starters


def get_players_out_in(sub_row):
    [player_out, player_in] = sub_row.de.split(' Substitution replaced by ')
    player_out = player_out.split('] ')[1]
    return [player_out, player_in]


def get_all_player_names(df, team):
    df = df[df['de'].str.contains(team)]
    players = list()
    for ix, row in df.iterrows():
        player = row.de.split(']')[1].split(' ')[1]
        if player != 'Team':
            players.append(player)
    return list(set(players))


def get_team_subs(df, team):
    df = df[df['de'].str.contains(team)]
    return df[df['de'].str.contains('Substitution')]


def is_starter(subs_in, subs_out, player):
    in_index = get_index(subs_in, player)
    out_index = get_index(subs_out, player)
    return (in_index < 0) or (out_index < in_index)
