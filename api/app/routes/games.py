import requests
from flask import jsonify, request
from typing import Any

from app import app
from app.database.util import data_headers
from app.models import Game


@app.route('/api/games/<date>')
def get_games(date):
    url = 'http://data.nba.net/prod/v1/{}/scoreboard.json'.format(date)
    r = requests.get(url, headers=data_headers)
    json_response = r.json()
    return jsonify(json_response)


@app.route('/api/games/search', methods=['POST'])
def search_games() -> Any:
    post = request.get_json()['teamSeason']
    season: str = post.get('year')
    season_type: str = post.get('type')
    team: str = post.get('team')
    opponent: str = post.get('opponent')

    query = Game.query.filter(
        (Game.season == season) &
        (Game.season_type == season_type) &
        ((Game.home == team) | (Game.away == team))
    )

    if opponent is not None:
        query = query.filter(
            (Game.home == opponent) | (Game.away == opponent)
        )

    query = query.order_by(Game.date)

    return jsonify([game.serialize for game in query.all()])
