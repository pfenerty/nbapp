from flask import jsonify, request

from app import app
from app.models import GameEvent, Game, PlayerStint


@app.route('/api/menu/seasons')
def get_seasons():
    return jsonify([x.serialize['season'] for x in Game.query.distinct(Game.season)])


@app.route('/api/menu/teams', methods=['POST'])
def get_teams():
    post = request.get_json()
    season = str(post['season'])
    season_type = post['seasonType']
    query = Game.query.distinct(Game.home, Game.away).filter(
        (Game.season == season) & (Game.season_type == season_type)).all()
    home_teams = [x.serialize['home'] for x in query]
    away_teams = [x.serialize['away'] for x in query]
    return jsonify(sorted(list(set((home_teams + away_teams)))))


@app.route('/api/menu/players', methods=['POST'])
def get_players():
    post = request.get_json()
    season = str(post['season'])
    season_type = post['seasonType']
    team = post['team']
    query = PlayerStint.query.distinct(PlayerStint.player).filter(
        (PlayerStint.season == season) & (PlayerStint.season_type == season_type) & (PlayerStint.team == team))
    return jsonify(sorted([x.serialize_menu['player'] for x in query if x.serialize_menu['player'] != team]))
