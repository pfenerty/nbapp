from app import app
from typing import Any
from flask import request, jsonify
from app.models import PlayType


@app.route('/api/play-type/player', methods=['POST'])
def get_play_type_stats_for_player_season() -> Any:
    post = request.get_json()
    player: str = post.get('player')
    season: str = post.get('season')
    query = PlayType.query.filter(
        (PlayType.player_name == player) & (PlayType.season == season)
    )
    return jsonify([x.serialize for x in query.all()])


@app.route('/api/play-type/season', methods=['POST'])
def get_play_type_stats_for_season() -> Any:
    post = request.get_json()
    season: str = post.get('season')
    query = PlayType.query.filter(
        (PlayType.season == season)
    )
    return jsonify([x.serialize for x in query.all()])


@app.route('/api/play-type/all', methods=['POST'])
def get_all_play_type_stats() -> Any:
    query = PlayType.query.filter()
    return jsonify([x.serialize for x in query.all()])

