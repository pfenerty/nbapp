import pandas as pd
from flask import request, jsonify

from app import app
from app.models import GameEvent
from app.database.util import get_season_game_id_prefix


@app.route('/api/shots', methods=['POST'])
def get_shots():
    post = request.get_json()
    season = post.get('season')
    season_type = post.get('seasonType')
    team = post.get('team')
    player = post.get('player')

    season_prefix = get_season_game_id_prefix(season, season_type)
    query = GameEvent.query.filter(
        (GameEvent.game_id >= season_prefix * 100000) & (GameEvent.game_id < (season_prefix + 1) * 100000) & (
                (GameEvent.offensive_team != team) & (GameEvent.player3 == player) | (
                (GameEvent.offensive_team == team) & ((GameEvent.player1 == player) | (GameEvent.player2 == player)))) & (
                GameEvent.type == 'shot'))

    return jsonify([s.serialize for s in query.all()])


@app.route('/api/shots/time', methods=['POST'])
def get_shot_timing():
    post = request.get_json()
    season = post.get('season')
    season_type = post.get('seasonType')
    team = post.get('team')
    player = post.get('player')
    live_ball = post.get('liveBall')
    dead_ball = post.get('deadBall')

    season_prefix = get_season_game_id_prefix(season, season_type)
    query = GameEvent.query.filter(
        (
                (GameEvent.game_id >= season_prefix * 100000) & (GameEvent.game_id < (season_prefix + 1) * 100000) &
                ((GameEvent.type == 'shot') | (GameEvent.type == 'ft')) & (GameEvent.is_after_oreb == False) &
                (GameEvent.is_after_timeout == False)
        )
    )

    if live_ball and not dead_ball:
        query = query.filter(GameEvent.previous_possession_end.in_(['Live Rebound', 'Live Ball Tov']))
    elif not live_ball and dead_ball:
        query = query.filter(GameEvent.previous_possession_end.in_(['Made Shot', 'Team Rebound', 'Period Start', 'Dead Ball Tov']))

    if team:
        query = query.filter(GameEvent.offensive_team == team)

    if player:
        query = query.filter(GameEvent.player1 == player)

    shots = pd.DataFrame([s.serialize for s in query.all()])
    shots['score_change'] = shots['score_change'].abs()

    volume_data = shots.groupby(['time_since_possession_change']).size().to_dict()
    eff_data = shots.groupby(['time_since_possession_change']).mean()[['score_change']].to_dict()

    return jsonify({
        'volume': volume_data,
        'efficiency': eff_data['score_change']
    })