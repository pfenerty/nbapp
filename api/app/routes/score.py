from app.routes.util import convert_time
import pandas as pd


def get_score_changes_for_game(events):
    scores = list()
    scores.append({
        'score': 0,
        'time': 0
    })
    for period in range(0, len(pbp)):
        period_pbp = pbp[period]['pla']
        if len(period_pbp) > 0:
            scores.extend(get_score_changes_for_period(period_pbp, period))
    return scores


def get_score_changes_for_period(pbp, period):
    pbp_df = pd.DataFrame(pbp)
    current_score = 0
    score_data = list()
    for ix, row in pbp_df.iterrows():
        new_score = row['hs'] - row['vs']
        if current_score != new_score:
            score_data.append({
                'score': -new_score,
                'time': convert_time(row['cl'], period + 1)
            })
            current_score = new_score
    score_data.append({
        'score': -current_score,
        'time': convert_time(pbp_df.iloc[-1]['cl'], period + 1)
    })
    return score_data
