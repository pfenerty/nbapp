
def get_possession_stats_for_team(events, team):
    team_events = list(filter(lambda x: x['offensive_team'] == team, events))

    num_possessions = len(set(map(lambda x: x['possession'], team_events)))

    shots = list(filter(lambda x: x['type'] == 'shot', team_events))

    fg3 = list(filter(lambda x: x['zone'] == 'Three', shots))
    num_fg3a = len(fg3)
    num_fg3m = len(list(filter(lambda x: x['shot_value'] > 0, fg3)))

    fg2 = list(filter(lambda x: x['zone'] != 'Three', shots))
    num_fg2a = len(fg2)
    num_fg2m = len(list(filter(lambda x: x['shot_value'] > 0, fg2)))

    shooting_fouls = list(filter(lambda x: x['type'] == 'ft', team_events))
    num_fts_taken = sum(foul['fts_taken'] for foul in shooting_fouls)
    num_fts_made = sum(foul['fts_made'] for foul in shooting_fouls)

    turnovers = len(list(filter(lambda x: x['type'] == 'tov', team_events)))

    rebs = list(filter(lambda x: x['type'] == 'reb', team_events))
    orebs = len(list(filter(lambda x: x['is_oreb'], rebs)))

    return {
        'team': team,
        'possessions': num_possessions,
        'fg3': {
            'makes': num_fg3m,
            'attempts': num_fg3a,
            'pct': 0 if num_fg3a == 0 else num_fg3m / num_fg3a,
            'eff': 0 if num_fg3a == 0 else num_fg3m / num_fg3a * 1.5
        },
        'fg2': {
            'makes': num_fg2m,
            'attempts': num_fg2a,
            'pct': 0 if num_fg2a == 0 else num_fg2m / num_fg2a,
            'eff': 0 if num_fg2a == 0 else num_fg2m / num_fg2a
        },
        'ft': {
            'trips': len(shooting_fouls),
            'attempts': num_fts_taken,
            'makes': num_fts_made,
            'pct': 0 if num_fts_taken == 0 else num_fts_made / num_fts_taken,
            'eff': 0 if len(shooting_fouls) == 0 else num_fts_made / len(shooting_fouls)
        },
        'turnovers': turnovers,
        'orebs': orebs
    }
