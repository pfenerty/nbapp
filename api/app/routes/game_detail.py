from flask import jsonify

from app import app
from app.models import GameEvent, PlayerStint
from app.routes.events import get_events_for_game_live
from app.routes.stats import get_player_stats, get_minutes
from app.routes.lineups import get_lineups

import math


@app.route('/api/gameDetail/<game_id>/<home>/<away>')
def get_game(game_id, home, away):
    e = get_events_for_game_db(game_id)
    if len(e['events']) == 0 or len(e['rotations']) == 0:
        e = get_events_for_game_live(game_id, home, away)

    events = e['events']
    rotations = e['rotations']

    scores = sorted([{'score': -e['score_diff'], 'time': e['time']} for e in events if e['score_change'] != 0],
                    key=lambda s: s['time'])
    scores.append({'score': scores[-1]['score'], 'time': rotations[-1]['end']})

    stats = get_player_stats(events, rotations)
    stats = get_minutes(stats, rotations)

    raw_time = int(max([r['end'] for r in e['rotations']]))
    scoreboard_time = get_scoreboard_time(
        int(max([r['end'] for r in e['rotations']]))
    )

    data = {
        'home': home,
        'away': away,
        'rotations': rotations,
        'scores': scores,
        'secondsPlayed': raw_time,
        'scoreboard': {
            'home': {
                'name': home,
                'score': abs(sum([e['score_change'] for e in events if
                                  (e['offensive_team'] == home and e['score_change'] is not None)]))
            },
            'away': {
                'name': away,
                'score': abs(sum([e['score_change'] for e in events if
                                  (e['offensive_team'] == away and e['score_change'] is not None)]))
            },
            'time': scoreboard_time
        },
        'events': events,
        'stats': {
            'home': [s for s in stats if s['team'] == home],
            'away': [s for s in stats if s['team'] == away]
        },
    }
    return jsonify(data)


@app.route('/api/lineups/<game_id>/<home>/<away>')
def get_lineup_data(game_id, home, away):
    e = get_events_for_game_db(game_id)
    if len(e['events']) == 0 or len(e['rotations']) == 0:
        e = get_events_for_game_live(game_id, home, away)
    events = e['events']
    lineups = get_lineups(events, home)
    lineups.extend(get_lineups(events, away))
    return jsonify(lineups)


def get_events_for_game_db(game_id):
    events = [x.serialize for x in GameEvent.query.filter_by(game_id=game_id).all()]
    rotations = [r.serialize for r in PlayerStint.query.filter_by(game_id=game_id).all()]
    return {
        'events': events,
        'rotations': rotations
    }


def get_scoreboard_time(last_time: int) -> str:
    if last_time < 2880:
        time_display = get_time_display(last_time, False)
        return f'Q{time_display}'
    elif last_time == 2880:
        return 'FINAL'
    else:
        time_past_regulation = last_time - 2880
        time_display = get_time_display(time_past_regulation, True)
        return f'OT{time_display}'


def get_time_display(time: int, is_overtime: bool) -> str:
    period_length = 300 if is_overtime else 720
    period = math.ceil(time / period_length)
    time_elapsed = time - (period_length * (period - 1))
    time_remaining = period_length - time_elapsed
    if time_remaining != 0:
        minutes_remaining = math.floor(time_remaining / 60)
        seconds_remaining = int(time_remaining - (minutes_remaining * 60))
        return f'{period} - {minutes_remaining}:{seconds_remaining}'
    else:
        return f'{period} - FINAL' if is_overtime else f'{period} - END'
