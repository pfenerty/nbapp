import pandas as pd

from app.database.nba_api import get_game_play_by_play_v1
from app.routes.util import get_player_name, get_assister_name, convert_time, get_play_zone, get_tov_type,\
    get_period_end
from app.routes.rotations import get_starters, get_players_out_in
from app.models import GameEvent, PlayerStint
from typing import List, Dict
import logging

logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)

eventmsgtype = 'etype'
event_num = 'evt'

eventmsgtype_map: Dict[int, str] = {
    1: 'Made Shot',
    2: 'Missed Shot',
    3: 'Made Shot',
    4: 'Rebound',
    5: 'Turnover',
    6: 'Foul',
    7: 'Violation',
    8: 'Substitution',
    9: 'Timeout',
    10: 'Jump Ball',
    11: 'Ejection',
    12: 'Period Start',
    13: 'End Period'
}


def get_events_for_game_live(game_id, home, away):
    pbp: pd.DataFrame = get_game_play_by_play_v1(game_id, file_override=True, file_write=False)
    game_events: List[GameEvent] = list()
    rotations: List[PlayerStint] = list()
    for period in range(0, len(pbp)):
        period_pbp = pbp[period]['pla']
        if len(period_pbp) > 0:
            period_df = pd.DataFrame(period_pbp)
            [period_events, period_player_stints] = get_events_for_period(period_df, game_id, period + 1, home, away)
            game_events.extend(period_events)
            rotations.extend(period_player_stints)
    return {
        'events': game_events,
        'rotations': rotations
    }


def get_shot_value(play: pd.Series) -> int:
    if play[eventmsgtype] == 1:
        zone = get_play_zone(play)
        return 3 if zone == 'Three' else 2
    elif play[eventmsgtype] == 2:
        return 0
    else:
        return 0


def get_play_description(play: pd.Series) -> str:
    return play['de']


def get_events_for_period(period_df: pd.DataFrame, game_id: int, period: int, home: str, away: str) -> List[List[Dict]]:
    events: List[Dict] = list()
    player_stints: List[Dict] = list()

    period_df['time'] = period_df['cl'].apply(lambda x: convert_time(x, period))
    period_df = period_df[(period_df[eventmsgtype] <= 9) & (period_df[eventmsgtype] > 0)]

    # free throws used for linking to shots / fouls
    free_throws = period_df[period_df[eventmsgtype] == 3]
    # used for linking and ones to shots
    single_free_throws = free_throws[free_throws['de'].str.contains('of 1')]
    # used for linking together ft trips
    multi_free_throws = free_throws[
        (~free_throws['de'].str.contains('of 1')) & (~free_throws['de'].str.contains('Technical'))]
    # linked to ft trip to get location
    fouls = period_df[period_df[eventmsgtype] == 6]

    period_start_time = convert_time('12:00', period) if period <= 4 else convert_time('5:00', period)

    # initialize loop variables

    # used to track team changes, when team differs from one play to the next (of certain types) a new
    # possession is triggered
    previous_team: str = ''
    # used to group plays into a possession. will be equal to the game id + the event number of the
    # first play of the possession
    possession: int = 0
    # used to track previous event for live ball vs dead ball
    previous_play: Dict = {
        'time': period_start_time,
        eventmsgtype: 12
    }
    previous_poss_end_type: str = eventmsgtype_map[period_df.iloc[0][eventmsgtype]]
    # used for tracking time since possession changed
    poss_start_time: int = 0
    is_after_oreb: int = False
    # used for tracking time since offensive rebound
    last_oreb_time: int = 0
    is_after_timeout: bool = False
    sub_num: int = 0
    score_diff: int = 0

    home_players_on_court: Dict = dict()
    away_players_on_court: Dict = dict()
    for player in get_starters(period_df, home):
        home_players_on_court[player] = {
            'time': period_start_time,
            'score': score_diff
        }
    for player in get_starters(period_df, away):
        away_players_on_court[player] = {
            'time': period_start_time,
            'score': -score_diff
        }

    for ix, play in period_df.iterrows():

        score_diff: int = int(play['hs']) - int(play['vs'])

        # skip fouls and substitutions
        if play[eventmsgtype] == 7 or play[eventmsgtype] == 6:
            continue

        # if play is substitution, update players on court, add player stint
        if play[eventmsgtype] == 8:
            # [player_out, player_in] = [play['PLAYER1_NAME'], play['PLAYER2_NAME']]
            # sub_team: str = play['PLAYER1_TEAM_ABBREVIATION']
            [player_out, player_in] = get_players_out_in(play)
            sub_team = get_play_team(play)
            player_list: Dict = home_players_on_court if sub_team == home else away_players_on_court
            try:
                player_stints.append(
                    dict(
                        game_id=int(game_id),
                        sub_id=int(str(period) + str(sub_num)),
                        player=player_out,
                        team=sub_team,
                        start=player_list[player_out]['time'],
                        score_change=player_list[player_out]['score'] - (
                            score_diff if sub_team == home else - score_diff),
                        end=play['time']
                    )
                )
            except KeyError as e:
                logging.error(e)
                continue
            del player_list[player_out]
            player_list[player_in] = {'time': play['time'], 'score': score_diff}
            sub_num += 1
            continue

        # if play is timeout, set after timeout flag, skip to next play
        if play[eventmsgtype] == 9:
            is_after_timeout: bool = True
            continue

        current_team: str = get_play_team(play)
        # in the case of team rebounds, PLAYER1_TEAM_ABBREVIATION is not set, so read it from v1 description
        if current_team is None:
            current_team: str = play['de'][1:4]

        # when the team has changed start a new possession
        if previous_team != current_team:
            possession = int(str(game_id) + str(play[event_num]))
            # if the play is a rebound, start the new possession at the time of the rebound
            if play[eventmsgtype] == 4:
                poss_start_time: str = play['time']
                # in the case of team rebounds, PLAYER1_TEAM_ABBREVIATION is not set, so read it from v1
                # description
                if get_player_name(play) is not None:
                    previous_poss_end_type: str = 'Live Rebound'
                # team rebounds are those that go out of bounds to the defensive team, dead ball
                else:
                    previous_poss_end_type: str = 'Team Rebound'
            # if the play is not a rebound, start the new possession at the time of the previous event
            else:
                poss_start_time: int = previous_play['time']
                # logging put backs can often be messy in terms of order
                # if the previous play was a rebound, we need to look for the shot because that is really
                # the previous event
                if previous_play[eventmsgtype] == 4:
                    linked_shots: pd.DataFrame = period_df[(period_df['time'] == previous_play['time'])]
                    linked_made_shots: pd.DataFrame = linked_shots[linked_shots[eventmsgtype] == 1]
                    # if a made shot is found, log previous event as made shot
                    if len(linked_made_shots) >= 1:
                        previous_poss_end_type: str = 'Made Shot'
                    else:
                        # TODO: it makes no sense for a made shot not to be found, update logic
                        if get_player_name(previous_play) is not None:
                            previous_poss_end_type: str = 'Live Rebound'
                        else:
                            previous_poss_end_type: str = 'Team Rebound'
                # if the previous play is a missed shot, log as live rebound
                elif previous_play[eventmsgtype] == 2:
                    previous_poss_end_type: str = 'Live Rebound'
                # if the previous play is a turnover, determine live or dead ball turnover
                elif previous_play[eventmsgtype] == 5:
                    if 'Steal' in previous_play['de']:
                        previous_poss_end_type: str = 'Live Ball Tov'
                    else:
                        previous_poss_end_type: str = 'Dead Ball Tov'
                else:
                    previous_poss_end_type: str = eventmsgtype_map[previous_play[eventmsgtype]]
            # reset after oreb and after timeout flags
            is_after_oreb: bool = False
            is_after_timeout: bool = False

        # update timing variables
        time_since_poss_change: int = play['time'] - poss_start_time
        time_since_oreb: int = (play['time'] - last_oreb_time) if is_after_oreb else None

        # update players on court
        home_players: List = list(home_players_on_court.keys())
        home_players.sort()
        away_players: List = list(away_players_on_court.keys())
        away_players.sort()
        while len(home_players) < 5:
            home_players.append(None)
        while len(away_players) < 5:
            away_players.append(None)

        off_players = home_players if current_team == home else away_players
        def_players = away_players if current_team == home else home_players

        # set common event fields
        event: Dict = dict(
            game_id=int(game_id),
            event_id=int(play[eventmsgtype]),
            possession=possession,
            player1=get_player_name(play),
            player2=None,
            player3=None,
            offensive_team=current_team,
            defensive_team=home if home != current_team else away,
            time=play['time'],
            off_player1=off_players[0],
            off_player2=off_players[1],
            off_player3=off_players[2],
            off_player4=off_players[3],
            off_player5=off_players[4],
            def_player1=def_players[0],
            def_player2=def_players[1],
            def_player3=def_players[2],
            def_player4=def_players[3],
            def_player5=def_players[4],
            time_since_possession_change=time_since_poss_change,
            is_after_oreb=is_after_oreb,
            time_since_oreb=time_since_oreb,
            is_after_timeout=is_after_timeout,
            previous_possession_end=previous_poss_end_type,
            zone=get_play_zone(play),
            x=play['locX'],
            y=play['locY'],
            score_diff=score_diff,
            score_change=0,
            and_one_value=None
        )

        # if play is a made shot
        if play[eventmsgtype] == 1:
            linked_free_throws: pd.DataFrame = single_free_throws[single_free_throws['time'] == play['time']]
            is_and_one: bool = len(linked_free_throws) == 1
            and_one_val = None
            if is_and_one:
                and_one_free_throw: pd.Series = linked_free_throws.iloc[0]
                and_one_val: int = 0 if 'Missed' in get_play_description(and_one_free_throw) else 1
            elif len(linked_free_throws) > 1:
                logging.debug("MORE THAN ONE AND ONE FREE THROW FOUND")
                logging.debug(linked_free_throws)

            event['and_one_value'] = and_one_val
            event['fts_made'] = and_one_val if and_one_val else 0
            event['fts_taken'] = 1 if is_and_one else 0
            event['type'] = 'shot'

            shot_value: int = get_shot_value(play)
            event['shot_value'] = shot_value

            score_change: int = shot_value + (0 if and_one_val is None else and_one_val)
            event['score_change'] = score_change if current_team == home else -score_change
            event['player2'] = get_assister_name(play)

        # if play is a missed shot
        elif play[eventmsgtype] == 2:
            event['shot_value'] = 0
            event['score_change'] = 0
            event['type'] = 'shot'
            event['fts_taken'] = 0
            event['fts_made'] = 0

        # if play is free throw
        elif play[eventmsgtype] == 3:
            # find associated shooting foul to record location
            linked_fouls: pd.DataFrame = fouls[fouls['time'] == play['time']]
            linked_shooting_fouls: List[Dict] = [x for x in linked_fouls.to_dict('records') if
                                                 'Shooting' in get_play_description(x)]
            linked_free_throws: pd.DataFrame = multi_free_throws[multi_free_throws['time'] == play['time']]
            fts_made: int = int(
                len([x for x in linked_free_throws.to_dict('records') if 'Missed' not in get_play_description(x)]))
            # if free throw is a technical set specific field values
            if 'Technical' in get_play_description(play):
                foul_x: int = -1000
                foul_y: int = -1000
                foul_zone: str = 'Technical'
                fts_made: int = 0 if 'Missed' in get_play_description(play) else 1
                fts_taken: int = 1
            # if not a technical and only 1 free throw, it is an and one which is associated with its shot
            elif 'of 1' in get_play_description(play):
                continue
            # free throws are linked together, add event on first free throw, link others, skip others when
            # they come up
            elif '1 of' not in get_play_description(play):
                continue
            # if shooting foul is found, use its location for the event
            elif len(linked_shooting_fouls) >= 1:
                linked_foul: Dict = linked_shooting_fouls[0]
                foul_x: int = int(linked_foul['locX'])
                foul_y: int = int(linked_foul['locY'])
                foul_zone: str = get_play_zone(linked_foul)
                fts_taken: int = len(linked_free_throws)
            # if none of the above are met, the free throws come from a personal foul in bonus
            else:
                foul_x: int = -1000
                foul_y: int = -1000
                foul_zone: str = 'Personal'
                fts_taken: int = len(linked_free_throws)

            event['type'] = 'ft'
            event['x'] = foul_x
            event['y'] = foul_y
            event['zone'] = foul_zone
            event['fts_taken'] = fts_taken
            event['fts_made'] = fts_made
            event['score_change'] = fts_made if current_team == home else -fts_made

        # if play is a rebound
        elif play[eventmsgtype] == 4:
            rebounder: str = get_player_name(play)
            if rebounder is None:
                # team rebounds are often recorded after a missed free throw that is not the last
                # free throw of the set. these plays are meaningless and should be ignored
                linked_free_throws: pd.DataFrame = multi_free_throws[multi_free_throws['time'] == play['time']]
                if len(linked_free_throws) > 1:
                    linked_free_throws_event_ids: List[int] = linked_free_throws[event_num]
                    play_evt: int = int(play[event_num])
                    if any(evt < play_evt for evt in linked_free_throws_event_ids) and \
                            any(evt > play_evt for evt in linked_free_throws_event_ids):
                        continue

            event['type'] = 'reb'
            is_after_oreb = previous_team == current_team
            event['is_oreb'] = is_after_oreb
            if is_after_oreb:
                last_oreb_time = play['time']

        # if play is a turnover
        elif play[eventmsgtype] == 5:
            event['type'] = 'tov'
            if 'Steal' in play['de']:
                event['player2'] = play['de'].split('Steal:')[1].split(' ')[0]
            event['tov_type'] = get_tov_type(get_play_description(play))

        events.append(event)
        previous_team: str = current_team
        previous_play: Dict = play

    # for players remaining on court at end of period, add player stint
    last_recorded_time = period_df.iloc[-1]['time']
    period_end = get_period_end(last_recorded_time)
    rotation_end_time = period_end if period_end - last_recorded_time < 60 else last_recorded_time
    for player in home_players_on_court:
        player_stints.append(
            dict(
                game_id=int(game_id),
                sub_id=int(str(period) + str(sub_num)),
                player=player,
                team=home,
                start=home_players_on_court[player]['time'],
                end=rotation_end_time,
                score_change=home_players_on_court[player]['score'] - score_diff,
            )
        )
        sub_num += 1
    for player in away_players_on_court:
        player_stints.append(
            dict(
                game_id=int(game_id),
                sub_id=int(str(period) + str(sub_num)),
                player=player,
                team=away,
                start=away_players_on_court[player]['time'],
                end=rotation_end_time,
                score_change=away_players_on_court[player]['score'] + score_diff,
            )
        )
        sub_num += 1

    return [events, player_stints]


def get_event_id(game_id, play) -> int:
    return int(str(game_id) + str(play['evt']))


def get_play_team(play) -> str:
    return play['de'][1:4]


def get_stealer(play):
    if 'Steal' not in play['de']:
        return None
    else:
        return play['de'].split('Steal:')[1].split('(')[0].strip()
