from flask import request, jsonify

from app.routes.util import format_year_string
from app.models import GameStats
from sqlalchemy import func
from app import db, app


@app.route('/api/game_by_game', methods=['POST'])
def get_game_by_game():
    post = request.get_json()
    season = post.get('season')
    season_type = post.get('seasonType')
    num_players = post.get('numPlayers')
    team = post.get('team')
    stat = post.get('stat')
    is_team = post.get('isTeam')

    stat_lists = get_stat_lists(season, season_type, num_players, team, stat, is_team)

    return jsonify(stat_lists)


def get_stat_lists(season, season_type, num_players, team, stat, is_team):
    season_ending = format_year_string(season)[2:4]
    season_prefix = int(('2' if season_type == 'Regular Season' else '1') + season_ending)

    column = GameStats.team if is_team else GameStats.player

    column_map = {
        'pts': GameStats.points,
        'ast': GameStats.ast,
        'ast_pts': GameStats.ast_pts,
        'reb': GameStats.reb,
        'oreb': GameStats.oreb,
        'dreb': GameStats.dreb,
        'tov': GameStats.tov,
        'stl': GameStats.stl,
        'blk': GameStats.blk,
        'fg3a': GameStats.fg3a,
        'fg3m': GameStats.fg3m,
        'fg3_pct': GameStats.fg3m * 100 / GameStats.fg3a,
        'fg2a': GameStats.fg2a,
        'fg2m': GameStats.fg2m,
        'fg2_pct': GameStats.fg2m * 100 / GameStats.fg2m,
        'fta': GameStats.fta,
        'ftm': GameStats.ftm,
        'ft_pct': GameStats.ftm * 100 / GameStats.fta,
        'ft2a': GameStats.two_pt_fouls,
        'ft3a': GameStats.three_pt_fouls,
        'poss': (GameStats.oposs + GameStats.dposs) / 2,
        'min': GameStats.seconds_played / 60,
        'plus_minus': GameStats.opoints - GameStats.dpoints,
        'ortg': GameStats.ortg,
        'drtg': GameStats.drtg,
        'net_rtg': GameStats.ortg - GameStats.drtg
    }

    func_map = {
        'avg': func.avg,
        'sum': func.sum
    }

    stats = db.session.query(column, func_map['avg'](column_map[stat])).filter(
        (GameStats.game_id >= season_prefix * 100000) & (
                GameStats.game_id < (season_prefix + 1) * 100000) & (GameStats.is_team == is_team))

    if team is not '':
        stats = stats.filter(GameStats.team == team)

    stats = stats.group_by(column)

    qualified_columns = [c[0] for c in
                         db.session.query(GameStats.player, func.count()).filter(
                             (GameStats.game_id >= season_prefix * 100000) & (
                                     GameStats.game_id < (season_prefix + 1) * 100000) & (
                                         GameStats.is_team == is_team)).group_by(GameStats.player).all()
                         if c[1] > 50]

    top_columns = sorted([c for c in stats.all() if c[0] in qualified_columns], key=lambda x: x[1], reverse=stat != 'drtg')[
                  0: num_players]

    data = list()
    for c in [p[0] for p in top_columns]:
        stat_list = db.session.query(column_map[stat]).filter(
            (GameStats.game_id >= season_prefix * 100000) & (
                    GameStats.game_id < (season_prefix + 1) * 100000) & (
                    column == c) & (GameStats.is_team == is_team))

        if team is not '':
            stat_list = stat_list.filter(GameStats.team == team)

        stat_list = stat_list.all()

        t = db.session.query(GameStats.team).filter(
            (GameStats.game_id >= season_prefix * 100000) & (
                    GameStats.game_id < (season_prefix + 1) * 100000) & (
                    column == c) & (GameStats.is_team == is_team)).first() if team is '' else team

        stat_list = [int(p[0]) for p in stat_list]

        data.append({
            'column': c,
            'team': t,
            'list': stat_list
        })

    return data
