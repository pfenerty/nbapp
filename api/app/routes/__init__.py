from app.routes.games import get_games, search_games
from app.routes.game_detail import get_game
from app.routes.game_by_game import get_game_by_game
from app.routes.menu import get_players, get_teams, get_seasons
from app.routes.rotations import get_rotations, get_starters
from app.routes.shots import get_shots, get_shot_timing
from app.routes.play_type import get_play_type_stats_for_player_season, get_play_type_stats_for_season
