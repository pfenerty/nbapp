from math import sqrt, ceil
import pandas as pd

from app.models import GameEvent
from app.database.util import format_year_string


def convert_time(time_string, period):
    [minutes, seconds] = time_string.split(':')
    if period <= 4:
        period_end_time = 720 * period
    else:
        period_end_time = 2880 + ((period - 4) * 300)
    time = period_end_time - (int(minutes) * 60 + float(seconds))
    return time


def get_period_end(last_time):
    if last_time <= 2880:
        period = ceil(last_time / 720)
        return period * 720
    else:
        last_time = last_time - 2880
        period = ceil(last_time / 300)
        return 2880 + (period * 300)


last_names = ["Morris", "Jr.", "Hill", "III", "Jackson", "Simmons", "Dragic"]


def get_player_name(play):
    try:
        first_name = play['de'].split('] ')[1].split(' ')[0]
        last_name = play['de'].split('] ')[1].split(' ')[1]
        if get_index(last_names, last_name) >= 0:
            return first_name + ' ' + last_name
        else:
            return first_name
    except IndexError as e:
        return ''


def get_assister_name(play):
    try:
        first_name = play['de'].split('Assist: ')[1].split(' ')[0]
    except IndexError as e:
        return None
    last_name = play['de'].split('Assist: ')[1].split(' ')[1]
    if get_index(last_names, last_name) >= 0:
        return first_name + ' ' + last_name
    else:
        return first_name


def get_index(item_list, item):
    try:
        return item_list.index(item)
    except ValueError:
        return -1


def get_play_zone(play):
    dist = sqrt((play['locX'] * play['locX']) + (play['locY'] * play['locY'])) / 10
    desc = get_play_description(play)
    if dist <= 4:
        return 'Rim'
    elif dist <= 13.75:
        return 'ShortMid'
    elif play['locY'] > 470:
        return 'BackCourt'
    elif '3pt Shot' in play['de'] or '3 FTA' in play['de'] or '3PT' in desc:
        return 'Three'
    else:
        return 'LongMid'


def get_play_description(play: pd.Series) -> str:
    try:
        home_desc = play['HOMEDESCRIPTION'] or ''
        away_desc = play['VISITORDESCRIPTION'] or ''
        is_home: bool = home_desc > away_desc
        return home_desc if is_home else away_desc
    except Exception as e:
        return ''


def get_tov_type(play_description: str):
    if 'Lost Ball' in play_description:
        return 'lost ball'
    elif 'Bad Pass' in play_description:
        return 'bad pass'
    elif 'Offensive  Foul' in play_description:
        return 'off foul'
    elif 'Travelling' in play_description:
        return 'travel'
    elif '3 Second Violation' in play_description:
        return 'three sec'
    else:
        return None


def get_game_ids_for_season(season: str, season_type: str):
    season_ending = format_year_string(season)[2:4]
    season_prefix = int(('2' if season_type == 'Regular Season' else '1') + season_ending)
    return [g.serialize['gameId'] for g in
            GameEvent.query.distinct(GameEvent.game_id).filter(
                (GameEvent.game_id >= season_prefix * 100000) & (GameEvent.game_id < (season_prefix + 1) * 100000))]


def get_game_ids_for_team_season(season: str, season_type: str, team: str):
    season_ending = format_year_string(season)[2:4]
    season_prefix = int(('2' if season_type == 'Regular Season' else '1') + season_ending)
    return [g.serialize['gameId'] for g in
            GameEvent.query.distinct(GameEvent.game_id).filter(
                (GameEvent.game_id >= season_prefix * 100000) & (GameEvent.game_id < (season_prefix + 1) * 100000) & (
                        GameEvent.offensive_team == team))]


def get_game_ids_for_player_season(season: str, season_type: str, player: str):
    season_ending = format_year_string(season)[2:4]
    season_prefix = int(('2' if season_type == 'Regular Season' else '1') + season_ending)
    return [g.serialize['gameId'] for g in
            GameEvent.query.distinct(GameEvent.game_id).filter(
                (GameEvent.game_id >= season_prefix * 100000) & (GameEvent.game_id < (season_prefix + 1) * 100000) & ((
                            GameEvent.player1 == player) | (GameEvent.player2 == player)))]


play_type_season_type_map = {
    'Regular Season': 'Reg',
    'Playoffs': 'Post'
}