import math
from typing import List, Dict


def get_player_stats(events: List[Dict], player_stints: List[Dict], for_db=False) -> List[Dict]:
    players = get_players(events)
    player_stat_list = list()
    for p in players:
        shots = [e for e in events if
                 e['type'] == 'shot' and
                 e['player1'] == p['name'] and
                 e['offensive_team'] == p['team']]
        fouls_drawn = [e for e in events if
                       e['type'] == 'ft' and
                       e['player1'] == p['name'] and
                       e['offensive_team'] == p['team']]
        assists = [e for e in events if
                   e['type'] == 'shot' and
                   e['player2'] == p['name'] and
                   e['offensive_team'] == p['team']]
        rebounds = [e for e in events if
                    e['type'] == 'reb' and
                    e['player1'] == p['name'] and
                    e['offensive_team'] == p['team']]

        steals = [e for e in events if
                  e['type'] == 'tov' and
                  e['player2'] == p['name'] and
                  e['defensive_team'] == p['team']]

        tov = [e for e in events if
               e['type'] == 'tov' and
               e['player1'] == p['name'] and
               e['offensive_team'] == p['team']]

        blocks = [e for e in events if
                  e['type'] == 'shot' and
                  e['player3'] == p['name'] and
                  e['defensive_team'] == p['team']]

        points_on_shots = sum([s['shot_value'] for s in shots])

        points_on_ft = sum(f['fts_made'] for f in fouls_drawn)
        points_on_ft += sum(f['fts_made'] for f in shots)
        total_points = points_on_shots + points_on_ft

        poss = [e for e in events if player_is_in(p, e)]
        oposs = [e for e in poss if e['offensive_team'] == p['team']]
        dposs = [e for e in poss if e['defensive_team'] == p['team']]

        num_oposs = len(set([e['possession'] for e in oposs]))
        num_dposs = len(set([e['possession'] for e in dposs]))

        num_opoints = abs(sum(e['score_change'] if e['score_change'] is not None else 0 for e in oposs))
        num_dpoints = abs(sum(e['score_change'] if e['score_change'] is not None else 0 for e in dposs))

        assisted_players = list(map(lambda x: x['player1'], assists))
        assisted_players = [{'player': x, 'assists': assisted_players.count(x)} for x in set(assisted_players)]

        player_rotations = [r for r in player_stints if r['player'] == p['name'] and r['team'] == p['team']]
        player_total_seconds_played = 0
        for r in player_rotations:
            player_total_seconds_played += r['end'] - r['start']

        player_stats = dict(
            player=p['name'],
            is_team=False,
            team=p['team'],
            game_id=events[0]['game_id'],
            seconds_played=player_total_seconds_played,
            points=total_points,
            fg2a=len([s for s in shots if s['zone'] != 'Three']),
            fg2m=len([s for s in shots if s['zone'] != 'Three' and s['shot_value'] > 0]),
            fg3a=len([s for s in shots if s['zone'] == 'Three']),
            fg3m=len([s for s in shots if s['zone'] == 'Three' and s['shot_value'] > 0]),
            fta=sum([f['fts_taken'] for f in fouls_drawn]) + sum([s['fts_taken'] for s in shots]),
            ftm=sum([f['fts_made'] for f in fouls_drawn]) + sum([s['fts_made'] for s in shots]),
            and_one=sum([s['fts_taken'] for s in shots]),
            two_pt_fouls=len([f for f in fouls_drawn if f['fts_taken'] == 2]),
            three_pt_fouls=len([f for f in fouls_drawn if f['fts_taken'] == 3]),
            efg=round(points_on_shots / len(shots) * 500) / 10 if len(shots) > 0 else 0,
            ts_pct=round(total_points / (len(fouls_drawn) + len(shots)) * 500) / 10 if (len(fouls_drawn)
                                                                                        + len(shots)) > 0 else 0,
            ast=len(assists),
            ast_pts=sum(abs(a['score_change']) for a in assists),
            reb=len(rebounds),
            oreb=len([r for r in rebounds if r['is_oreb']]),
            dreb=len([r for r in rebounds if not r['is_oreb']]),
            tov=len(tov),
            stl=len(steals),
            blk=len(blocks),
            oposs=num_oposs,
            opoints=num_opoints,
            ortg=0 if num_oposs <= 0 else round(num_opoints / num_oposs * 100, 1),
            dposs=len(set([e['possession'] for e in dposs])),
            dpoints=abs(sum(e['score_change'] if e['score_change'] is not None else 0 for e in dposs)),
            drtg=0 if num_dposs <= 0 else round(num_dpoints / num_dposs * 100, 1)
        )
        if not for_db:
            player_stats['players_assisted'] = assisted_players
        player_stat_list.append(player_stats)

    return player_stat_list


def get_players(events):
    players = list()
    for e in events:
        if not any(p['name'] == e['player1'] and p['team'] == e['offensive_team'] for p in players):
            if e['player1'] is not None:
                players.append({
                    'name': e['player1'],
                    'team': e['offensive_team']
                })
    return players


def get_minutes(stats, rotations):
    for s in stats:
        player_rotations = [r for r in rotations if r['player'] == s['player'] and r['team'] == s['team']]
        player_total_seconds_played = 0
        for r in player_rotations:
            player_total_seconds_played += r['end'] - r['start']
        player_minutes_played = math.floor(player_total_seconds_played / 60)
        player_seconds_played = int(player_total_seconds_played - (player_minutes_played * 60))
        player_seconds_played = str(player_seconds_played) if player_seconds_played >= 10 else '0' + str(
            player_seconds_played)
        player_time_played = str(player_minutes_played) + ':' + str(player_seconds_played)
        s['min'] = player_time_played
    return stats


def player_is_in(player: Dict, event: Dict) -> bool:
    2+2
    if player['team'] == event['offensive_team']:
        return player['name'] == event['off_player1'] or \
                player['name'] == event['off_player2'] or \
                player['name'] == event['off_player3'] or \
                player['name'] == event['off_player4'] or \
                player['name'] == event['off_player5']
    else:
        return player['name'] == event['def_player1'] or \
                player['name'] == event['def_player2'] or \
                player['name'] == event['def_player3'] or \
                player['name'] == event['def_player4'] or \
                player['name'] == event['def_player5']
