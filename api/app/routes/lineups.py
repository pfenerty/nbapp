from typing import Dict, List
import pandas as pd


def get_lineups(events: List[Dict], team: str) -> List[Dict]:
    df = pd.DataFrame(events)

    offensive_df = df[df['offensive_team'] == team]
    offensive_group = offensive_df.groupby(['off_player1', 'off_player2', 'off_player3', 'off_player4', 'off_player5'])
    offensive_possessions = offensive_group.nunique()[['possession']]
    offensive_points = offensive_group.sum()[['score_change']]
    offense = offensive_possessions.join(offensive_points, how='outer')
    offense['score_change'] = offense['score_change'].apply(lambda x: abs(x))
    offense = offense.rename(columns={'possession': 'off_possessions', 'score_change': 'off_points'})
    offense['lineup'] = offense.index

    defensive_df = df[df['defensive_team'] == team]
    defensive_group = defensive_df.groupby(['def_player1', 'def_player2', 'def_player3', 'def_player4', 'def_player5'])
    defensive_possessions = defensive_group.nunique()[['possession']]
    defensive_points = defensive_group.sum()[['score_change']]
    defense = defensive_possessions.join(defensive_points, how='outer')
    defense['score_change'] = defense['score_change'].apply(lambda x: abs(x))
    defense['lineup'] = defense.index
    defense = defense.rename(columns={'possession': 'def_possessions', 'score_change': 'def_points'})

    lineups = pd.merge(offense, defense, on='lineup', how='outer')
    lineups['team'] = team
    lineups = lineups.fillna(0)
    lineups['diff'] = lineups['off_points'] - lineups['def_points']
    return lineups.to_dict(orient='records')
