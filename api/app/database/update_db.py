import json
import os
import re
from typing import List, Dict

import pandas as pd
from sqlalchemy.exc import IntegrityError

from app import db, logging
from app.database.data import file_check
from app.database.nba_api import get_game_play_by_play_v1, get_game_log, get_game_play_by_play_v2, get_shot_chart_detail
from app.database.teams import nickname_map
from app.database.util import format_game_id, format_year_string, get_info_for_game
from app.routes.stats import get_player_stats
from app.routes.util import convert_time, get_play_zone, get_tov_type, get_index, get_period_end

current_season = 2019
all_seasons = list(range(1996, current_season + 1))
all_seasons.reverse()
current_season_type = 'Regular Season'

eventmsgtype_map: Dict[int, str] = {
    1: 'Made Shot',
    2: 'Missed Shot',
    3: 'Made Shot',
    4: 'Rebound',
    5: 'Turnover',
    6: 'Foul',
    7: 'Violation',
    8: 'Substitution',
    9: 'Timeout',
    10: 'Jump Ball',
    11: 'Ejection',
    12: 'Period Start',
    13: 'End Period'
}

eventmsgtype: str = 'EVENTMSGTYPE'


logging.basicConfig(format='%(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)


def merge_play_by_play(v1: pd.DataFrame, v2: pd.DataFrame) -> pd.DataFrame:
    v1: pd.DataFrame = v1[['evt', 'locX', 'locY', 'de', 'hs', 'vs']]
    return v2.merge(v1, left_on='EVENTNUM', right_on='evt')


def update_pbp() -> None:
    logging.info('Getting new games')
    get_data_for_season(current_season, current_season_type)
    save_data_for_season(current_season, current_season_type)


def get_all_games() -> None:
    logging.info('Getting PBP Data for All Games')
    for season in all_seasons:
        for season_type in ['Regular Season', 'Playoffs']:
            get_data_for_season(season, season_type)


def get_data_for_season(season: int, season_type: str) -> None:
    season_str = format_year_string(season)
    logging.info(f'Getting New Games Not On File {season} - {season_type} from API')

    if season == current_season and current_season_type == 'Regular Season' and season_type == 'Playoffs':
        logging.info('Season has not started yet')
        return

    game_log_df: pd.DataFrame = get_game_log(season_str, season_type, season == current_season and season_type == current_season_type)
    try:
        season_game_ids: List[int] = [int(g) for g in game_log_df['GAME_ID'].unique()]
    except KeyError:
        logging.info('No games found')
        return

    current_dir: str = os.path.dirname(os.path.realpath(__file__))

    if season > 2015:
        v1_pbp_dir: str = f'{current_dir}/data/pbp/v1/{season_str}/{season_type}'
        file_check(f'{v1_pbp_dir}/a')
        v1_game_ids_on_file: List[int] = [int(x.split('.')[0]) for x in os.listdir(v1_pbp_dir)]
        new_v1_game_ids: List[int] = [game_id for game_id in season_game_ids if game_id not in v1_game_ids_on_file]
        logging.info(f'{str(len(new_v1_game_ids))} new v1 pbp games found')
        for game_id in new_v1_game_ids:
            game_id_str: str = format_game_id(game_id)
            logging.info(f'Got v1 pbp for {game_id_str}')
            try:
                get_game_play_by_play_v1(game_id_str, True, True)
            except json.decoder.JSONDecodeError as e:
                logging.error(e)
                continue
    else:
        shot_detail_dir: str = f'{current_dir}/data/shot_detail/{season_str}/{season_type}'
        file_check(f'{shot_detail_dir}/a')
        shot_details_games_on_file: List[int] = [int(x.split('.')[0]) for x in os.listdir(shot_detail_dir)]
        new_shot_detail_game_ids: List[int] = [game_id for game_id in season_game_ids
                                               if game_id not in shot_details_games_on_file]
        logging.info(f'{str(len(new_shot_detail_game_ids))} new shot detail games found')
        for game_id in new_shot_detail_game_ids:
            game_id_str: str = format_game_id(game_id)
            logging.info(f'Got shot detail for {game_id_str}')
            get_shot_chart_detail(season_str, season_type, game_id_str)

    v2_pbp_dir: str = f'{current_dir}/data/pbp/v2/{season_str}/{season_type}'
    file_check(f'{v2_pbp_dir}/a')
    v2_game_ids_on_file: List[int] = [int(x.split('.')[0]) for x in os.listdir(v2_pbp_dir)]
    new_v2_game_ids: List[int] = [game_id for game_id in season_game_ids if game_id not in v2_game_ids_on_file]
    logging.info(f'{str(len(new_v2_game_ids))} new v2 pbp games found')
    for game_id in new_v2_game_ids:
        game_id_str: str = format_game_id(game_id)
        logging.info(f'Got v2 pbp for {game_id_str}')
        get_game_play_by_play_v2(game_id_str, True)


def save_all_games() -> None:
    logging.info('Saving PBP Data for All Games')
    for season in all_seasons:
        for season_type in ['Playoffs', 'Regular Season']:
            save_data_for_season(season, season_type)


def get_merged_play_by_play(game_id: int) -> pd.DataFrame:
    game_id_str: str = format_game_id(game_id)
    pbp_v2_df: pd.DataFrame = get_game_play_by_play_v2(game_id_str)
    game_info = get_info_for_game(game_id)

    if int(game_info['season'].split('-')[0]) > 2015:
        try:
            pbp_v1: pd.DataFrame = get_game_play_by_play_v1(game_id_str)
        except json.decoder.JSONDecodeError as e:
            logging.error(e)
            raise json.decoder.JSONDecodeError

        formatted_v1 = list()
        for period in pbp_v1:
            formatted_v1.extend(period['pla'])
        pbp_v1_df = pd.DataFrame(formatted_v1)

        pbp_df = merge_play_by_play(pbp_v1_df, pbp_v2_df)
    else:
        shot_df = get_shot_chart_detail(
            season_year=game_info['season'],
            season_type=game_info['season_type'],
            game_id=game_id_str
        )
        shot_df = shot_df[['GAME_EVENT_ID', 'LOC_X', 'LOC_Y', 'PERIOD']]
        shot_df = shot_df.rename(columns={
            'GAME_EVENT_ID': 'EVENTNUM',
            'LOC_X': 'locX',
            'LOC_Y': 'locY'
        })
        pbp_df = pd.merge(pbp_v2_df, shot_df, on=['EVENTNUM', 'PERIOD'], how='left')
        pbp_df['locX'] = pbp_df['locX'].fillna(0)
        pbp_df['locY'] = pbp_df['locY'].fillna(0)

    return pbp_df.sort_values(by='EVENTNUM', ascending=True)


def get_starters(pbp_df, team) -> List[str]:
    team_df = pbp_df[pbp_df['PLAYER1_TEAM_ABBREVIATION'] == team]

    starters = list()
    subs = list()
    for ix, play in team_df.iterrows():
        player = play['PLAYER1_NAME']
        if player is None:
            continue
        if 'Technical' in play['de']:
            continue
        if play[eventmsgtype] == 8:
            sub = play['PLAYER2_NAME']
            subs.append(sub)
        if get_index(starters, player) < 0 and player != 'Team' and get_index(subs, player) < 0:
            starters.append(player)
        if len(starters) == 5:
            return starters

    return starters


def get_period_df(full_df: pd.DataFrame, period: int) -> pd.DataFrame:
    period_df = full_df[full_df['PERIOD'] == period]
    period_df['time'] = period_df['PCTIMESTRING'].apply(lambda x: convert_time(x, period))
    period_df['de'] = period_df.apply(lambda x: convert_desc(x), axis=1)
    period_df = period_df.sort_values(by=['time', 'EVENTNUM'], ascending=True)
    period_df = period_df[period_df[eventmsgtype] <= 9].reset_index()
    return period_df


def convert_desc(play: pd.Series) -> str:
    home = play['HOMEDESCRIPTION'] or ''
    vis = play['VISITORDESCRIPTION'] or ''
    return home if len(home) > len(vis) else vis


def get_all_players(pbp_df: pd.DataFrame) -> List[Dict[str, str]]:
    df = pbp_df[pbp_df['PLAYER1_NAME'].notnull()][['PLAYER1_NAME', 'PLAYER1_TEAM_ABBREVIATION']].drop_duplicates()
    df = df.rename(columns={
        'PLAYER1_NAME': 'name',
        'PLAYER1_TEAM_ABBREVIATION': 'team'
    })
    return df.to_dict('records')


def get_play_description(play: pd.Series) -> str:
    home_desc = play['HOMEDESCRIPTION'] or ''
    away_desc = play['VISITORDESCRIPTION'] or ''
    is_home: bool = home_desc > away_desc
    return home_desc if is_home else away_desc


def get_shot_value(play: pd.Series) -> int:
    if play[eventmsgtype] == 1:
        zone = get_play_zone(play)
        return 3 if zone == 'Three' else 2
    elif play[eventmsgtype] == 2:
        return 0
    else:
        return 0


def get_events_for_period(period_df: pd.DataFrame, game_id: int, period: int, home: str, away: str) -> List[List[Dict]]:
    events: List[Dict] = list()
    player_stints: List[Dict] = list()

    # free throws used for linking to shots / fouls
    free_throws = period_df[period_df[eventmsgtype] == 3]
    # used for linking and ones to shots
    single_free_throws = free_throws[free_throws['de'].str.contains('of 1')]
    # used for linking together ft trips
    multi_free_throws = free_throws[
        (~free_throws['de'].str.contains('of 1')) & (~free_throws['de'].str.contains('Technical'))]
    # linked to ft trip to get location
    fouls = period_df[period_df[eventmsgtype] == 6]
    rebounds = period_df[period_df[eventmsgtype] == 4]

    period_start_time = convert_time('12:00', period) if period <= 4 else convert_time('5:00', period)

    # initialize loop variables

    # used to track team changes, when team differs from one play to the next (of certain types) a new
    # possession is triggered
    previous_team: str = ''
    # used to group plays into a possession. will be equal to the game id + the event number of the
    # first play of the possession
    possession: int = 0
    # used to track previous event for live ball vs dead ball
    previous_play: Dict = {
        'time': period_start_time,
        eventmsgtype: 12
    }
    try:
        previous_poss_end_type: str = eventmsgtype_map[period_df.iloc[0][eventmsgtype]]
    except IndexError as e:
        return [[], []]

    # used for tracking time since possession changed
    poss_start_time: int = 0
    is_after_oreb: int = False
    # used for tracking time since offensive rebound
    last_oreb_time: int = 0
    is_after_timeout: bool = False
    sub_num: int = 0

    period_df['SCOREMARGIN'] = period_df['SCOREMARGIN'].apply(lambda x: 0 if x == 'TIE' else x)
    first_score = period_df[period_df['SCOREMARGIN'].notnull()].iloc[0]
    first_score_margin = int(first_score['SCOREMARGIN'])
    first_score_val = 1 if first_score[eventmsgtype] else get_shot_value(first_score)
    first_score_val = first_score_val if first_score['PLAYER1_TEAM_ABBREVIATION'] == home else -first_score_val
    score_diff: int = first_score_margin - first_score_val

    home_players_on_court: Dict = dict()
    away_players_on_court: Dict = dict()
    for player in get_starters(period_df, home):
        home_players_on_court[player] = {
            'time': period_start_time,
            'score': score_diff
        }
    for player in get_starters(period_df, away):
        away_players_on_court[player] = {
            'time': period_start_time,
            'score': -score_diff
        }

    game_info = get_info_for_game(game_id)

    for ix, play in period_df.iterrows():

        # skip fouls and substitutions
        if play[eventmsgtype] == 7 or play[eventmsgtype] == 6:
            continue

        # if play is substitution, update players on court, add player stint
        if play[eventmsgtype] == 8:
            [player_out, player_in] = [play['PLAYER1_NAME'], play['PLAYER2_NAME']]
            sub_team: str = play['PLAYER1_TEAM_ABBREVIATION']
            player_list: Dict = home_players_on_court if sub_team == home else away_players_on_court
            try:
                player_stints.append(
                    dict(
                        game_id=int(game_id),
                        sub_id=int(str(period) + str(sub_num)),
                        season=int(game_info['year']),
                        season_type=game_info['season_type'],
                        player=player_out,
                        team=sub_team,
                        start=player_list[player_out]['time'],
                        score_change=player_list[player_out]['score'] - (
                            score_diff if sub_team == home else - score_diff),
                        end=play['time']
                    )
                )
            except KeyError as e:
                logging.error(e)
                continue
            del player_list[player_out]
            player_list[player_in] = {'time': play['time'], 'score': score_diff}
            sub_num += 1
            continue

        # if play is timeout, set after timeout flag, skip to next play
        if play[eventmsgtype] == 9:
            is_after_timeout: bool = True
            continue

        current_team: str = play['PLAYER1_TEAM_ABBREVIATION']
        # in the case of team rebounds, PLAYER1_TEAM_ABBREVIATION is not set, so read it from v1 description
        if current_team is None:
            try:
                regex = re.compile('(.+)( Rebound| Turnover)')
                nickname = regex.search(play['de']).group(1).title()
            except AttributeError as e:
                nickname = ''
            if nickname == 'Hornets':
                current_team = 'NOH' if 2002 < int(game_info['year']) < 2012 else 'CHA'
            else:
                current_team: str = nickname_map[nickname]

        # when the team has changed start a new possession
        if previous_team != current_team:
            possession = int(str(game_id) + str(play['EVENTNUM']))
            # if the play is a rebound, start the new possession at the time of the rebound
            if play[eventmsgtype] == 4:
                poss_start_time: str = play['time']
                # in the case of team rebounds, PLAYER1_TEAM_ABBREVIATION is not set, so read it from v1
                # description
                if play['PLAYER1_NAME'] is not None:
                    previous_poss_end_type: str = 'Live Rebound'
                # team rebounds are those that go out of bounds to the defensive team, dead ball
                else:
                    previous_poss_end_type: str = 'Team Rebound'
            # if the play is not a rebound, start the new possession at the time of the previous event
            else:
                poss_start_time: int = previous_play['time']
                # logging put backs can often be messy in terms of order
                # if the previous play was a rebound, we need to look for the shot because that is really
                # the previous event
                if previous_play[eventmsgtype] == 4:
                    linked_shots: pd.DataFrame = period_df[(period_df['time'] == previous_play['time'])]
                    linked_made_shots: pd.DataFrame = linked_shots[linked_shots['SCORE'].notnull()]
                    # if a made shot is found, log previous event as made shot
                    if len(linked_made_shots) >= 1:
                        previous_poss_end_type: str = 'Made Shot'
                    else:
                        # TODO: it makes no sense for a made shot not to be found, update logic
                        if previous_play['PLAYER1_NAME'] is not None:
                            previous_poss_end_type: str = 'Live Rebound'
                        else:
                            previous_poss_end_type: str = 'Team Rebound'
                # if the previous play is a missed shot, log as live rebound
                elif previous_play[eventmsgtype] == 2:
                    previous_poss_end_type: str = 'Live Rebound'
                # if the previous play is a turnover, determine live or dead ball turnover
                elif previous_play[eventmsgtype] == 5:
                    if 'Steal' in previous_play['de']:
                        previous_poss_end_type: str = 'Live Ball Tov'
                    else:
                        previous_poss_end_type: str = 'Dead Ball Tov'
                else:
                    previous_poss_end_type: str = eventmsgtype_map[previous_play[eventmsgtype]]
            # reset after oreb and after timeout flags
            is_after_oreb: bool = False
            is_after_timeout: bool = False

        # update timing variables
        time_since_poss_change: int = play['time'] - poss_start_time
        time_since_oreb: int = (play['time'] - last_oreb_time) if is_after_oreb else None

        # update players on court
        home_players: List = list(home_players_on_court.keys())
        home_players.sort()
        away_players: List = list(away_players_on_court.keys())
        away_players.sort()
        while len(home_players) < 5:
            home_players.append(None)
        while len(away_players) < 5:
            away_players.append(None)

        # set offensive and defensive players
        off_players = home_players if current_team == home else away_players
        def_players = away_players if current_team == home else home_players

        # set common event fields
        event: Dict = dict(
            game_id=int(game_id),
            event_id=int(play['EVENTNUM']),
            season=int(game_info['year']),
            season_type=game_info['season_type'],
            possession=possession,
            player1=play['PLAYER1_NAME'],
            player2=play['PLAYER2_NAME'],
            player3=play['PLAYER3_NAME'],
            offensive_team=current_team,
            defensive_team=home if home != current_team else away,
            time=play['time'],
            off_player1=off_players[0],
            off_player2=off_players[1],
            off_player3=off_players[2],
            off_player4=off_players[3],
            off_player5=off_players[4],
            def_player1=def_players[0],
            def_player2=def_players[1],
            def_player3=def_players[2],
            def_player4=def_players[3],
            def_player5=def_players[4],
            time_since_possession_change=time_since_poss_change,
            is_after_oreb=is_after_oreb,
            time_since_oreb=time_since_oreb,
            is_after_timeout=is_after_timeout,
            previous_possession_end=previous_poss_end_type,
            zone=get_play_zone(play),
            x=play['locX'],
            y=play['locY'],
            score_diff=score_diff,
            score_change=0
        )

        if possession == 21900001378:
            2+2

        # if play is a made shot
        if play[eventmsgtype] == 1:
            linked_free_throws: pd.DataFrame = single_free_throws[single_free_throws['time'] == play['time']]
            is_and_one: bool = len(linked_free_throws) == 1
            and_one_val: int = int()
            event['fts_taken'] = 0
            event['fts_made'] = 0
            if is_and_one:
                and_one_free_throw: pd.Series = linked_free_throws.iloc[0]
                and_one_val = 0 if 'MISS' in get_play_description(and_one_free_throw) else 1
                event['fts_taken'] = 1
                event['fts_made'] = and_one_val
            elif len(linked_free_throws) > 1:
                logging.debug("MORE THAN ONE AND ONE FREE THROW FOUND")
                logging.debug(linked_free_throws)

            event['type'] = 'shot'
            shot_value: int = get_shot_value(play)
            event['shot_value'] = shot_value
            score_change: int = shot_value + (0 if and_one_val is None else and_one_val)
            score_change = score_change if current_team == home else -score_change
            event['score_change'] = score_change
            score_diff += score_change

        # if play is a missed shot
        elif play[eventmsgtype] == 2:
            event['shot_value'] = 0
            event['score_change'] = 0
            event['fts_taken'] = 0
            event['fts_made'] = 0
            event['type'] = 'shot'

        # if play is free throw
        elif play[eventmsgtype] == 3:
            # find associated shooting foul to record location
            linked_fouls: pd.DataFrame = fouls[fouls['time'] == play['time']]
            linked_shooting_fouls: List[Dict] = [x for x in linked_fouls.to_dict('records') if
                                                 'S.FOUL' in get_play_description(x)]
            linked_free_throws: pd.DataFrame = multi_free_throws[multi_free_throws['time'] == play['time']]
            fts_made: int = int(
                len([x for x in linked_free_throws.to_dict('records') if 'MISS' not in get_play_description(x)]))
            linked_oreb: pd.DataFrame = rebounds[(rebounds['time'] == play['time'])]
            if len(linked_oreb) > 0 and not is_after_oreb:
                event['is_after_oreb'] = True
            # if free throw is a technical set specific field values
            if 'Technical' in get_play_description(play):
                foul_x: int = -1000
                foul_y: int = -1000
                foul_zone: str = 'Technical'
                fts_made: int = 0 if 'MISS' in get_play_description(play) else 1
                fts_taken: int = 1
            # if not a technical and only 1 free throw, it is an and one which is associated with its shot
            elif 'of 1' in get_play_description(play):
                continue
            # free throws are linked together, add event on first free throw, link others, skip others when
            # they come up
            elif '1 of' not in get_play_description(play):
                continue
            # if shooting foul is found, use its location for the event
            elif len(linked_shooting_fouls) >= 1:
                linked_foul: Dict = linked_shooting_fouls[0]
                foul_x: int = int(linked_foul['locX'])
                foul_y: int = int(linked_foul['locY'])
                foul_zone: str = get_play_zone(linked_foul)
                fts_taken: int = len(linked_free_throws)
            # if none of the above are met, the free throws come from a personal foul in bonus
            else:
                foul_x: int = -1000
                foul_y: int = -1000
                foul_zone: str = 'Personal'
                fts_taken: int = len(linked_free_throws)

            event['type'] = 'ft'
            event['x'] = foul_x
            event['y'] = foul_y
            event['zone'] = foul_zone
            event['fts_taken'] = fts_taken
            event['fts_made'] = fts_made
            score_change = fts_made if current_team == home else -fts_made
            event['score_change'] = score_change
            score_diff += score_change

        # if play is a rebound
        elif play[eventmsgtype] == 4:
            rebounder: str = play['PLAYER1_NAME']
            if rebounder is None:
                # team rebounds are often recorded after a missed free throw that is not the last
                # free throw of the set. these plays are meaningless and should be ignored
                linked_free_throws: pd.DataFrame = multi_free_throws[multi_free_throws['time'] == play['time']]
                if len(linked_free_throws) > 1:
                    linked_free_throws_event_ids: List[int] = linked_free_throws['EVENTNUM']
                    play_evt: int = int(play['EVENTNUM'])
                    if any(evt < play_evt for evt in linked_free_throws_event_ids) and \
                            any(evt > play_evt for evt in linked_free_throws_event_ids):
                        continue

            event['type'] = 'reb'
            is_after_oreb = previous_team == current_team
            event['is_oreb'] = is_after_oreb
            if is_after_oreb:
                last_oreb_time = play['time']

        # if play is a turnover
        elif play[eventmsgtype] == 5:
            event['type'] = 'tov'
            if 'Steal' in play['de']:
                event['player2'] = play['de'].split('Steal:')[1].split(' ')[0]
            event['tov_type'] = get_tov_type(get_play_description(play))

        events.append(event)
        previous_team: str = current_team
        previous_play: Dict = play

    # for players remaining on court at end of period, add player stint
    for player in home_players_on_court:
        player_stints.append(
            dict(
                game_id=int(game_id),
                sub_id=int(str(period) + str(sub_num)),
                season=int(game_info['year']),
                season_type=game_info['season_type'],
                player=player,
                team=home,
                start=home_players_on_court[player]['time'],
                end=get_period_end(period_df.iloc[-1]['time']),
                score_change=home_players_on_court[player]['score'] - score_diff,
            )
        )
        sub_num += 1
    for player in away_players_on_court:
        player_stints.append(
            dict(
                game_id=int(game_id),
                sub_id=int(str(period) + str(sub_num)),
                season=int(game_info['year']),
                season_type=game_info['season_type'],
                player=player,
                team=away,
                start=away_players_on_court[player]['time'],
                end=get_period_end(period_df.iloc[-1]['time']),
                score_change=away_players_on_court[player]['score'] + score_diff,
            )
        )
        sub_num += 1

    return [events, player_stints]


def get_game_events(game_id: int, pbp_df: pd.DataFrame) -> List[List[Dict]]:
    logging.info('Adding Game: ' + str(game_id))

    home: str = pbp_df[~pbp_df['HOMEDESCRIPTION'].isnull()]['PLAYER1_TEAM_ABBREVIATION'].value_counts().keys()[0]
    away: str = pbp_df[~pbp_df['VISITORDESCRIPTION'].isnull()]['PLAYER1_TEAM_ABBREVIATION'].value_counts().keys()[0]

    periods: List[int] = sorted(pbp_df['PERIOD'].unique())

    events: List[Dict] = list()
    player_stints: List[Dict] = list()

    for period in periods:
        period_df: pd.DataFrame = get_period_df(pbp_df, period)

        [period_events, period_player_stints] = get_events_for_period(period_df, game_id, period, home, away)
        events.extend(period_events)
        player_stints.extend(period_player_stints)

    return [events, player_stints]


def get_team_stats(pbp_df: pd.DataFrame, events: List[Dict], player_stints: List[Dict]) -> List[Dict]:
    team_stats = list()

    home: pd.DataFrame = pbp_df[~pbp_df['HOMEDESCRIPTION'].isnull()]['PLAYER1_TEAM_ABBREVIATION'].value_counts().keys()[
        0]
    away: pd.DataFrame = \
        pbp_df[~pbp_df['VISITORDESCRIPTION'].isnull()]['PLAYER1_TEAM_ABBREVIATION'].value_counts().keys()[0]

    for t in [home, away]:
        shots = [e for e in events if
                 e['type'] == 'shot' and
                 e['offensive_team'] == t]
        fouls_drawn = [e for e in events if
                       e['type'] == 'ft' and
                       e['offensive_team'] == t]
        assists = [e for e in events if
                   e['type'] == 'shot' and
                   e['player2'] is not None and
                   e['offensive_team'] == t]
        rebounds = [e for e in events if
                    e['type'] == 'reb' and
                    e['offensive_team'] == t]

        steals = [e for e in events if
                  e['type'] == 'tov' and
                  e['defensive_team'] == t]

        tov = [e for e in events if
               e['type'] == 'tov' and
               e['offensive_team'] == t]

        blocks = [e for e in events if
                  e['type'] == 'shot' and
                  e['player3'] is not None and
                  e['defensive_team'] == t]

        points_on_shots = sum([s['shot_value'] for s in shots])

        points_on_ft = sum(f['fts_made'] for f in fouls_drawn)
        points_on_ft += sum(0 if f['fts_made'] is None else f['fts_made'] for f in shots)
        total_points = points_on_shots + points_on_ft

        oposs = [e for e in events if e['offensive_team'] == t]
        dposs = [e for e in events if e['defensive_team'] == t]

        num_oposs = len(set([e['possession'] for e in oposs]))
        num_dposs = len(set([e['possession'] for e in dposs]))

        num_opoints = sum([x['score_change'] for x in oposs])
        num_dpoints = sum([x['score_change'] for x in dposs])

        player_rotations = [r for r in player_stints if r['team'] == t]
        player_total_seconds_played = 0
        for r in player_rotations:
            player_total_seconds_played += r['end'] - r['start']

        team_stats.append(dict(
            player=t,
            is_team=True,
            team=t,
            game_id=events[0]['game_id'],
            seconds_played=player_total_seconds_played,
            points=total_points,
            fg2a=len([s for s in shots if s['zone'] != 'Three']),
            fg2m=len([s for s in shots if s['zone'] != 'Three' and s['shot_value'] > 0]),
            fg3a=len([s for s in shots if s['zone'] == 'Three']),
            fg3m=len([s for s in shots if s['zone'] == 'Three' and s['shot_value'] > 0]),
            fta=sum([f['fts_taken'] for f in fouls_drawn]) + sum([s['fts_taken'] for s in shots]),
            ftm=sum([f['fts_made'] for f in fouls_drawn]) + sum([s['fts_made'] for s in shots]),
            and_one=sum([s['fts_taken'] for s in shots]),
            two_pt_fouls=len([f for f in fouls_drawn if f['fts_taken'] == 2]),
            three_pt_fouls=len([f for f in fouls_drawn if f['fts_taken'] == 3]),
            efg=round(points_on_shots / len(shots) * 500) / 10 if len(shots) > 0 else 0,
            ts_pct=round(total_points / (len(fouls_drawn) + len(shots)) * 500) / 10 if (len(fouls_drawn)
                                                                                        + len(
                        shots)) > 0 else 0,
            ast=len(assists),
            ast_pts=sum(a['score_change'] for a in assists),
            reb=len(rebounds),
            oreb=len([r for r in rebounds if r['is_oreb']]),
            dreb=len([r for r in rebounds if not r['is_oreb']]),
            tov=len(tov),
            stl=len(steals),
            blk=len(blocks),
            oposs=num_oposs,
            opoints=num_opoints,
            ortg=0 if num_oposs <= 0 else round(num_opoints / num_oposs * 100, 1),
            dposs=num_dposs,
            dpoints=num_dpoints,
            drtg=0 if num_dposs <= 0 else round(num_dpoints / num_dposs * 100, 1)
        ))

    return team_stats


def save_data_for_season(season: int, season_type: str):
    season_str = format_year_string(season)
    logging.info(f'Saving New Games to Database {season} - {season_type}')

    if season == current_season and current_season_type == 'Regular Season' and season_type == 'Playoffs':
        logging.info('Season has not started yet')
        return

    # get game ids that exist in the database for games
    game_ids_in_db: List[int] = pd.read_sql('SELECT DISTINCT id FROM game', con=db.engine)['id'].tolist()
    game_log: pd.DataFrame = get_game_log(season_str, season_type)
    try:
        game_ids_in_log: List[int] = [int(game_id) for game_id in game_log['GAME_ID'].unique()]
    except KeyError as e:
        logging.info('No new Games Found')
        return

    new_game_ids: List[int] = [game_id for game_id in game_ids_in_log if game_id not in game_ids_in_db]
    logging.info(f'{str(len(new_game_ids))} new games found')
    games_data: List[Dict] = list()
    for game_id in new_game_ids:
        logging.info(f'Saving {game_id} to Database')
        game_log_entries = game_log[game_log['GAME_ID'] == format_game_id(game_id)]
        away_entry = game_log_entries[game_log_entries['MATCHUP'].str.contains('@')].iloc[0]
        home_entry = game_log_entries[game_log_entries['MATCHUP'].str.contains('vs.')].iloc[0]
        games_data.append(
            {
                'id': game_id,
                'home': home_entry['TEAM_ABBREVIATION'],
                'away': away_entry['TEAM_ABBREVIATION'],
                'home_score': home_entry['PTS'],
                'away_score': away_entry['PTS'],
                'date': home_entry['GAME_DATE'],
                'season': season,
                'season_type': season_type
            }
        )

    try:
        pd.DataFrame(games_data).to_sql('game', db.engine, if_exists='append', index=False, chunksize=1000)
    except IntegrityError as e:
        logging.error(e)

    logging.info(f'Saving New PBP Games for Database {season} - {season_type}')
    # get game ids that exist in the database for play by play based data
    game_ids_in_db: List[int] = pd.read_sql('SELECT DISTINCT game_id FROM game_event', con=db.engine)[
        'game_id'].tolist()

    # game game ids that are on file for each type
    v1_game_ids: List[str] = os.listdir(
        os.path.dirname(os.path.realpath(__file__)) + f'/data/pbp/v1/{season_str}/{season_type}')
    v2_game_ids: List[str] = os.listdir(
        os.path.dirname(os.path.realpath(__file__)) + f'/data/pbp/v2/{season_str}/{season_type}')

    # merge lists of game ids that are on file
    game_ids_on_file: List[int] = [int(game_id[2:10]) for game_id in v2_game_ids
                                   if (game_id in v1_game_ids or season < 2016)]

    # find games ids that exist on file but are not in database
    new_game_ids: List[int] = [game_id for game_id in game_ids_on_file if game_id not in game_ids_in_db]
    logging.info(str(len(new_game_ids)) + ' New PBP Games Found')

    for game_id in new_game_ids:
        logging.info(f'Saving PBP Data for {game_id}')
        try:
            pbp_df: pd.DataFrame = get_merged_play_by_play(game_id)
        except Exception as e:
            logging.error(e)
            continue
        [events, player_stints] = get_game_events(game_id, pbp_df)
        player_stats = get_player_stats(events, player_stints, for_db=True)
        team_stats = get_team_stats(pbp_df, events, player_stints)

        try:
            pd.DataFrame(player_stats).to_sql('game_stats', db.engine, if_exists='append', index=False, chunksize=1000)
        except IntegrityError as e:
            logging.error(e)

        try:
            pd.DataFrame(team_stats).to_sql('game_stats', db.engine, if_exists='append', index=False, chunksize=1000)
        except IntegrityError as e:
            logging.error(e)

        try:
            pd.DataFrame(events).to_sql('game_event', db.engine, if_exists='append', index=False, chunksize=1000)
        except IntegrityError as e:
            logging.error(e)

        try:
            pd.DataFrame(player_stints).to_sql('player_stint', db.engine, if_exists='append', index=False,
                                               chunksize=1000)
        except IntegrityError as e:
            logging.error(e)


def initialize_pbp() -> None:
    logging.info('Initialize PBP Data')
    get_all_games()
    save_all_games()


if __name__ == "__main__":
    initialize_pbp()
    update_pbp()