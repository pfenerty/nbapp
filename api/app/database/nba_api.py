import pandas as pd
import requests
import json
import logging
import os
from typing import Dict

from app.database.util import get_info_for_game, data_headers, stats_headers, format_game_id, json_to_pandas

logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True


def get_game_play_by_play_v1(game_id, file_override: bool = False, file_write: bool = True):
    game_info = get_info_for_game(game_id)
    file_path = os.path.dirname(
        os.path.realpath(__file__)) + '/data/pbp/v1/{}/{}/{}.json' \
                    .format(game_info['season'], game_info['season_type'], game_id)

    if (not file_check(file_path)) or file_override:
        base_url = 'https://data.nba.com/data/10s/v2015/json/mobile_teams/nba/{}/scores/pbp/{}_full_pbp.json'
        url = base_url.format(game_info['year'], game_id)
        r = requests.get(url, headers=data_headers)
        json_data = r.json()['g']['pd']
        if file_write:
            with open(file_path, 'w') as json_file:
                json.dump(json_data, json_file)
        return json_data
    else:
        with open(file_path) as json_file:
            json_data = json.load(json_file)
        return json_data


def get_game_log(season_year: str, season_type: str, file_override: bool = False) -> pd.DataFrame:
    current_dir = os.path.dirname(os.path.realpath(__file__))
    file_path = f"{current_dir}/data/game_log/{season_year}-{season_type}.json"
    if not file_check(file_path) or file_override:
        base_url = f"https://stats.nba.com/stats/teamgamelogs?DateFrom=&DateTo=&GameSegment=&LastNGames=0" \
                   f"&LeagueID=00&Location=&MeasureType=Base&Month=0&OpponentTeamID=0&Outcome=&PORound=0" \
                   f"&PaceAdjust=N&PerMode=Totals&Period=0&PlusMinus=N&Rank=N&Season={season_year}&SeasonSegment" \
                   f"&SeasonType={season_type}&ShotClockRange=&VsConference=&VsDivision="
        url = base_url.format(season_year, season_type)
        logging.info(url)
        r = requests.get(url, headers=stats_headers)
        json_data = r.json()
        logging.info('completed')
        with open(file_path, 'w') as json_file:
            json.dump(json_data, json_file)
        return json_to_pandas(json_data)
    else:
        with open(file_path) as json_file:
            json_data = json.load(json_file)
        return json_to_pandas(json_data)


def get_game_play_by_play_v2(game_id, file_override: bool = False) -> pd.DataFrame:
    game_id = format_game_id(game_id)
    game_info = get_info_for_game(game_id)
    current_dir = os.path.dirname(os.path.realpath(__file__))
    file_path = f"{current_dir}/data/pbp/v2/{game_info['season']}/{game_info['season_type']}/{game_id}.json"
    if not file_check(file_path) or file_override:
        url = f"https://stats.nba.com/stats/playbyplayv2?EndPeriod=10&EndRange=55800&GameID={game_id}&RangeType=2" \
              f"&Season={game_info['season']}&SeasonType={game_info['season_type']}&StartPeriod=1&StartRange=0"
        r = requests.get(url, headers=stats_headers)
        json_data = r.json()
        with open(file_path, 'w') as json_file:
            json.dump(json_data, json_file)
        return json_to_pandas(json_data)
    else:
        with open(file_path) as json_file:
            json_data = json.load(json_file)
        return json_to_pandas(json_data)


def file_check(file_path) -> bool:
    split_path = file_path.split('/')
    current_path = '/'.join(split_path[:-1])

    if not os.path.exists(current_path):
        os.makedirs(current_path)

    return os.path.isfile(file_path)


def get_play_type_stats(season_year: str, play_type: str) -> pd.DataFrame:
    url = f'https://stats.nba.com/stats/synergyplaytypes?LeagueID=00&PerMode=Totals&PlayType={play_type}' \
          f'&PlayerOrTeam=P&SeasonType=Regular+Season&SeasonYear={season_year}&TypeGrouping=offensive'
    r = requests.get(url, headers=stats_headers)
    return json_to_pandas(r.json())


def get_shot_chart_detail(season_year: str, season_type: str, game_id: str, file_override: bool = False,
                          file_write: bool = True) -> pd.DataFrame:
    file_path: str = f'{os.path.dirname(os.path.realpath(__file__))}/data/shot_detail/{season_year}/{season_type}/{game_id}.json'
    if (not file_check(file_path)) or file_override:
        url: str = 'https://stats.nba.com/stats/shotchartdetail'
        params: Dict = {
            'LeagueID': '00',
            'Season': season_year,
            'SeasonType': season_type,
            'GameID': game_id,
            'TeamID': 0,
            'PlayerID': 0,
            'Outcome': '',
            'Location': '',
            'Month': 0,
            'SeasonSegment': '',
            'DateFrom': '',
            'DateTo': '',
            'OpponentTeamID': 0,
            'VsConference': '',
            'VsDivision': '',
            'PlayerPosition': '',
            'RookieYear': '',
            'GameSegment': '',
            'Period': 0,
            'LastNGames': 0,
            'ContextMeasure': 'FGA'
        }
        r = requests.get(url, params, headers=stats_headers)
        json_data = r.json()
        if file_write:
            with open(file_path, 'w') as json_file:
                json.dump(json_data, json_file)
    else:
        with open(file_path) as json_file:
            json_data = json.load(json_file)
    return json_to_pandas(json_data)
