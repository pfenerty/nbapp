from typing import Dict
import requests
from app.database.util import data_headers


nickname_map = {'Hawks': 'ATL',
                'Celtics': 'BOS',
                'Nets': 'BKN',
                'Hornets': 'CHA',
                'Bobcats': 'CHA',
                'Bullets': 'WAS',
                'Bulls': 'CHI',
                'Cavaliers': 'CLE',
                'Mavericks': 'DAL',
                'Nuggets': 'DEN',
                'Pistons': 'DET',
                'Warriors': 'GSW',
                'Rockets': 'HOU',
                'Pacers': 'IND',
                'Clippers': 'LAC',
                'Lakers': 'LAL',
                'Grizzlies': 'MEM',
                'Heat': 'MIA',
                'Bucks': 'MIL',
                'Timberwolves': 'MIN',
                'Pelicans': 'NOP',
                'Knicks': 'NYK',
                'Thunder': 'OKC',
                'Magic': 'ORL',
                '76ers': 'PHI',
                '76Ers': 'PHI',
                'Suns': 'PHX',
                'Trail Blazers': 'POR',
                'Kings': 'SAC',
                'Spurs': 'SAS',
                'Supersonics': 'SEA',
                'Raptors': 'TOR',
                'Jazz': 'UTA',
                'Wizards': 'WAS',
                '': 'NA'
            }


def get_nickname_map() -> Dict[str, str]:
    url = 'http://data.nba.net/data/10s/prod/v1/2018/teams.json'
    req = requests.get(url, headers=data_headers)
    json_resp = req.json()
    return {team['nickname']: team['tricode'] for team in json_resp['league']['standard'] if team['isNBAFranchise']}
