from app.database.nba_api import get_play_type_stats
from app.database.util import format_year_string
from app.database.data import file_check
import pandas as pd
import os
from sqlalchemy.exc import IntegrityError
from app import db
import logging


logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)

play_types = ['Transition', 'Isolation', 'PRBallHandler', 'PRRollman', 'Postup', 'Spotup', 'Handoff', 'Cut',
              'OffScreen', 'OffRebound', 'Misc']
ignored_columns = ['FGMX']
available_years = range(2015, 2020)


def get_play_type_data_for_year(year: int) -> None:
    df = pd.DataFrame()
    year_string = format_year_string(year)
    for pt in play_types:
        pt_df = get_play_type_stats(year_string, pt)
        pt_df['PLAY_TYPE'] = pt
        df = df.append(pt_df)
    df['SEASON'] = year
    df.drop(labels=ignored_columns, axis=1, inplace=True)
    df.columns = [x.lower() for x in df.columns]
    file_path = os.path.dirname(os.path.realpath(__file__)) + f'/data/play-type/{year_string}.csv'
    file_check(file_path)
    df.to_csv(file_path, index=False)


def get_play_type_data_for_all_years() -> None:
    for year in available_years:
        get_play_type_data_for_year(year)


def update_play_type() -> None:
    get_play_type_data_for_year(2019)
    save_play_type_data()


def save_play_type_data() -> None:
    df = pd.DataFrame()
    for year in available_years:
        year_string = format_year_string(year)
        file_path = os.path.dirname(os.path.realpath(__file__)) + f'/data/play-type/{year_string}.csv'
        year_df = pd.read_csv(file_path)
        df = df.append(year_df)
    try:
        df.to_sql('play_type', db.engine, if_exists='replace', index=False, chunksize=1000)
    except IntegrityError as e:
        logging.error(e)


def initialize_play_type() -> None:
    get_play_type_data_for_all_years()
    save_play_type_data()


if __name__ == "__main__":
    initialize_play_type()
