import re

import pandas as pd

seasons = [2016, 2017, 2018]

data_headers = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate',
    'Accept-Language': 'en-US,en;q=0.5',
    'Connection': 'keep-alive',
    'DNT': '1',
    'Host': 'data.nba.net',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:64.0) Gecko/20100101 Firefox/64.0'
}

stats_headers = {
    'Host': 'stats.nba.com',
    'User-Agent': 'Firefox/55.0',
    'Accept': 'application/json, text/plain, */*',
    'Accept-Language': 'en-US,en;q=0.5',
    'Accept-Encoding': 'gzip, deflate',
    'Referer': 'https://stats.nba.com/',
    'x-nba-stats-origin': 'stats',
    'x-nba-stats-token': 'true',
    'DNT': '1',
}


# class GameInfo:
#     def __init__(self, game_id: int):
#         self.game_id: int = game_id
#
#     def get_game_id_str(self) -> str:
#         game_id_str: str = str(self.game_id)
#         while len(game_id_str) < 10:
#             game_id_str = '0{}'.format(game_id_str)
#         return game_id_str
#
#     def get_season_int(self) -> int:
#         game_id_str: str = self.get_game_id_str()
#         first_year_ending: str = game_id_str[3:5]
#         if int(first_year_ending) > 90:
#             return int(f'19{first_year_ending}')
#         else:
#             return int('20' + first_year_ending)
#
#     def get_season_str(self) -> str:



def get_info_for_game(game_id):
    game_id = format_game_id(game_id)
    first_year_ending = game_id[3:5]
    season_type = 'Regular Season' if game_id[2] == '2' else 'Playoffs'
    if int(first_year_ending) > 90:
        season = '19' + first_year_ending
    else:
        season = '20' + first_year_ending

    return {
        'year': season,
        'season': format_year_string(season),
        'season_type': str(season_type)
    }


def format_game_id(game_id):
    game_id = str(game_id)
    while len(game_id) < 10:
        game_id = '0{}'.format(game_id)
    return game_id


def format_year_string(year):
    year = str(year)
    if re.match(r'[1-2][09][0-9]{2}-[0-9]{2}', year):
        return year
    elif re.match(r'[1-2][09][0-9]{2}', year):
        return str(year) + "-" + str(int(year) + 1)[2:4]
    else:
        raise ValueError('Unexpected Year Format!')


def json_to_pandas(json: dict, index: int = 0) -> pd.DataFrame:
    data = json['resultSets'][index]
    headers = data['headers']
    rows = data['rowSet']
    data_dict = [dict(zip(headers, row)) for row in rows]
    return pd.DataFrame(data_dict)


def get_season_game_id_prefix(season, season_type):
    season_ending = format_year_string(season)[2:4]
    return int(('2' if season_type == 'Regular Season' else '4') + season_ending)
