export class Dimensions {
    height: number;
    width: number;
    margins: Margins;

    constructor(height: number, width: number, margins: Margins) {
        this.height = height;
        this.width = width;
        this.margins = margins;
    }

    get chartHeight(): number { return this.height - this.margins.bottom - this.margins.top; }
    get chartWidth(): number { return this.width - this.margins.left - this.margins.right; }

    get xAxisRange(): Array<number> { return [this.margins.left, this.width - this.margins.right]; }
    get yAxisRange(): Array<number> { return [this.margins.top, this.height - this.margins.bottom]; }
}

export class Margins {
    left: number;
    right: number;
    top: number;
    bottom: number;

    constructor(top: number, bottom: number, left: number, right: number) {
        this.top = top;
        this.bottom = bottom;
        this.left = left;
        this.right = right;
    }
}