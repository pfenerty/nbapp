import { ShotGridCell } from './shot-grid-cell.model';

export class ShotGrid {
    cells: ShotGridCell[];
    zoneSideInfo: {};

    constructor(cells: ShotGridCell[], zoneEff: {}) {
        this.cells = cells;
        this.zoneSideInfo = zoneEff;
    }
}