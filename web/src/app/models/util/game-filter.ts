export class GameFilter {
    date: Date;
    team1: String;
    team2: String;
    season: Number;
    seasonType: String;

    constructor() {
        this.date = new Date();
        this.team1 = null;
        this.team2 = null;
        this.season = null;
        this.seasonType = null;
    }
}