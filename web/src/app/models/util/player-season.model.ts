import { TeamSeason } from '../../components/season-team-controls/season-team-controls.component';

export class PlayerSeason {
    player: string;
    year: number;
    team: string;

    constructor(player: string, year: number, team: string) {
        this.player = player;
        this.year = year;
        this.team = team;
    }

    public equals(b: PlayerSeason): boolean {
        return this.player === b.player && this.year === b.year && this.team === b.team;
    }
}