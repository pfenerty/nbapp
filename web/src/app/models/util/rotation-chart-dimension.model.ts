import {Margin} from '../data/margin.model';

export class RotationChartDimensions {
    height: number;
    width: number;
    margin: Margin;

    constructor(windowWidth: number, minWidth: number, maxWidth: number) {
        const calculatedWidth = windowWidth < minWidth ? minWidth : (windowWidth > maxWidth ? maxWidth : windowWidth);
        this.height = calculatedWidth * 40 / 100;
        this.margin = new Margin(calculatedWidth);
        this.width = calculatedWidth - this.margin.right - this.margin.left;
    }
}