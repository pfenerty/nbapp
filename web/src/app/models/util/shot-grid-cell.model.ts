export class ShotGridCell {
    x: number;
    y: number;
    raw_vol: number;
    adj_vol: number;
    zone: string;
    side: string;

    constructor(x: number, y: number, raw_vol: number, adj_vol: number, zone: string, side: string) {
        this.x = x;
        this.y = y;
        this.raw_vol = raw_vol;
        this.adj_vol = adj_vol;
        this.zone = zone;
        this.side = side;
    }
}
