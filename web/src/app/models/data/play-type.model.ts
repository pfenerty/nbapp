export class PlayType {
    efg_pct: number;
    fga: number;
    fgm: number;
    percentile: number;
    play_type: string;
    player_id: number;
    player_name: string;
    poss: number;
    poss_pct: number;
    ppp: number;
    pts: number;
    season: number;
    team: string;
    type_grouping: string;
}
