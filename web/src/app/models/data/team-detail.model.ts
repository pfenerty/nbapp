import { Rotation } from "./rotation.model";
import { Shot } from "./shot.model";
import { PlayerGameStats } from "./player-game-stats.model";

export class TeamDetail {
    rotations: Rotation[];
    shots: Shot[];
    oppShots: Shot[];
    playType: any[];
    stats: PlayerGameStats[];
}