export class Rotation {
    end: number;
    player: string;
    start: number;
    team: string;
    game_id: number;
    score_change: number;
}
