export class PossessionStats {
    team: string;
    possessions: number;
    fg3: ShotPossessions;
    fg2: ShotPossessions;
    ft: ShootingFoulPossessions;
    turnovers: number;
    oRebs: number;
}

export class GamePossessionStats {
    home: PossessionStats;
    away: PossessionStats;
}

export class ShotPossessions {
    attempts: number;
    makes: number;
    pct: number;
    eff: number;
}

export class ShootingFoulPossessions extends ShotPossessions {
    trips: number;
}
