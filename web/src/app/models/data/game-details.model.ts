import { Rotation } from './rotation.model';
import { Score } from './score.model';
import { GameEvent } from './game-event.model';
import { Team } from './team.model';

export class GameDetails {
    home: string;
    away: string;
    secondsPlayed: number;
    rotations: Array<Rotation>;
    scores: Array<Score>;
    scoreboard: ScoreBoard;
    events: Array<GameEvent>;
    stats: GameStats;
    lineups: Lineups;
}

export class TeamRotations {
    team: string;
    rotations: Array<Rotation>;
}

export class GameRotations {
    home: TeamRotations;
    away: TeamRotations;
}

export class ScoreBoard {
    home: Team;
    away: Team;
    time: string;
}

export class GameStats {
    home: Array<PlayerGameStats>;
    away: Array<PlayerGameStats>;
}

export class PlayerGameStats {
    player: string;
    team: string;
    scoring: ScoringStats;
    passing: PassingStats;
    rebounding: ReboundingStats;
    tov: number;
    stl: number;
    min: string;
}

export class ScoringStats {
    points: number;
    fg2a: number;
    fg2m: number;
    fg3a: number;
    fg3m: number;
    fta: number;
    ftm: number;
    twoPtFouls: number;
    threePtFouls: number;
    and1s: number;
    efg: number;
    ts_pct: number;
}

class PassingStats {
    assists: number;
    playersAssisted: any;
}

class ReboundingStats {
    off: number;
    def: number;
}

export class Lineups {
    home: Lineup;
    away: Lineup;
}

export class Lineup {
    lineup: string;
    off_poss: number;
    def_poss: number;
    off_points: number;
    def_points: number;
    diff: number;
}
