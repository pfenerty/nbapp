import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class MenuService extends BaseService {

    private baseUrl = this.apiUrl + 'menu/';

    constructor(http: HttpClient) { super(http); }

    public getSeasons(): Observable<Array<number>> {
        return this.http.get<Array<number>>(
            `${this.baseUrl}seasons`
        );
    }

    public getTeams(season: number, seasonType: string): Observable<Array<string>> {
        return this.http.post<Array<string>>(
            `${this.baseUrl}teams`,
            {
                'season': season,
                'seasonType': seasonType
            }
        );
    }

    public getPlayers(season: number, seasonType: string, team: string): Observable<Array<string>> {
        return this.http.post<Array<string>>(
            `${this.baseUrl}players`,
            {
                'season': season,
                'seasonType': seasonType,
                'team': team
            }
        );
    }
}
