import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { GameDetails } from '../models/data/game-details.model';
import { BaseService } from './base.service';
import { Starters } from '../views/rotations-view/rotations-view.component';
import { TeamSeason } from '../components/season-team-controls/season-team-controls.component';

@Injectable({ providedIn: 'root' })
export class API extends BaseService {

    private gamesUrl = this.apiUrl + 'games';
    private gameDetailUrl = this.apiUrl + 'gameDetail';

    constructor(
        http: HttpClient,
    ) {
        super(http);
    }

    getGames(date: Date): Observable<any> {
        const dateString = this.formatDate(date);

        const url = `${this.apiUrl}games/${dateString}`;

        return this.http.get<any>(url).pipe(
            catchError(this.handleError('getGames', []))
        );
    }

    getGame(id: string, home: string, away: string): Observable<GameDetails> {
        const url = `${this.apiUrl}gameDetail/${id}/${home}/${away}`;
        return this.http.get<GameDetails>(url).pipe(
            catchError(this.handleError<GameDetails>(`getGame id=${id}`)
            )
        );
    }

    searchGames(teamSeason: TeamSeason): Observable<any> {
        const url = `${this.apiUrl}games/search`;
        return this.http.post<TeamSeason>(
            url,
            {teamSeason}
        ).pipe(
            catchError(this.handleError('searchGames', []))
        );
    }

    getPlayerConsistency(season: number, seasonType: string, team: string,
        numPlayer: number, stat: string, isTeam: boolean): Observable<any> {
        return this.http.post<any>(
            `${this.apiUrl}game_by_game`,
            {
                'season': season,
                'seasonType': seasonType,
                'numPlayers': numPlayer,
                'team': team,
                'stat': stat,
                'isTeam': isTeam
            }).pipe(
                catchError(this.handleError('getPlayerConsistency', []))
            );
    }

    getRotations(season: number, seasonType: string, team: string, gameIds: Array<number>) {
        return this.http.post<any>(
            `${this.apiUrl}rotations`,
            {
                'season': season,
                'seasonType': seasonType,
                'team': team,
                'gameIds': gameIds
            }).pipe(
                catchError(this.handleError('getRotations', []))
            );
    }

    getStartingLineups(season: number, seasonType: string, team: string) {
        return this.http.post<Array<Starters>>(
            `${this.apiUrl}starters`,
            {
                'season': season,
                'seasonType': seasonType,
                'team': team
            }
        ).pipe(
            catchError(this.handleError('getStarters', []))
        );
    }

    getShots(season: number, seasonType: string, team: string, player: string) {
        return this.http.post<any>(
            `${this.apiUrl}shots`,
            {
                'season': season,
                'seasonType': seasonType,
                'team': team,
                'player': player
            }).pipe(
                catchError(this.handleError('getShots', []))
            );
    }

    getShotTiming(season: number, seasonType: string, team: string, player: string, liveBall: boolean, deadBall: boolean) {
        return this.http.post<any>(
            `${this.apiUrl}shots/time`,
            {
                'season': season,
                'seasonType': seasonType,
                'team': team,
                'player': player,
                'liveBall': liveBall,
                'deadBall': deadBall,
            }).pipe(
                catchError(this.handleError('getShotTiming', []))
            );
    }

    getPlayerPlayTypeData(season: number, player: string): Observable<any> {
        return this.http.post<any>(
            `${this.apiUrl}play-type/player`,
            { 'player': player, 'season': season }
        ).pipe(
            catchError(this.handleError('getPlayerPlayTypeData'))
        );
    }

    getSeasonPlayTypeData(season: number): Observable<any> {
        return this.http.post<any>(
            `${this.apiUrl}play-type/season`,
            { 'season': season }
        ).pipe(
            catchError(this.handleError('getSeasonPlayTypeData'))
        );
    }

    getAllPlayTypeData(): Observable<any> {
        return this.http.post<any>(
            `${this.apiUrl}play-type/all`,
            { }
        ).pipe(
            catchError(this.handleError('getAllPlayTypeData'))
        );
    }

    getLineups(id: string, home: string, away: string): Observable<any> {
        const url = `${this.apiUrl}lineups/${id}/${home}/${away}`;
        return this.http.get<any>(url).pipe(
            catchError(this.handleError<any>(`getLineups id=${id}`)
            )
        );
    }

    private formatDate(date: Date) {
        const year = date.getFullYear().toString();
        let month = (date.getMonth() + 1).toString(),
            day = date.getDate().toString();

        month = month.length > 1 ? month : '0' + month;
        day = day.length > 1 ? day : '0' + day;

        return year + month + day;
    }
}