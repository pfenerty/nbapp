// NPM Imports

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';

import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatSelectModule,
  MatSortModule,
  MatSidenavModule,
  MatSlideToggleModule,
  MatTabsModule,
  MatTableModule,
  MatTooltipModule,
  MatToolbarModule,
} from '@angular/material';

// Top Level App Imports
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

// Header Footer
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';

// Views
import { GamesComponent } from './views/game/games/games.component';
import { GameDetailComponent } from './views/game/game-detail/game-detail.component';

// Components
import { GameRotationsVizComponent } from './components/viz/rotations/game-rotations-viz/game-rotations-viz.component';
import { ScatterShotChartComponent } from './components/viz/shots/scatter-shot-chart/scatter-shot-chart.component';

import { SeasonRotationsVizComponent } from './components/viz/rotations/season-rotations-viz/season-rotations-viz.component';
import { PlayTypeComponent } from './views/play-type/play-type.component';
import { PossessionBreakdownComponent } from './components/viz/possession-breakdown/possession-breakdown.component';
import { ConsistencyComponent } from './views/consistency/consistency.component';
import { ExpandableStatsTableComponent } from './components/expandable-stats-table/expandable-stats-table.component';
import { RotationsViewComponent } from './views/rotations-view/rotations-view.component';
import { SeasonRotationsViz2Component } from './components/viz/rotations/season-rotations-viz2/season-rotations-viz2.component';
import { GameShotSettingsComponent } from './components/viz/shots/game-shot-settings/game-shot-settings.component';
import { GameShotChartComponent } from './components/viz/shots/game-shot-chart/game-shot-chart.component';
import { ShotStatsComponent } from './components/viz/shots/shot-stats/shot-stats.component';
import { ShotsComponent } from './views/shots/shots.component';
import { SeasonTeamControlsComponent } from './components/season-team-controls/season-team-controls.component';
import { PlayerOrderComponent } from './components/viz/rotations/game-rotations-viz/player-order/player-order.component';
import { ShotTimeComponent } from './views/shot-time/shot-time.component';
import { PlayTypeChartComponent } from './views/play-type/play-type-chart/play-type-chart.component';

// Pipes
import { SeasonDisplayPipe } from './pipes/season-display.pipe';
import { ScoreboardComponent } from './views/game/game-detail/scoreboard/scoreboard.component';
import { GameOverviewComponent } from './components/game-overview/game-overview.component';
import { GameOverviewStatComponent } from './components/game-overview/game-overview-stat/game-overview-stat.component';
import { GameOverviewColumnComponent } from './components/game-overview/game-overview-column/game-overview-column.component';
import { LineupsComponent } from './views/game/game-detail/lineups/lineups.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    GamesComponent,
    GameDetailComponent,
    GameRotationsVizComponent,
    ScatterShotChartComponent,
    SeasonRotationsVizComponent,
    PlayTypeComponent,
    PossessionBreakdownComponent,
    ConsistencyComponent,
    ExpandableStatsTableComponent,
    RotationsViewComponent,
    SeasonRotationsViz2Component,
    GameShotSettingsComponent,
    GameShotChartComponent,
    ShotStatsComponent,
    ShotsComponent,
    SeasonTeamControlsComponent,
    PlayerOrderComponent,
    ShotTimeComponent,
    PlayTypeChartComponent,
    SeasonDisplayPipe,
    ScoreboardComponent,
    GameOverviewComponent,
    GameOverviewStatComponent,
    GameOverviewColumnComponent,
    LineupsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatExpansionModule,
    MatGridListModule,
    MatListModule,
    MatInputModule,
    MatMenuModule,
    MatIconModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSelectModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatSortModule,
    MatTabsModule,
    MatTableModule,
    MatTooltipModule,
    MatToolbarModule,
    FlexLayoutModule
  ],
  providers: [
    HttpClientModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
