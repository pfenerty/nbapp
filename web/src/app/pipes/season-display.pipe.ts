import {Pipe, PipeTransform} from '@angular/core';


@Pipe({
    name: 'seasonDisplay'
})
export class SeasonDisplayPipe implements PipeTransform {
    transform(season: number): string {
        return `${season}-${((season + 1) % 100).toString().padStart(2, '0')}`;
    }
}
