import { TEAM_COLORS } from '../../enums';
import { Rotation } from 'src/app/models/data/rotation.model';
import { Score } from 'src/app/models/data/score.model';
import * as d3 from 'd3';

const totalHeight = 40,
    totalWidth = 100,
    transitionLength = 2000;

export function updatePlayerOrder(svg, rotations: Array<Rotation>, score_data: Array<Score>, home, away, homePlayers, awayPlayers): void {
    const home_color = TEAM_COLORS[home][0],
        away_color = TEAM_COLORS[away][0];

    const players = awayPlayers.map(p => p);
    players.push('');
    players.push(...homePlayers);
    const rectHeight = totalHeight / (players.length + 4);

    // find the last minute logged: determines overtime
    // calculate longest player name: determines width of player labels
    const maxTime = rotations.map(r => r.end).reduce((a: number, b: number) => a > b ? a : b),
        longestPlayerName = players.reduce((a: string, b: string) => a.length > b.length ? a : b),
        playerLabelsWidth = (longestPlayerName.length) * (rectHeight * 0.5);

    // calculate height of each player row. height over number of players plus top and bottom minute labels
    // calculate scale used to determine width of rotation rects

    const x = d3.scaleLinear().range([playerLabelsWidth, totalWidth - 5]);
    x.domain([0, maxTime]);

    const y = d3.scaleLinear().range([1, totalHeight - 7]);
    y.domain([0, players.length]);

    // Add Player Labels
    const playerLabels = svg.selectAll('.playerLabel')
        .data(players, d => d);

    playerLabels
        .transition().duration(transitionLength)
        .attr('x', playerLabelsWidth)
        .attr('y', (d, index) => y(index + 1))
        .style('fill-opacity', 1);

    const cards = svg.selectAll('.rotation')
        .data(rotations, (d: Rotation) => [d.player, d.start, d.end]);

    cards
        .transition().duration(transitionLength)
        .attr('y', d => y(getPlayerIndex(homePlayers, awayPlayers, d, d.team === home)))
        .style('fill', d => {
            const index = getPlayerIndex(homePlayers, awayPlayers, d, d.team === home) - 0.5;
            return index % 2 === 0 ? '#000000' : '#4f4f4f';
        });
}

export function plotRotations(svg, rotations: Array<Rotation>, score_data: Array<Score>, home, away, homePlayers, awayPlayers): void {
    const home_color = TEAM_COLORS[home][0],
        away_color = TEAM_COLORS[away][0];

    const players = awayPlayers.map(p => p);
    players.push('');
    players.push(...homePlayers);
    const rectHeight = totalHeight / (players.length + 4);

    // find the last minute logged: determines overtime
    // calculate longest player name: determines width of player labels
    const maxTime = rotations.map(r => r.end).reduce((a: number, b: number) => a > b ? a : b),
        longestPlayerName = players.reduce((a: string, b: string) => a.length > b.length ? a : b),
        playerLabelsWidth = (longestPlayerName.length) * (rectHeight * 0.5);

    // calculate height of each player row. height over number of players plus top and bottom minute labels
    // calculate scale used to determine width of rotation rects

    const x = d3.scaleLinear().range([playerLabelsWidth, totalWidth - 5]);
    x.domain([0, maxTime]);

    const y = d3.scaleLinear().range([1, totalHeight - 7]);
    y.domain([0, players.length]);

    // Add Player Labels
    const playerLabels = svg.selectAll('.playerLabel')
        .data(players, d => d);

    playerLabels.enter()
        .append('text')
        .text(d => d)
        .style('font-size', rectHeight * 0.75 + 'px')
        .classed('mono', true)
        .classed('playerLabel', true)
        .style('text-anchor', 'end')
        .style('fill-opacity', 0)
        .attr('x', -10)
        .attr('y', (d, index) => y(index + 1))
        .transition().duration(transitionLength)
        .style('fill-opacity', 1)
        .attr('x', playerLabelsWidth);

    playerLabels.exit()
        .transition()
        .duration(1000)
        .style('fill-opacity', 0)
        .attr('y', y(players.length + 5))
        .remove();

    playerLabels
        .transition().duration(1000)
        .attr('x', playerLabelsWidth)
        .attr('y', (d, index) => y(index + 1))
        .style('fill-opacity', 1);

    // Add Quarter Labels
    const minutes = ['Q1', 'Q2', 'Q3', 'Q4'];
    if (maxTime > 2880) {
        const extraTime = maxTime - 2880;
        const numOverTime = extraTime / 300;
        for (let i = 1; i <= numOverTime; i++) {
            minutes.push('OT' + i);
        }
    }
    minutes.push('end');

    svg.selectAll('.quarterLabelTop')
        .data(minutes)
        .enter().append('text')
        .text(d => d)
        .classed('mono', true)
        .classed('quarterLabelTop', true)
        .attr('y', -10)
        .attr('x', (d, i) => {
            return i < 4 ? x(i * 60 * 12) + 1 : x((4 * 60 * 12) + ((i - 4) * 60 * 5)) + 1;
        })
        .transition().duration(transitionLength)
        .attr('y', y(0))
        .attr('font-size', rectHeight * 0.75 + 'px')
        .style('text-anchor', 'end');

    svg.selectAll('.quarterLabelBottom')
        .data(minutes)
        .enter().append('text')
        .text(d => d)
        .classed('mono', true)
        .classed('quarterLabelBottom', true)
        .attr('y', totalHeight + 10)
        .attr('x', (d, i) => {
            return i < 4 ? x(i * 60 * 12) + 1 : x((4 * 60 * 12) + ((i - 4) * 60 * 5)) + 1;
        })
        .transition().duration(transitionLength)
        .attr('y', y(players.length + 1))
        .attr('font-size', rectHeight * 0.75 + 'px')
        .style('text-anchor', 'end');

    const cards = svg.selectAll('.rotation')
        .data(rotations, (d: Rotation) => [d.player, d.start, d.end]);

    cards
        .enter()
        .append('rect')
        .attr('x', d => x((d.start + d.end) / 2))
        .attr('y', d => y(getPlayerIndex(homePlayers, awayPlayers, d, d.team === home) + 0.5))
        .attr('width', 0)
        .attr('height', 0)
        .style('fill', d => {
            const index = getPlayerIndex(homePlayers, awayPlayers, d, d.team === home) - 0.5;
            return index % 2 == 0 ? '#000000' : '#4f4f4f';
        })
        .attr('rx', 0.25)
        .attr('ry', 0.25)
        .classed('rotation', true)
        .transition().duration(transitionLength)
        .attr('x', d => x(d.start))
        .attr('width', d => x(d.end) - x(d.start))
        .attr('y', d => y(getPlayerIndex(homePlayers, awayPlayers, d, d.team === home)))
        .attr('height', rectHeight * .8);

    cards
        .transition().duration(transitionLength)
        .attr('y', d => y(getPlayerIndex(homePlayers, awayPlayers, d, d.team === home)));

    const maxLead = Math.abs(score_data.map(s => s.score).reduce((a, b) => Math.abs(a) > Math.abs(b) ? a : b));

    const yAxisScale = (Math.min(awayPlayers.length, homePlayers.length) * 2) / players.length * (totalHeight - 7);
    const yAxisShift = (Math.max(0, awayPlayers.length - homePlayers.length) + 1.5) / players.length * (totalHeight - 7);

    const scoreY = d3.scaleLinear()
        .range([yAxisShift, yAxisScale + yAxisShift]);

    scoreY.domain([Math.min(-maxLead, -20), Math.max(maxLead, 20)]);

    svg.append('g')
        .style('font-size', rectHeight * 0.75 + 'px')
        .attr('transform', 'translate(110, 0)')
        .transition().duration(transitionLength)
        .call(d3.axisRight(scoreY).tickSize(0).tickFormat(d => d < 0 ? -d : d))
        .attr('transform', 'translate(94, 0)');

    let score_datas: Array<Array<Score>> = [];
    let current_data: Array<Score> = [],
        current_score = 0;

    score_data.sort((a, b) => a.time - b.time);

    score_data.forEach(s => {
        if (s.score === 0) {
            current_data.push(s);
        } else if ((current_score * s.score) < 0) {
            score_datas.push(current_data);
            current_data = [s];
            current_score = s.score;
        } else {
            current_data.push(s);
            current_score = s.score;
        }
    });
    score_datas.push(current_data);

    const line = d3.area()
        .x(d => x(d.time))
        .y0(d => scoreY(0))
        .y1(d => scoreY(-d.score))
        .curve(d3.curveStep);

    const flatline = d3.area()
        .x(d => x(d.time))
        .y0(d => scoreY(0))
        .y1(d => scoreY(0))
        .curve(d3.curveStep);

    score_datas = score_datas.filter(sd => sd.length > 0);

    for (let ix = 0; ix < score_datas.length; ix++) {
        score_datas[ix] = score_datas[ix].sort((a, b) => a.time - b.time);
    }

    score_datas.forEach(element => {
        const element_scores = element.map(e => e.score),
            avg_score = element_scores.reduce((a, b) => a + b) / element_scores.length,
            line_color = avg_score < 0 ? home_color : away_color;

        svg.append('path')
            .datum(element)
            .attr('class', 'point-diff')
            .attr('fill', line_color)
            .attr('stroke', line_color)
            .attr('stroke-opacity', 1)
            .attr('stroke-linejoin', 'bevel')
            .attr('stroke-linecap', 'square')
            .attr('stroke-width', 0.15)
            .attr('opacity', 0.5)
            .transition().duration(transitionLength)
            .attr('d', flatline);
    });

    svg.selectAll('.point-diff')
        .transition().duration(transitionLength)
        .attr('d', line);

    const strokeWidth = 0.2,
        strokeOpacity = 0.25,
        strokeColor = 'black';

    for (let i = 0; i < minutes.length; i++) {
        svg.append('line')
            .attr('x1', i < 4 ? x(i * 60 * 12) : x((4 * 60 * 12) + ((i - 4) * 60 * 5)))
            .attr('x2', i < 4 ? x(i * 60 * 12) : x((4 * 60 * 12) + ((i - 4) * 60 * 5)))
            .attr('y1', y(players.length / 2))
            .attr('y2', y(players.length / 2))
            .attr('stroke-opacity', 0)
            .style('stroke-width', strokeWidth)
            .style('stroke', strokeColor)
            .style('fill', 'none')
            .transition().duration(transitionLength)
            .attr('stroke-opacity', strokeOpacity)
            .attr('y1', y(0.5))
            .attr('y2', y(players.length));
    }

    svg.append('rect')
        .attr('y', totalHeight + 10)
        .transition().duration(transitionLength)
        .attr('x', playerLabelsWidth)
        .attr('y', totalHeight - rectHeight * 1.5)
        .attr('rx', 0.2)
        .attr('ry', 0.2)
        .attr('width', totalWidth / 10)
        .attr('height', rectHeight)
        .style('fill', 'black');

    svg.append('text')
        .attr('y', totalHeight + 10)
        .text('Player')
        .style('font-size', rectHeight * 0.75 + 'px')
        .classed('mono', true)
        .style('text-anchor', 'end')
        .transition().duration(transitionLength)
        .attr('x', playerLabelsWidth)
        .attr('y', totalHeight - rectHeight * 1.2);

    svg.append('text')
        .attr('y', totalHeight + 10)
        .text('On Court')
        .style('font-size', rectHeight * 0.75 + 'px')
        .classed('mono', true)
        .style('text-anchor', 'end')
        .transition().duration(transitionLength)
        .attr('x', playerLabelsWidth)
        .attr('y', totalHeight - rectHeight * 0.5);

    svg.append('rect')
        .attr('y', totalHeight + 10)
        .transition().duration(transitionLength)
        .attr('x', playerLabelsWidth + totalWidth / 10)
        .attr('y', totalHeight - rectHeight)
        .attr('rx', 0.2)
        .attr('ry', 0.2)
        .attr('width', totalWidth / 40)
        .attr('height', rectHeight)
        .attr('opacity', 0.5)
        .style('fill', home_color);

    svg.append('text')
        .attr('y', totalHeight + 10)
        .classed('mono', true)
        .text(home + ' Lead')
        .style('font-size', rectHeight * 0.75 + 'px')
        .transition().duration(transitionLength)
        .attr('x', playerLabelsWidth + totalWidth * 5 / 40)
        .attr('y', totalHeight - rectHeight * 0.25);

    svg.append('rect')
        .attr('y', totalHeight + 10)
        .transition().duration(transitionLength)
        .attr('x', playerLabelsWidth + totalWidth / 10)
        .attr('y', totalHeight - rectHeight * 2)
        .attr('rx', 0.2)
        .attr('ry', 0.2)
        .attr('width', totalWidth / 40)
        .attr('height', rectHeight)
        .attr('opacity', 0.5)
        .style('fill', away_color);

    svg.append('text')
        .attr('y', totalHeight + 10)
        .style('font-size', rectHeight * 0.75 + 'px')
        .classed('mono', true)
        .text(away + ' Lead')
        .transition().duration(transitionLength)
        .attr('x', playerLabelsWidth + totalWidth * 5 / 40)
        .attr('y', totalHeight - rectHeight);
}

function getPlayerIndex(homePlayers, awayPlayers, player, isHome) {
    if (!isHome) {
        return awayPlayers.indexOf(player.player) + 0.5;
    } else {
        return homePlayers.indexOf(player.player) + awayPlayers.length + 1.5;
    }
}

export function sortPlayers(team: string, rotations: Array<Rotation>): Array<string> {
    rotations = rotations.filter(r => r.team === team);
    const players = rotations.map(r => r.player).filter((v, i, a) => a.indexOf(v) === i);
    players.sort((a, b) => compareRotation(a, b, rotations));
    return players;
}

export function compareRotation(a: string, b: string, rotations: Array<Rotation>): number {
    const aRotations = rotations.filter(r => r.player === a);
    aRotations.sort((c, d) => c.start - d.start);
    const aStart = aRotations[0].start,
        aEnd = aRotations[0].end;

    const bRotations = rotations.filter(r => r.player === b);
    bRotations.sort((c, d) => c.start - d.start);
    const bStart = bRotations[0].start,
        bEnd = bRotations[0].end;

    if (aStart !== bStart) {
        return aStart - bStart;
    } else {
        return bEnd - aEnd;
    }
}
