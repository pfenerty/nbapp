import * as d3 from 'd3';
import { GameSummary } from '../models/data/game-summary.model';
import { PlayerGameStats } from '../models/data/player-game-stats.model';
import { colorScales, colors } from '../d3/shots/shots';
import { GamePossessionStats, PossessionStats } from '../models/data/possession-stats.model';

const height = 25, width = 100;
const teamLabelWidth = 8;
const strokeWidth = 0.15;

function getTextColor(eff: number) {
    if (eff < 0.35 || eff > 0.65) {
        return 'white';
    } else {
        return 'black';
    }
}

export function plotPossessions(containerSelector: string, possessionStats: GamePossessionStats) {
    const container = d3.select(containerSelector),
        svg = container.append('svg')
            .attr('viewBox', '0, 0, ' + width + ', ' + height);

    svg.append('svg:image')
        .attr('x', -10)
        .transition().duration(2000)
        .attr('x', 0)
        .attr('y', 0)
        .attr('width', 8)
        .attr('height', 8)
        .attr('xlink:href', 'assets/img/' + possessionStats.home.team + '.png');

    svg.append('svg:image')
        .attr('x', -10)
        .transition().duration(2000)
        .attr('x', 0)
        .attr('y', height * 3 / 7)
        .attr('width', 8)
        .attr('height', 8)
        .attr('xlink:href', 'assets/img/' + possessionStats.away.team + '.png');

    const xMax = Math.max(possessionStats.home.possessions + possessionStats.home.oRebs,
                        possessionStats.away.possessions + possessionStats.away.oRebs);

    drawPossessionBarsForTeam(svg, xMax, 0, possessionStats.home);
    drawPossessionBarsForTeam(svg, xMax, height * 3 / 7, possessionStats.away);

    const legendColors = svg.selectAll('legendColors').data(colors, d => d);

    legendColors.enter().append('rect')
        .attr('x', (d, i) => i + width - 20)
        .attr('y', height - 1)
        .attr('width', 1)
        .attr('height', 1)
        .attr('rx', 0.2)
        .attr('ry', 0.2)
        .style('opacity', 0.8)
        .style('fill', (d, i) => colors[i]);

    svg.append('text')
        .style('font-size', 0.75 + 'px')
        .attr('x', width - 26.5)
        .attr('y', height - 0.25)
        .text('Less Efficient');

    svg.append('text')
        .style('font-size', 0.75 + 'px')
        .attr('x', width - 8.5)
        .attr('y', height - 0.25)
        .text('More Efficient');
}

function drawPossessionBarsForTeam(svg: any, xMax: number, yStart: number, teamStats: PossessionStats) {
    let xPos = 0;
    drawPossessionBar(svg, 'Possessions', xPos, xMax, yStart, teamStats.possessions, 'black', 'white');
    xPos += teamStats.possessions;
    drawPossessionBar(svg, 'OReb', xPos, xMax, yStart, teamStats.oRebs, 'grey', 'white');

    xPos = 0;
    drawPossessionBar(svg, '2pt FGA',
                    xPos, xMax, yStart + height / 7 + strokeWidth,
                    teamStats.fg2.attempts,
                    colorScales['raw'](teamStats.fg2.eff),
                    getTextColor(teamStats.fg2.eff));
    xPos += teamStats.fg2.attempts;
    drawPossessionBar(svg, '3pt FGA',
                    xPos, xMax, yStart + height / 7 + strokeWidth,
                    teamStats.fg3.attempts,
                    colorScales['raw'](teamStats.fg3.eff),
                    getTextColor(teamStats.fg3.eff));
    xPos += teamStats.fg3.attempts;
    drawPossessionBar(svg, 'Fouls Drawn',
                    xPos, xMax, yStart + height / 7 + strokeWidth,
                    teamStats.ft.trips,
                    colorScales['raw'](teamStats.ft.eff),
                    getTextColor(teamStats.ft.eff));
    xPos += teamStats.ft.trips;
    drawPossessionBar(svg, 'Turnovers',
                    xPos, xMax, yStart + height / 7 + strokeWidth,
                    teamStats.turnovers,
                    colorScales['raw'](0),
                    getTextColor(0));
}

function drawPossessionBar(svg: any, type: string, x: number, xMax, y: number, amount: number, barColor: string, textColor: string) {
    svg.selectAll('.possessionBar')
        .data([{}]).enter()
        .append('rect')
        .attr('x', width + 10)
        .attr('rx', 0.2)
        .attr('ry', 0.2)
        .attr('y', y + strokeWidth)
        .attr('width', (amount / xMax) * (width - teamLabelWidth - 2))
        .attr('height', height / 7)
        .attr('fill', barColor)
        .style('stroke', 'black')
        .style('opacity', 0.6)
        .style('stroke-width', strokeWidth)
        .transition().duration(2000)
        .attr('x', teamLabelWidth + ((x / xMax) * (width - 2 - teamLabelWidth)));

    svg.selectAll('.possessionText')
        .data([{}]).enter()
        .append('text')
        .attr('x', width + 10)
        .attr('y', y + (height / 10) + strokeWidth)
        .text(amount + ' ' + type)
        .attr('font-size', '1px')
        .attr('fill', textColor)
        .attr('alignment-baseline', 'middle')
        .transition().duration(2000)
        .attr('x', teamLabelWidth + ((x / xMax) * (width - 2 - teamLabelWidth)));
}