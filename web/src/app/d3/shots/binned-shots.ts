import * as d3 from 'd3';
import { yScale, colorScales, leageZoneInfo, colors } from './shots';
import { Shot } from '../../models/data/shot.model';
import { ShotGridCell } from '../../models/util/shot-grid-cell.model';
import { ShotGrid } from '../../models/util/shot-grid.model';

export function calcShotGrid(shotData: Shot[], isTeam: boolean): ShotGrid {
  const shotGrid: ShotGridCell[] = [],
    teamZoneInfo = {};

  const maxPct = 10,
      min = 1;

  shotData.forEach(shot => {
    const x = Math.round(shot.x / 10),
      y = Math.round(shot.y / 10);

    if (shotGrid.findIndex(sc => (sc.x === x && sc.y === y)) === -1) {

      const shotsAtCoords = shotData.filter(s => Math.floor(s.x / 10) === x && Math.floor(s.y / 10) === y);

      if (!teamZoneInfo[shot.zone]) {
        teamZoneInfo[shot.zone] = {};
      }

      if (!teamZoneInfo[shot.zone][shot.side]) {

        const shotsInZoneSide = shotData.filter(s => (s.zone === shot.zone) && (s.side === shot.side)),
          makesInZoneSide = shotsInZoneSide.filter(s => s.made);

        teamZoneInfo[shot.zone][shot.side] = {
          'fg': makesInZoneSide.length / shotsInZoneSide.length,
          'efg': makesInZoneSide.length / shotsInZoneSide.length * shot.value / 2,
          'zone_rel_fg': makesInZoneSide.length / shotsInZoneSide.length - leageZoneInfo[shot.zone][shot.side]['pct_make'],
          'zone_rel_vol': ((shotsInZoneSide.length / shotData.length) - leageZoneInfo[shot.zone][shot.side]['pct_take']) * 20 + 1
        }

      }

      shotGrid.push(
        new ShotGridCell(
          x, y,
          Math.min(1, (shotsAtCoords.length / shotData.length * 700)), 0,
          shot.zone, shot.side
        )
      )

    }

  });

  return new ShotGrid(shotGrid, teamZoneInfo);
}

export function plot_shots(container_selector: string, shotGrid: ShotGrid) {
  const svg = d3.select(container_selector).select('.court'),
    shotsGroup = svg.select('.shots');

  let shots = shotsGroup.selectAll('.shot').data([]);
  shots.exit().remove();

  shots = shotsGroup.selectAll('.shot')
    .data(shotGrid.cells, d => d);

  shots.enter().append('rect')
    .classed('shot', true)
    .attr('x', (d) => 24.5 - d.x - (d.raw_vol / 2))
    .attr('y', (d) => yScale(d.y) - 5 - (d.raw_vol / 2))
    .attr('width', (d) => d.raw_vol)
    .attr('height', (d) => d.raw_vol)
    .attr('rx', 0.2)
    .attr('ry', 0.2)
    .style('fill', (d) => colorScales['rel'](shotGrid.zoneSideInfo[d.zone][d.side]['zone_rel_fg']));
}

export function updateShotSizes(adj: boolean) {
  d3.select('.shots').selectAll('.shot')//.transition().duration(1000)
    .attr('x', (d) => 24.5 - d.x - ((adj ? d.adj_vol : d.raw_vol) / 2))
    .attr('y', (d) => yScale(d.y) - 5 - ((adj ? d.adj_vol : d.raw_vol) / 2))
    .attr('width', (d) => adj ? d.adj_vol : d.raw_vol)
    .attr('height', (d) => adj ? d.adj_vol : d.raw_vol);
}

export function updateShotColor(measureType: string, effInfo: {}) {
  d3.select('.shots').selectAll('.shot')//.transition().duration(1000)
    .style('fill', (d) => measureType === 'rel_fg' ? colorScales['rel'](effInfo[d.zone][d.side]['zone_rel_fg']) : colorScales['raw'](effInfo[d.zone][d.side][measureType]));
}

export function add_legend(container_selector: string): void {

  const court = d3.select(container_selector).select('.court');

  court.selectAll('.legend').remove();
  const legendElement = court.append('g').attr('class', 'legend');

  const legendColors = legendElement.selectAll('legendColors').data(colors, d => d);

  legendColors.enter().append('rect')
    .attr('x', (d, i) => i + 1)
    .attr('y', yScale(-1))
    .attr('width', 1)
    .attr('height', 1)
    .attr('rx', 0.2)
    .attr('ry', 0.2)
    .style('fill', (d, i) => colors[i]);

  legendElement.append('text')
    .style('font-size', 0.75 + 'px')
    .attr('x', 0)
    .attr('y', yScale(-3))
    .text('Less Efficient');

  legendElement.append('text')
    .style('font-size', 0.75 + 'px')
    .attr('x', 8)
    .attr('y', yScale(-3))
    .text('More Efficient');

  const sizes = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
  const legendSizes = legendElement.selectAll('legendSizes').data(sizes, d => d);

  let x_shift = 0;

  legendSizes.enter().append('rect')
    .attr('x', (d, i) => {
      x_shift += d + 0.3;
      return x_shift - d + 40;
    })
    .attr('y', (d, i) => yScale(-1) + (1 - d) / 2)
    .attr('width', d => d)
    .attr('height', d => d)
    .attr('rx', 0.2)
    .attr('ry', 0.2)
    .style('fill', 'white');

  legendElement.append('text')
    .style('font-size', 0.75 + 'px')
    .attr('x', 38)
    .attr('y', yScale(-3))
    .text('Fewer Shots');

  legendElement.append('text')
    .style('font-size', 0.75 + 'px')
    .attr('x', 45)
    .attr('y', yScale(-3))
    .text('More Shots');
}