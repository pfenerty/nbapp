import { yScale } from './shots';
import { ShotType } from './shot-type.model';
import { GameEvent } from 'src/app/models/data/game-event.model';
import { ShotChartSettings } from 'src/app/components/viz/shots/game-shot-settings/game-shot-settings.component';

const shotRadius = 0.5;
const shotStrokeWidth = 0.2;
const shotOpacity = 0.6;

const styleMap = {
  'game': {
    'fill': {
      'make': 'red',
      'miss': 'none',
      'assist': 'blue',
      'foul': 'black',
      'tov': 'none'
    },
    'stroke': {
      'make': 'none',
      'miss': 'red',
      'assist': 'none',
      'foul': 'none',
      'tov': 'blue',
      'andOne': 'black'
    }
  },
  'view': {
    'fill': {
      'make': 'red',
      'miss': 'blue',
      'assist': 'yellow',
      'foul': 'green',
      'tov': 'black'
    },
    'stroke': {
      'make': 'none',
      'miss': 'none',
      'assist': 'none',
      'foul': 'none',
      'tov': 'none'
    }
  }
}

export function drawScatterShots(svg: any, shotData: GameEvent[], settings: ShotChartSettings) {

  const shotsGroup = svg.select('.shots');

  const shots = shotsGroup.selectAll('.shot')
    .data(shotData, function (d) {
      return [d.x, d.y, d.player1, d.player2];
    });

  shots.enter()
    .append('circle')
    .classed('shot', true)
    .attr('cx', d => 25.5 - d.x / 10)
    .attr('cy', d => yScale(d.y / 10) - 5)
    .attr('r', 0)
    .style('opacity', shotOpacity)
    .style('fill', d => getShotFill(d, settings))
    .style('stroke', d => getShotStroke(d, settings))
    .style('stroke-width', shotStrokeWidth)
    .transition()
    .duration(2000)
    .attr('r', shotRadius);


  shots.exit()
    .transition()
    .duration(1000)
    .attr('r', 0)
    .remove();

  shots.transition().duration(1000)
    .style('fill', d => getShotFill(d, settings))
    .style('stroke', d => getShotStroke(d, settings));
}

function getShotFill(shot: GameEvent, settings: ShotChartSettings) {
  const fillMap = styleMap[settings.view]['fill'];
  if (shot.type === 'shot' && shot.shot_value === 0) {
    return fillMap['miss'];
  } else if (shot.type === 'tov') {
    return fillMap['tov'];
  } else if (shot.type === 'shot' && shot.shot_value > 0 && shot.and_one_value == null ) {
    if (settings.isTeam) {
      if (shot.player2 !== null && settings.showAssists) {
        return fillMap['assist'];
      } else {
        return fillMap['make'];
      }
    } else {
      if (shot.player2 === settings.selection) {
        return fillMap['assist'];
      } else {
        return fillMap['make'];
      }
    }
  } else if (shot.type === 'shot' && shot.and_one_value != null) {
    if (settings.isTeam || shot.player1 === settings.selection) {
      return fillMap['make'];
    } else {
      return fillMap['assist'];
    }
  } else if (shot.type === 'ft') {
    return fillMap['foul'];
  } else {
    return 'none';
  }
}

function getShotStroke(shot: GameEvent, settings: ShotChartSettings) {
  const strokeMap = styleMap[settings.view]['stroke'];
  if (shot.type === 'shot' && shot.shot_value === 0) {
    return strokeMap['miss'];
  } else if (shot.type === 'shot' && shot.fts_taken > 0) {
    return strokeMap['andOne'];
  } else if(shot.type === 'tov') {
    return strokeMap['tov'];
  } else {
    return 'none';
  }
}

class ShotLegendEntry {
  shotType: ShotType;
  text: string;
  width: number;
}

export function drawScatterLegend(svg: any, settings: ShotChartSettings) {

  svg.selectAll('.legend').remove();
  const legendElement = svg.append('g').attr('class', 'legend');
  const legendHeight = 48,
    legendTextHeightOffset = 0.4;

  let legendWidth = 0;
  if (settings.showShots) { legendWidth += 9.5; }
  if (settings.showAssists) { legendWidth += 5; }
  if (settings.showFouls) { legendWidth += 8; }
  if (settings.showShots || settings.showFouls) { legendWidth += 4.5; }
  if (settings.showTov) { legendWidth += 6; }

  let x = 25 - (legendWidth / 2);


  const fillMap = styleMap[settings.view]['fill'];
  const strokeMap = styleMap[settings.view]['stroke'];

  if (settings.showShots) {
    legendElement.append('circle')
      .attr('cx', x)
      .attr('cy', legendHeight)
      .attr('r', shotRadius)
      .style('opacity', 1)
      .style('fill', fillMap['make']);

    legendElement.append('text')
      .attr('x', x + 0.6)
      .attr('y', legendHeight + legendTextHeightOffset)
      .text('Make')
      .style('font-size', '.08rem')
      .style('text-anchor', 'start');

    x += 5;
  }

  if (settings.showShots || settings.showFouls) {
    legendElement.append('circle')
      .attr('cx', x)
      .attr('cy', legendHeight)
      .attr('r', shotRadius)
      .style('opacity', 1)
      .style('fill', fillMap['make'])
      .style('stroke', 'black')
      .style('stroke-width', shotStrokeWidth);

    legendElement.append('text')
      .attr('x', x + 0.6)
      .attr('y', legendHeight + legendTextHeightOffset)
      .text('And1')
      .style('font-size', '.08rem')
      .style('text-anchor', 'start');

    x += 4.5;
  }

  if (settings.showShots) {
    legendElement.append('circle')
      .attr('cx', x)
      .attr('cy', legendHeight)
      .attr('r', shotRadius)
      .style('opacity', 1)
      .style('fill', fillMap['miss'])
      .style('stroke', strokeMap['miss'])
      .style('stroke-width', shotStrokeWidth);

    legendElement.append('text')
      .attr('x', x + 0.6)
      .attr('y', legendHeight + legendTextHeightOffset)
      .text('Miss')
      .style('font-size', '.08rem')
      .style('text-anchor', 'start');

    x += 4.5;
  }

  if (settings.showAssists) {
    legendElement.append('circle')
      .attr('cx', x)
      .attr('cy', legendHeight)
      .attr('r', shotRadius)
      .style('opacity', 1)
      .style('fill', fillMap['assist']);

    legendElement.append('text')
      .attr('x', x + 0.6)
      .attr('y', legendHeight + legendTextHeightOffset)
      .text('Assist')
      .style('font-size', '.08rem')
      .style('text-anchor', 'start');

    x += 5;
  }

  if (settings.showFouls) {
    legendElement.append('circle')
      .attr('cx', x)
      .attr('cy', legendHeight)
      .attr('r', shotRadius)
      .style('opacity', 1)
      .style('fill', fillMap['foul'])
      .style('stroke', strokeMap['foul'])
      .style('stroke-width', shotStrokeWidth);

    legendElement.append('text')
      .attr('x', x + 0.6)
      .attr('y', legendHeight + legendTextHeightOffset)
      .text('Foul Drawn')
      .style('font-size', '.08rem')
      .style('text-anchor', 'start');

    x += 8;
  }

  if (settings.showTov) {
    legendElement.append('circle')
      .attr('cx', x)
      .attr('cy', legendHeight)
      .attr('r', shotRadius)
      .style('opacity', 1)
      .style('fill', fillMap['tov'])
      .style('stroke', strokeMap['tov'])
      .style('stroke-width', shotStrokeWidth);

    legendElement.append('text')
      .attr('x', x + 0.6)
      .attr('y', legendHeight + legendTextHeightOffset)
      .text('Turnover')
      .style('font-size', '.08rem')
      .style('text-anchor', 'start');
  }
}
