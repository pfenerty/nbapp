import * as d3 from 'd3';
import { Shot } from 'src/app/models/data/shot.model';
import { ShotChartSettings } from 'src/app/components/viz/shots/game-shot-settings/game-shot-settings.component';
import { GameEvent } from 'src/app/models/data/game-event.model';

export const yScale = d3.scaleLinear().domain([0, 47]).rangeRound([47, 0]);
export const colors = ['#67001f', '#b2182b', '#d6604d', '#f4a582', '#fddbc7', '#f7f7f7',
                      '#d1e5f0', '#92c5de', '#4393c3', '#2166ac', '#053061'].reverse();


export const colorScales = {
  'raw': d3.scaleLinear().domain([0.40, 0.70]).range(['#0000fc', '#fc0000']),
  'pts': d3.scaleLinear().domain([90, 120]).range(['#0000fc', '#fc0000']),
  'rel': d3.scaleLinear().domain([-0.05, 0.05]).range(['#0000fc', '#fc0000']),
  'ast': d3.scaleLinear().domain([0.5, 0.7]).range(['#0000fc', '#fc0000'])
};

export const leageZoneInfo2 = {
  'Rim': {'pct_take': 0.32, 'pct_make': 0.61},
  'ShortMid': {'pct_take': 0.14, 'pct_make': 0.4},
  'LongMid': {'pct_take': 22.8, 'pct_make': 0.4},
  'Three': {'pct_take': 0.30, 'pct_make': 0.35},
  'BackCourt': {'pct_take': 0.002, 'pct_make': 0.02}
};

export const leageZoneInfo = {
  'Corner3': {
    'R': { 'pct_take': 0.035545319214672924, 'pct_make': 0.3912067766609773, 'size': 2626 },
    'L': { 'pct_take': 0.0378260258966941, 'pct_make': 0.3821135796602115, 'size': 2708 }
  },
  'Top3': {
    'C': { 'pct_take': 0.05886245541106087, 'pct_make': 0.34841197129332724, 'size': 7878 },
    'BC': { 'pct_take': 0.00028087520714546526, 'pct_make': 0.056, 'size': 233 },
    'LC': { 'pct_take': 0.08557593461225178, 'pct_make': 0.3494072391655398, 'size': 9858 },
    'RC': { 'pct_take': 0.082676178973682, 'pct_make': 0.351280100016307, 'size': 9458 }
  },
  'Mid': {
    'RC': { 'pct_take': 0.037406960087633064, 'pct_make': 0.39423336837362966, 'size': 6733 },
    'C': { 'pct_take': 0.04466028143695756, 'pct_make': 0.4103544564916606, 'size': 8612 },
    'R': { 'pct_take': 0.054034772350644605, 'pct_make': 0.3974217694147001, 'size': 12343 },
    'L': { 'pct_take': 0.05751987192090554, 'pct_make': 0.3962536867394574, 'size': 12573 },
    'LC': { 'pct_take': 0.034599331517007, 'pct_make': 0.3965450058449149, 'size': 6470 }
  },
  'ShortMid': {
    'C': { 'pct_take': 0.12948796449737382, 'pct_make': 0.4003418536449928, 'size': 14248 },
    'R': { 'pct_take': 0.009584585568631857, 'pct_make': 0.4087445785957098, 'size': 1808 },
    'L': { 'pct_take': 0.009199224784428278, 'pct_make': 0.4075476306790425, 'size': 1752 }
  },
  'RA': {
    'C': { 'pct_take': 0.32044378282728986, 'pct_make': 0.6111233823833615, 'size': 3617 }
  },
  'Heave': {
    'BC': { 'pct_take': 0.002296435693621324, 'pct_make': 0.02299412915851272, 'size': 2006 }
  }
};

export function drawCourt(svg: any) {

  // Append the outer paint rectangle
  svg.append('g')
    .classed('court-paint', true)
    .append('rect')
    .attr('width', 16)
    .attr('height', 19)
    .attr('x', 25)
    .attr('transform', 'translate(' + -8 + ',' + 0 + ')')
    .attr('y', yScale(19));

  // Append inner paint lines
  svg.append('g')
    .classed('inner-court-paint', true)
    .append('line')
    .attr('x1', 19)
    .attr('x2', 19)
    .attr('y1', yScale(19))
    .attr('y2', yScale(0));
  svg.append('g')
    .classed('inner-court-paint', true)
    .append('line')
    .attr('x1', 31)
    .attr('x2', 31)
    .attr('y1', yScale(19))
    .attr('y2', yScale(0));

  // Append foul circle
  // Add clipPaths w/ rectangles to make the 2 semi-circles with our desired styles
  const solidFoulCircle = svg.append('g').classed('foul-circle solid', true);
  solidFoulCircle.append('defs').append('clipPath')
    .attr('id', 'cut-off-bottom')
    .append('rect')
    .attr('width', 100)
    .attr('height', 6)
    .attr('x', 23)
    .attr('y', 27.9)
    .attr('transform', 'translate(' + -6 + ',' + -6 + ')');

  solidFoulCircle.append('circle')
    .attr('cx', 25)
    .attr('cy', yScale(19))
    .attr('r', 6)
    .attr('clip-path', 'url(#cut-off-bottom)');

  // Add backboard and rim;
  svg.append('g').classed('backboard', true)
    .append('line')
    .attr('x1', 22)
    .attr('x2', 28)
    .attr('y1', yScale(4))
    .attr('y2', yScale(4));

  svg.append('g').classed('rim', true)
    .append('circle')
    .attr('cx', 25)
    .attr('cy', yScale(4.75))
    .attr('r', .75);

  // Add restricted area -- a 4ft radius circle from the center of the rim
  const restrictedArea = svg.append('g').classed('restricted-area', true);
  restrictedArea.append('defs').append('clipPath')
    .attr('id', 'restricted-cut-off')
    .append('rect')
    .attr('width', 10)
    .attr('height', 6.5)
    .attr('x', 24)
    .attr('y', 40)
    .attr('transform', 'translate(' + -4 + ',' + -4 + ')');
  restrictedArea.append('circle')
    .attr('cx', 25)
    .attr('cy', yScale(4.75))
    .attr('r', 4)
    .attr('clip-path', 'url(#restricted-cut-off)')
    .style('-webkit-clip-path', 'url(#restricted-cut-off)');

  // Add 3 point arc
  const threePointArea = svg.append('g').classed('three-point-area', true);
  threePointArea.append('defs').append('clipPath')
    .attr('id', 'three-point-cut-off')
    .append('rect')
    .attr('width', 44)
    .attr('height', 23.75)
    .attr('x', 25)
    .attr('y', yScale(6))
    .attr('transform', 'translate(' + -22 + ',' + -23.75 + ')');
  threePointArea.append('circle')
    .attr('cx', 25)
    .attr('cy', yScale(4.75))
    .attr('r', 23.75)
    .attr('clip-path', 'url(#three-point-cut-off)')
    .style('-webkit-clip-path', 'url(#three-point-cut-off)');
  threePointArea.append('line')
    .attr('x1', 3.12)
    .attr('x2', 3.12)
    .attr('y1', 32.8)
    .attr('y2', yScale(0));
  threePointArea.append('line')
    .attr('x1', 46.9)
    .attr('x2', 46.9)
    .attr('y1', 32.8)
    .attr('y2', yScale(0));

  // Append baseline
  svg.append('g')
    .classed('court-baseline', true)
    .append('line')
    .attr('x1', 0)
    .attr('x2', 50)
    .attr('y1', yScale(0))
    .attr('y2', yScale(0));

  svg.append('g').classed('shots', true);
}

export function filterEvents(events: Array<GameEvent>, settings: ShotChartSettings) {
  if (settings.isTeam) {
    return events.filter(s =>
      (s.type === 'shot' && settings.showShots) || 
      (s.type === 'shot' && settings.showAssists && s.player2 !== null) ||
      (s.type === 'ft' && settings.showFouls) ||
      (s.type === 'tov' && settings.showTov)
    );
  } else {
    return events.filter(s =>
      (s.type === 'shot' && settings.showShots && s.player1 === settings.selection) ||
      (s.type === 'shot' && settings.showAssists && s.player2 === settings.selection) ||
      (s.type === 'ft' && settings.showFouls && s.player1 === settings.selection) ||
      (s.type === 'tov' && settings.showTov && s.player1 === settings.selection)
    )
  }
}

export function filterOnCourtEvents(events: Array<GameEvent>, settings: ShotChartSettings, isHome: boolean) {
  if (settings.isTeam) {
    return events.filter((e: GameEvent) => e.type !== 'reb');
  } else {
    return events.filter((e: GameEvent) =>
    [e.off_player1, e.off_player2, e.off_player3, e.off_player4, e.off_player5].indexOf(settings.selection) > -1);
  }
}
