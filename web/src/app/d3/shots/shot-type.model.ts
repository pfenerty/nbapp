export class ShotType {
    fill: string;
    stroke: string;

    constructor(fill: string, stroke: string) {
        this.fill = fill;
        this.stroke = stroke;
    }
}
