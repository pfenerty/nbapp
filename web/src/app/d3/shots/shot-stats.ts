import * as d3 from 'd3';
import { GameEvent } from 'src/app/models/data/game-event.model';
import { ShotChartSettings } from 'src/app/components/viz/shots/game-shot-settings/game-shot-settings.component';


const shotStatsContianerWidth = 50, shotStatsContainerHeight = 10;

const zoneInfo = {
    'Rim': { 'freq': 32, 'eff': 61 },
    'ShortMid': { 'freq': 14, 'eff': 40 },
    'LongMid': { 'freq': 22.8, 'eff': 40 },
    'Three': { 'freq': 30, 'eff': 53 },
    'BackCourt': { 'freq': 2, 'eff': 2 },
    'Assists': { 'team': 50, 'player': 20 },
    'Tov': { 'team': 15, 'player': 15 },
    'Usage': { 'team': 85, 'player': 20 },
    'Points': { 'team': 112, 'player': 20, 'eff': 55 },
}

const shotStatTextStyle = {
    'size': 0.15,
    'weight': 'bolder',
    'anchor': 'middle',
    'stroke': 'black',
    'strokeWidth': '0.05'
};

export class Zone {
    name: string;
    column: number;
    height: string;

    constructor(name: string, column: number, height: string) {
        this.name = name;
        this.column = column;
        this.height = height;
    }
}

export class ShotStat {
    id: string;
    type: string;
    zone: string;
    textValue: number;
    colorValue: number;
    x: number;
    y: number;

    constructor(id: string, type: string, zone: string, textValue: number, colorValue: number, x: number, y: number) {
        this.id = id;
        this.type = type;
        this.zone = zone;
        this.textValue = textValue;
        this.colorValue = colorValue;
        this.x = x;
        this.y = y;
    }
}

export class Line {
    num: number;
    x: number;

    constructor(num: number, x: number) {
        this.num = num;
        this.x = x;
    }
}

export function drawShotStatsContainer(containerSelector: string) {
    d3.select(containerSelector).append('svg')
        .attr('viewBox', '0, 0, ' + shotStatsContianerWidth + ', ' + shotStatsContainerHeight)
        .classed('shotStatsSVG', true);
}


export function drawShotStats(svg: any,
    events: Array<GameEvent>,
    onCourtEvents: Array<GameEvent>,
    settings: ShotChartSettings) {

    let numStatsCols = 1;
    if (settings.showShots || settings.showFouls) { numStatsCols += 7; }
    if (settings.showAssists || settings.showTov) { numStatsCols += 1; }
    const colWidth = shotStatsContianerWidth / numStatsCols;

    let colIndex = 1;

    //#region
    // Draw Shots & Fouls label based on what types are currently shown
    let labelData = [];
    if (settings.showShots && settings.showFouls) {
        labelData = ['Shots', '&', 'Fouls Drawn'];
    } else if (settings.showShots) {
        labelData = ['Shots'];
    } else if (settings.showFouls) {
        labelData = ['Fouls Drawn'];
    }

    const shotsLabel = svg.selectAll('.shotsLabel').data(labelData, d => d);

    shotsLabel.enter()
        .append('text')
        .text(d => d)
        .classed('shotsLabel', true)
        .attr('x', -20)
        .style('font-size', '0.075rem')
        .style('text-anchor', 'middle')
        .transition().duration(1000)
        .attr('x', (d, i) => {
            const base = colWidth * colIndex;
            const offset = labelData.length === 1 ? 0 : (i === 0 ? -0.5 : (i === 1 ? 1.9 : 0));
            return base + offset;
        })
        .attr('y', (d, i) => {
            const base = shotStatsContainerHeight / 4 + 0.5;
            const offset = labelData.length === 1 ? 0 : (i <= 1 ? -0.5 : 0.5);
            return base + offset;
        });

    shotsLabel.exit()
        .transition().duration(1000)
        .attr('x', -20)
        .remove();

    shotsLabel
        .transition().duration(1000)
        .attr('x', (d, i) => {
            const base = colWidth * colIndex;
            const offset = labelData.length === 1 ? 0 : (i === 0 ? -0.5 : (i === 1 ? 1.9 : 0));
            return base + offset;
        })
        .attr('y', (d, i) => {
            const base = shotStatsContainerHeight / 4 + 0.5;
            const offset = labelData.length === 1 ? 0 : (i <= 1 ? -0.5 : 0.5);
            return base + offset;
        });

    //#endregion

    //#region
    // Show EFG% or TS% depending on what types are shown
    labelData = [];
    if (settings.showFouls) {
        labelData = ['TS%'];
    } else if (settings.showShots) {
        labelData = ['EFG%'];
    }

    const effLabel = svg.selectAll('.effLabel').data(labelData, d => d);

    effLabel.enter()
        .append('text')
        .text(d => d)
        .classed('effLabel', true)
        .attr('x', -20)
        .style('font-size', '0.075rem')
        .style('text-anchor', 'middle')
        .transition().duration(1000)
        .attr('x', colWidth * colIndex)
        .attr('y', 3 * shotStatsContainerHeight / 4 + 0.5);

    effLabel.exit()
        .transition().duration(1000)
        .attr('x', -20)
        .remove();

    effLabel
        .transition().duration(1000)
        .attr('x', colWidth * colIndex)
        .attr('y', 3 * shotStatsContainerHeight / 4 + 0.5);
    //#endregion

    if (settings.showShots || settings.showFouls) { colIndex++; }

    const shotStats: Array<ShotStat> = [];
    const zones: Array<Zone> = [];
    const lines: Array<Line> = [];
    let lineIndex = 0;

    const shotsAndFouls = events.filter(
            (e: GameEvent) =>
                (e.type === 'shot' || e.type === 'ft') &&
                e.player2 !== settings.selection &&
                (settings.isTeam || e.player1 === settings.selection)
    ),
        assists = events.filter(
            (e: GameEvent) =>
                ((settings.isTeam && e.player2 !== null) || (e.player2 === settings.selection))
                && e.type === 'shot'
    ),
        tov = events.filter(
            (e: GameEvent) =>
                e.type === 'tov' &&
                (settings.isTeam || e.player1 === settings.selection)
    );

    const usagePct = shotsAndFouls.length / onCourtEvents.length * 100,
        assistPct = assists.length / onCourtEvents.filter(e => e.type === 'shot' && e.shot_value !== 0).length * 100,
        tovPct = tov.length / (shotsAndFouls.length + assists.length + tov.length) * 100;

    const filteredShotsAndFouls = events.filter(
            (e: GameEvent) =>
                ((e.type === 'shot' && settings.showShots) || (e.type === 'ft' && settings.showFouls)) &&
                e.player2 !== settings.selection &&
                (e.player1 === settings.selection || settings.isTeam));

    const toalPoints = filteredShotsAndFouls.length === 0 ? 0 :
                        filteredShotsAndFouls.map(e => Math.abs(e.score_change)).reduce((a, b) => a + b),
        totalEff = filteredShotsAndFouls.length === 0 ? 0 : toalPoints / filteredShotsAndFouls.length * 50;

    if (settings.showShots || settings.showFouls) {
        const zoneNames = ['Rim', 'ShortMid', 'LongMid', 'Three'];
        for (let i = 0; i < zoneNames.length; i++) {
            const zone = zoneNames[i],
                zoneShots = filteredShotsAndFouls.filter(e => e.zone === zone),
                numShotsInZone = zoneShots.length,
                pctShotsInZone = filteredShotsAndFouls.length === 0 ? 0 : numShotsInZone / filteredShotsAndFouls.length * 100,
                effInZone = zoneShots.length === 0 ? 0 :
                            zoneShots.map((s: GameEvent) => Math.abs(s.score_change))
                                    .reduce((a, b) => a + b) / zoneShots.length * 50;

            zones.push(new Zone(zone, colIndex, 'top'));
            shotStats.push(
                new ShotStat(
                    zone + 'freq', 'freq', zone, numShotsInZone, pctShotsInZone,
                    colWidth * colIndex, shotStatsContainerHeight / 4 + 1)
            );
            shotStats.push(
                new ShotStat(zone + 'eff', 'eff', zone, effInZone, effInZone,
                colWidth * colIndex, 3 * shotStatsContainerHeight / 4 + 1)
            );
            colIndex++;
        }
        lines.push(new Line(lineIndex, (colIndex - 0.5) * colWidth));
        lineIndex++;

        zones.push(new Zone('Total', colIndex, 'top'));
        zones.push(new Zone('', colIndex, 'bottom'));

        shotStats.push(
            new ShotStat(
                'usage', settings.isTeam ? 'team' : 'player', 'Usage', filteredShotsAndFouls.length, usagePct,
                colWidth * colIndex, shotStatsContainerHeight / 4 + 1)
        );
        shotStats.push(
            new ShotStat(
                'total-eff', 'eff', 'Points', totalEff, totalEff,
                colWidth * colIndex, 3 * shotStatsContainerHeight / 4 + 1)
        );

        colIndex++;
        lines.push(new Line(lineIndex, (colIndex - 0.5) * colWidth));
        lineIndex++;
    }

    if (settings.showAssists) {
        zones.push(new Zone('Assist', colIndex, settings.showTov ? 'top' : 'middle'));
        const numAssists = assists.length;
        const y = (shotStatsContainerHeight / 4) * (settings.showTov ? 1 : 2) + 1;
        shotStats.push(new ShotStat('ast', settings.isTeam ? 'team' : 'player', 'Assists', numAssists, assistPct, colWidth * colIndex, y));
    }

    if (settings.showTov) {
        zones.push(new Zone('Tov', colIndex, settings.showAssists ? 'bottom' : 'middle'));
        const numTov = tov.length;
        let y = (shotStatsContainerHeight / 4) * (settings.showAssists ? 3 : 2) + 1;
        shotStats.push(new ShotStat('tov', settings.isTeam ? 'team' : 'player', 'Tov', numTov, tovPct, colWidth * colIndex, y));
    }

    if (settings.showAssists || settings.showTov) {
        colIndex++;
        if (settings.showShots || settings.showFouls) {
            lines.push(new Line(lineIndex, (colIndex - 0.5) * colWidth));
            lineIndex++;
        }
    }

    if (settings.showShots || settings.showFouls) {
        zones.push(new Zone('Points', colIndex, 'middle'));
        shotStats.push(
            new ShotStat('points', settings.isTeam ? 'team' : 'player', 'Points', toalPoints, toalPoints,
            colWidth * colIndex, shotStatsContainerHeight / 2 + 1)
        );
    }

    //#region
    // Add Labels for zones
    const zoneLabels = svg.selectAll('.zoneLabel').data(zones, d => d.name);

    zoneLabels.enter()
        .append('text')
        .text(d => d.name)
        .classed('zoneLabel', true)
        .attr('x', d => colWidth * d.column)
        .attr('y', -20)
        .style('font-size', '0.075rem')
        .style('text-anchor', 'middle')
        .transition().duration(1000)
        .attr('y', (d: Zone) => {
            return (d.height === 'top' ? 0 : d.height === 'middle' ? 1 :
                    d.height === 'bottom' ? 2 : 0) * (shotStatsContainerHeight / 4) + 1;
        });

    zoneLabels.exit()
        .transition().duration(1000)
        .attr('y', -20)
        .remove();

    zoneLabels
        .transition().duration(1000)
        .attr('x', d => colWidth * d.column)
        .attr('y', (d: Zone) => {
            return (d.height === 'top' ? 0 : d.height === 'middle' ? 1 :
                    d.height === 'bottom' ? 2 : 0) * (shotStatsContainerHeight / 4) + 1;
        });
    //#endregion

    //#region
    // Add Zone Stats
    const zoneStats = svg.selectAll('.zoneStat').data(shotStats, d => [d.id]);

    zoneStats.enter()
        .append('text')
        .text((d: ShotStat) => Math.round(d.textValue))
        .classed('zoneStat', true)
        .classed('mono', true)
        .attr('x', (d: ShotStat) => d.x)
        .attr('y', shotStatsContainerHeight + 10)
        .style('font-size', shotStatTextStyle.size + 'rem')
        .style('font-weight', shotStatTextStyle.weight)
        .style('text-anchor', shotStatTextStyle.anchor)
        .style('fill', (d: ShotStat) => {
            const domainCenter = zoneInfo[d.zone][d.type];
            let domain = [domainCenter - 10, domainCenter + 10];
            if (d.zone === 'Tov') { domain = domain.reverse() }
            const colorScale = d3.scaleLinear().domain(domain).range(['#0000fc', '#fc0000']);
            return colorScale(d.colorValue);
        })
        .transition().duration(1000)
        .attr('y', (d: ShotStat) => d.y);

    zoneStats.exit()
        .transition().duration(1000)
        .attr('y', shotStatsContainerHeight + 10)
        .remove();

    zoneStats
        .transition().duration(1000)
        .attr('x', (d: ShotStat) => d.x)
        .attr('y', (d: ShotStat) => d.y)
        .style('fill', (d: ShotStat) => {
            const domainCenter = zoneInfo[d.zone][d.type];
            let domain = [domainCenter - 10, domainCenter + 10];
            if (d.zone === 'Tov') { domain = domain.reverse() }
            const colorScale = d3.scaleLinear().domain(domain).range(['#0000fc', '#fc0000']);
            return colorScale(d.colorValue);
        })
        .text(d => Math.round(d.textValue));

    const seperators = svg.selectAll('.seperator').data(lines, d => [d.num]);

    seperators.enter()
        .append('line')
        .attr('x1', (d: Line) => d.x)
        .attr('x2', (d: Line) => d.x)
        .attr('y1', 0)
        .attr('y2', 0)
        .style('stroke-width', 0.1)
        .attr('stroke-opacity', 0.5)
        .classed('seperator', true)
        .style('stroke', 'black')
        .style('fill', 'none')
        .transition().duration(1000)
        .attr('y2', shotStatsContainerHeight - 1);

    seperators.exit()
        .transition().duration(1000)
        .attr('y2', 0)
        .remove();

    seperators
        .transition().duration(1000)
        .attr('x1', (d: Line) => d.x)
        .attr('x2', (d: Line) => d.x);
    //#endregion
}