import * as d3 from 'd3';
import { TEAM_COLORS } from '../enums';

const width = 100,
    height = 45;

const fontSize = 0.75;

export function plotConsistency(data) {
    const container = d3.select('#consistencyContainer');
    const svg = container.append('svg')
        .attr('viewBox', '0, 0, ' + width + ', ' + height);

    const maxPlayerNameLength = Math.floor((width / data.length) * 2);

    svg.selectAll('.playerLabel')
        .data(data)
        .enter()
        .append('text')
        .text(d => getDisplayName(d.column, maxPlayerNameLength))
        .attr('x', (d, i) => (i + 1) * (width / (data.length + 0.5)))
        .attr('y', d => height - 5)
        .style('font-size', fontSize + 'px')
        .style('text-anchor', 'middle');

    const data_points = [];
    data.forEach((column, index) => {
        column.list.forEach(game => {
            data_points.push(
                {
                    'x': index,
                    'y': game,
                    'num': column.list.filter(g => g === game).length,
                    'inst': data_points.filter(d => d.x === index && d.y === game).length + 1,
                    'color': TEAM_COLORS[column.team][0]
                }
            );
        });
    });
    const maxStat = data_points.map(x => x.y).reduce((a, b) => a > b ? a : b),
        minStat = data_points.map(x => x.y).reduce((a, b) => a < b ? a : b);

    const y = d3.scaleLinear().range([2, height - 7]);
    y.domain([maxStat, minStat]);

    const radius = Math.max(Math.min(15 / (y.domain()[0] - y.domain()[1]), 0.3), 0.2);

    svg.selectAll('.gamePoint')
        .data(data_points)
        .enter().append('circle')
        .attr('cx', d => {
            const playerShift = d.x,
                instShift = (d.num / 2) - d.inst,
                xScale = (width / (data.length + 0.5));
            return ((playerShift + 1) * xScale) + (instShift * radius * 2);
        })
        .attr('cy', d => y(d.y))
        .attr('r', radius)
        .style('fill', d => d.color);

    svg.append('g')
        .attr('transform', 'translate(7,-0.5)')
        .style('font-size', fontSize + 'px')
        .call(d3.axisLeft(y).tickSize(0));

    if (y.domain()[1] < 0) {
        svg.append('line')
            .attr('x1', 5)
            .attr('x2', width - 1)
            .attr('y1', y(0))
            .attr('y2', y(0))
            .style('stroke-width', 0.05)
            .attr('stroke-opacity', 1)
            .style('stroke', 'black')
            .style('fill', 'none');
    }
}

export function clearChart() {
    const container = d3.select('#consistencyContainer');
    container.selectAll('svg').remove();
}

function getDisplayName(fullName: string, maxLength: number): string {
    if (fullName.length > maxLength) {
        const firstName = fullName.split(' ')[0],
            lastName = fullName.split(' ')[1];
        const fullLast = firstName[0] + '. ' + lastName;
        const fullFirst = firstName + ' ' + lastName[0] + '.';

        if (fullLast.length < maxLength) {
            return fullLast;
        } else if (fullFirst.length < maxLength) {
            return fullFirst;
        } else {
            return fullLast.slice(0, maxLength);
        }
    }
    return fullName;
}