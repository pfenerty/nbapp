import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameShotChartComponent } from './game-shot-chart.component';

describe('GameShotChartComponent', () => {
  let component: GameShotChartComponent;
  let fixture: ComponentFixture<GameShotChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameShotChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameShotChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
