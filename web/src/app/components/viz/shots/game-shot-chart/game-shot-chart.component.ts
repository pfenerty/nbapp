import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { GameEvent } from 'src/app/models/data/game-event.model';
import { ScatterShotChartComponent } from '../scatter-shot-chart/scatter-shot-chart.component';
import { GameShotSettingsComponent, ShotChartSettings } from '../game-shot-settings/game-shot-settings.component';
import { ShotStatsComponent } from '../shot-stats/shot-stats.component';
import { filterEvents, filterOnCourtEvents } from 'src/app/d3/shots/shots';

@Component({
  selector: 'game-shot-chart',
  templateUrl: './game-shot-chart.component.html',
  styleUrls: ['./game-shot-chart.component.css']
})
export class GameShotChartComponent implements OnInit {

  @Input() team: string;
  @Input() events: Array<GameEvent>;
  @Input() isHome: boolean;

  @ViewChild('settings') settingsComponent: GameShotSettingsComponent;
  @ViewChild('chart') chartComponent: ScatterShotChartComponent;
  @ViewChild('stats') shotStatsComponent: ShotStatsComponent;

  constructor() { }

  ngOnInit() {
    this.events = this.events.filter(e => e.offensive_team === this.team);
  }

  updateSettings(settings: ShotChartSettings) {
    settings.view = 'game';
    const filteredEvents = filterEvents(this.events, settings);
    const onCourtEvents = filterOnCourtEvents(this.events, settings, this.isHome);
    this.chartComponent.update(filteredEvents, settings);
    this.shotStatsComponent.update(filteredEvents, onCourtEvents, settings);
  }

}
