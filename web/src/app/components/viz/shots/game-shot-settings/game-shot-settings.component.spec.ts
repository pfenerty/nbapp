import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameShotSettingsComponent } from './game-shot-settings.component';

describe('GameShotSettingsComponent', () => {
  let component: GameShotSettingsComponent;
  let fixture: ComponentFixture<GameShotSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameShotSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameShotSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
