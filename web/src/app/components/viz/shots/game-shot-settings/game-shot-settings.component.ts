import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GameEvent } from 'src/app/models/data/game-event.model';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

export class ShotChartSettings {
  selection: string;
  isTeam: boolean;
  showShots: boolean;
  showAssists: boolean;
  showFouls: boolean;
  showTov: boolean;
  view: string;

  constructor(selection: string, isTeam: boolean, showShots: boolean, showAssists: boolean,
      showFouls: boolean, showTov: boolean, view: string) {
    this.selection = selection;
    this.isTeam = isTeam;
    this.showShots = showShots;
    this.showAssists = showAssists;
    this.showFouls = showFouls;
    this.showTov = showTov;
    this.view = view;
  }
}

@Component({
  selector: 'game-shot-settings',
  templateUrl: './game-shot-settings.component.html',
  styleUrls: ['./game-shot-settings.component.css']
})
export class GameShotSettingsComponent implements OnInit {

  @Input() team: string;
  @Input() events: Array<GameEvent>;
  @Input() view: string;
  @Output() update: EventEmitter<ShotChartSettings> = new EventEmitter();

  settings: ShotChartSettings;
  playerList: Array<string> = [];

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) { 
    iconRegistry.addSvgIcon('settings', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/settings.svg'));
  }

  ngOnInit() {
    this.settings = new ShotChartSettings(this.team, true, true, false, true, false, this.view);
    this.events = this.events.filter(e => e.offensive_team === this.team);
    this.getPlayerList();
  }

  updateSettings() {
    this.settings.isTeam = this.settings.selection === this.team;
    this.update.emit(this.settings);
  }

  getPlayerList() {
    this.events.filter(e => e.offensive_team === this.team && e.type === 'shot').forEach(shot => {
      if (this.playerList.indexOf(shot.player1) === -1 && shot.player1 !== null) {
        this.playerList.push(shot.player1);
      }
      if (this.playerList.indexOf(shot.player2) === -1 && shot.player2 !== null) {
        this.playerList.push(shot.player2);
      }
    });
  }
}
