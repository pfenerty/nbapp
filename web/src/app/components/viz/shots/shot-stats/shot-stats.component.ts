import { Component, OnInit, Input, ElementRef } from '@angular/core';
import { GameEvent } from 'src/app/models/data/game-event.model';
import { drawShotStats } from 'src/app/d3/shots/shot-stats';
import * as d3 from 'd3';
import { ShotChartSettings } from '../game-shot-settings/game-shot-settings.component';
import { filterOnCourtEvents } from 'src/app/d3/shots/shots';


@Component({
  selector: 'shot-stats',
  templateUrl: './shot-stats.component.html',
  styleUrls: ['./shot-stats.component.css']
})
export class ShotStatsComponent implements OnInit {

  @Input() allEvents: Array<GameEvent>;
  @Input() team: string;
  @Input() isHome: boolean;

  private host;
  private svg;

  constructor(private element: ElementRef) { this.host = d3.select(this.element.nativeElement); }

  ngOnInit() {
    this.allEvents = this.allEvents.filter(s => s.offensive_team === this.team);
    const settings = new ShotChartSettings(this.team, true, true, false, true, false, null);
    this.svg = this.host.append('svg')
                    .attr('viewBox', '0, 0, 50, 10')
                    .classed('shotStatsSVG', true);
    const onCourtEvents = filterOnCourtEvents(this.allEvents, settings, this.isHome);
    drawShotStats(this.svg, this.allEvents, onCourtEvents, settings);
  }

  update(events: Array<GameEvent>, onCourtEvents: Array<GameEvent>, settings: ShotChartSettings) {
    drawShotStats(this.svg, events, onCourtEvents, settings);
  }

}
