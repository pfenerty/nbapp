import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShotStatsComponent } from './shot-stats.component';

describe('ShotStatsComponent', () => {
  let component: ShotStatsComponent;
  let fixture: ComponentFixture<ShotStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShotStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShotStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
