import { Component, OnInit, Input, AfterViewInit, ElementRef } from '@angular/core';
import { drawScatterShots, drawScatterLegend } from '../../../../d3/shots/scatter-shots';
import { drawCourt, filterEvents } from '../../../../d3/shots/shots';
import { GameEvent } from 'src/app/models/data/game-event.model';
import * as d3 from 'd3';
import { ShotChartSettings } from '../game-shot-settings/game-shot-settings.component';

@Component({
  selector: 'scatter-shot-chart',
  templateUrl: './scatter-shot-chart.component.html',
  styleUrls: ['./scatter-shot-chart.component.css']
})
export class ScatterShotChartComponent implements OnInit {

  @Input() events: Array<GameEvent>;
  @Input() team: string;
  @Input() view: string;

  filteredEvents: Array<GameEvent>;

  private host;
  private svg;

  constructor(private element: ElementRef) { this.host = d3.select(this.element.nativeElement); }

  ngOnInit() {
    this.svg = this.host.append('svg')
                  .attr('viewBox', '0, 10, ' + 50 + ', ' + 43 + '')
                  .classed('court', true);

    this.events = this.events.filter(s => s.offensive_team === this.team);
    const settings = new ShotChartSettings(this.team, true, true, false, true, false, this.view);
    this.filteredEvents = filterEvents(this.events, settings);
    drawCourt(this.svg);
    this.update(this.filteredEvents, settings);
  }

  update(filteredEvents: Array<GameEvent>, settings: ShotChartSettings): void {
    drawScatterShots(this.svg, filteredEvents, settings);
    drawScatterLegend(this.svg, settings);
  }
}
