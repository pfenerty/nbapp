import { Component, OnInit, ElementRef } from '@angular/core';
import { Rotation } from 'src/app/models/data/rotation.model';
import * as d3 from 'd3';

@Component({
  selector: 'season-rotations-viz2',
  templateUrl: './season-rotations-viz2.component.html',
  styleUrls: ['./season-rotations-viz2.component.css']
})
export class SeasonRotationsViz2Component implements OnInit {

  private htmlElement: HTMLElement;
  private host;
  private svg;

  private width = 100;

  private players: Array<string>;
  private quarterLabels = ['Q1', 'Q2', 'Q3', 'Q4'];

  private colorScale = d3.scaleLinear().domain([-5, 5]).range(['#0000fc', '#fc0000']);

  constructor(private element: ElementRef) {
    this.htmlElement = this.element.nativeElement;
    this.host = d3.select(this.element.nativeElement);
  }

  ngOnInit() {
    this.svg = this.host.append('svg').attr('viewBox', '0, 0, 0, 0');
  }

  public init(data: Array<Rotation>) {
    this.players = this.sortPlayers(data);
    const longestPlayerName = this.players.reduce((a: string, b: string) => a.length > b.length ? a : b),
      playerLabelsWidth = longestPlayerName.length + 2,
      gridSize = (this.width - playerLabelsWidth) / 48,
      height = gridSize * (this.players.length + 3);

    this.svg.attr('viewBox', '0, 0, ' + this.width + ', ' + height);

    const x = d3.scaleLinear().range([playerLabelsWidth, this.width - 5]);
    x.domain([0, 2880]);

    const y = d3.scaleLinear().range([0, height]);
    y.domain([0, this.players.length + 2.5]);

    const playerLabels = this.svg.selectAll('.playerLabel')
      .data(this.players, d => d);

    playerLabels.enter()
      .append('text')
      .text(d => d)
      .style('font-size', gridSize + 'px')
      .classed('mono', true)
      .classed('playerLabel', true)
      .style('text-anchor', 'end')
      .style('fill-opacity', 0)
      .transition().duration(1000)
      .style('fill-opacity', 1)
      .attr('x', playerLabelsWidth)
      .attr('y', (d, index) => y(index + 2));

    playerLabels.exit()
      .transition()
      .duration(1000)
      .style('fill-opacity', 0)
      .attr('y', y(this.players.length + 5))
      .remove();

    playerLabels
      .transition().duration(1000)
      .attr('x', playerLabelsWidth)
      .attr('y', (d, index) => y(index + 2))
      .style('fill-opacity', 1);

    this.svg.selectAll('.quarterLabelTop')
      .data(this.quarterLabels)
      .enter().append('text')
      .text(d => d)
      .attr('font-size', gridSize + 'px')
      .classed('mono', true)
      .classed('quarterLabelTop', true)
      .style('text-anchor', 'start')
      .style('fill-opacity', 0);

    this.svg.selectAll('.quarterLabelTop')
      .transition().duration(1000)
      .attr('x', (d, i) => x(i * 720))
      .attr('y', y(1))
      .style('fill-opacity', 1);

    this.svg.selectAll('.quarterLabelBottom')
      .data(this.quarterLabels)
      .enter().append('text')
      .text(d => d)
      .attr('font-size', gridSize + 'px')
      .classed('mono', true)
      .classed('quarterLabelBottom', true)
      .style('text-anchor', 'start')
      .style('fill-opacity', 0);

    this.svg.selectAll('.quarterLabelBottom')
      .transition().duration(1000)
      .attr('x', (d, i) => x(i * 720))
      .attr('y', y(this.players.length + 2))
      .style('fill-opacity', 1);

    const rotationBlocks = this.svg.selectAll('.rotationBlock')
      .data(data, d => [d.start, d.end, d.player]);

    const rotationBlockOpacity = 1.5 / Array.from(new Set(data.map(x => x.game_id))).length;

    rotationBlocks
      .enter().append('rect')
      .attr('x', d => x(d.start))
      .attr('y', d => y(this.players.indexOf(d.player) + 1.25))
      .attr('width', d => x(d.end) - x(d.start))
      .attr('rx', 0.25)
      .attr('ry', 0.25)
      .attr('height', gridSize * .8)
      .style('fill', 'black')
      .classed('rotationBlock', true)
      .style('opacity', rotationBlockOpacity);

    rotationBlocks
      .exit().remove();

    rotationBlocks
      .attr('x', d => x(d.start))
      .attr('y', d => y(this.players.indexOf(d.player) + 1.25))
      .attr('width', d => x(d.end) - x(d.start))
      .attr('height', gridSize * .8)
      .style('opacity', rotationBlockOpacity);
  }

  private sortPlayers(data: Rotation[]): Array<string> {
    const players = Array.from(new Set(data.map(r => r.player)));
    players.sort((a: string, b: string) => this.getTotalMinutesForPlayer(data, b) - this.getTotalMinutesForPlayer(data, a));
    return players;
  }

  private getTotalMinutesForPlayer(rotationData: Rotation[], player: string): number {
    const playerRotations = rotationData.filter((r: Rotation) => r.player === player);
    return playerRotations.map(pr => (pr.end - pr.start)).reduce((a, b) => a + b);
  }
}
