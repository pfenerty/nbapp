import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeasonRotationsViz2Component } from './season-rotations-viz2.component';

describe('SeasonRotationsViz2Component', () => {
  let component: SeasonRotationsViz2Component;
  let fixture: ComponentFixture<SeasonRotationsViz2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeasonRotationsViz2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeasonRotationsViz2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
