import { Component, OnInit, Input } from '@angular/core';
import { Rotation } from '../../../../models/data/rotation.model';
import { StarterInfo } from '../../../../models/util/starter-info.model';
import { plotRotations, getStartingLineups, sortPlayers } from '../../../../d3/rotations/season-rotations';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'season-rotations-viz',
  templateUrl: './season-rotations-viz.component.html',
  styleUrls: ['./season-rotations-viz.component.css']
})
export class SeasonRotationsVizComponent implements OnInit {

  data: Rotation[];
  allPlayers: String[];
  team: string;

  startingLineups: StarterInfo[];
  selectedStartingLinup: StarterInfo;

  filteredData: Rotation[];
  filteredPlayers: String[];

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon('arrow.up', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/arrow.up.svg'));
    iconRegistry.addSvgIcon('arrow.down', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/arrow.down.svg'));
  }

  ngOnInit() {}

  public init(data, team) {
    this.data = data;
    this.startingLineups = getStartingLineups(data);
    this.allPlayers = sortPlayers(data);
    this.filteredPlayers = this.allPlayers;
    this.team = team;
    plotRotations(data, this.filteredPlayers, team);
  }

  starterDisplay(starters: StarterInfo): string {
    let displayString = '';
    starters.players.forEach(p => displayString += p.split(' ')[1] + ', ');
    displayString = displayString.slice(0, -2);
    return displayString + ': ' + starters.games.length + ' games';
  }

  filterByStarters(): void {
    if (this.selectedStartingLinup) {
      this.filteredData = this.data.filter(d => this.selectedStartingLinup.games.indexOf(d.game_id) !== -1);
    } else {
      this.filteredData = this.data;
    }
    this.filteredPlayers = sortPlayers(this.filteredData);
    plotRotations(this.filteredData, this.filteredPlayers, this.team);
  }

  movePlayer(player: string, up: boolean) {
    const index = this.filteredPlayers.indexOf(player);
    if (up && index !== 0) {
      this.filteredPlayers.splice(index, 1)[0];
      this.filteredPlayers.splice(index - 1, 0, player);
    } else if (!up && index !== this.filteredPlayers.length) {
      this.filteredPlayers.splice(index, 1)[0];
      this.filteredPlayers.splice(index + 1, 0, player);
    }
  }

  reorderPlayers() {
    plotRotations(this.filteredData, this.filteredPlayers, this.team);
  }

}
