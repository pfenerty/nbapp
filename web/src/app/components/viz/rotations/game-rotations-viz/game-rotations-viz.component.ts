import { Component, OnInit, Input, ElementRef } from '@angular/core';
import { plotRotations, sortPlayers, updatePlayerOrder } from '../../../../d3/rotations/game-rotations';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { Score } from 'src/app/models/data/score.model';
import { Rotation } from 'src/app/models/data/rotation.model';
import * as d3 from 'd3';
import { TeamPlayers } from './player-order/player-order.component';

@Component({
  selector: 'game-rotations-viz',
  templateUrl: './game-rotations-viz.component.html',
  styleUrls: ['./game-rotations-viz.component.css']
})
export class GameRotationsVizComponent implements OnInit {

  @Input() rotations: Array<Rotation>;
  @Input() scores: Array<Score>;
  @Input() home: string;
  @Input() away: string;

  private htmlElement: HTMLElement;
  private host;
  private svg;

  constructor(
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer,
    private element: ElementRef
    ) {
    this.htmlElement = this.element.nativeElement;
    this.host = d3.select(this.element.nativeElement);
    iconRegistry.addSvgIcon('arrow.up', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/arrow.up.svg'));
    iconRegistry.addSvgIcon('arrow.down', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/arrow.down.svg'));
  }

  ngOnInit() {
    this.svg = this.host.select('#rotations-container').append('svg').attr('viewBox', '0, 0, 100, 40');

    const homePlayers = sortPlayers(this.home, this.rotations);
    const awayPlayers = sortPlayers(this.away, this.rotations);

    plotRotations(this.svg, this.rotations, this.scores, this.home, this.away, homePlayers, awayPlayers);
  }

  updatePlayerOrder(teamPlayers: TeamPlayers) {
    updatePlayerOrder(this.svg, this.rotations, this.scores, this.home, this.away, teamPlayers.home, teamPlayers.away);
  }

}
