import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Rotation } from 'src/app/models/data/rotation.model';
import { sortPlayers } from 'src/app/d3/rotations/game-rotations';

export class TeamPlayers {
  home: Array<string>;
  away: Array<string>;

  constructor(home: Array<string>, away: Array<string>) {
    this.home = home;
    this.away = away;
  }
}

@Component({
  selector: 'rotations-player-order',
  templateUrl: './player-order.component.html',
  styleUrls: ['./player-order.component.css']
})
export class PlayerOrderComponent implements OnInit {

  @Input() home: string;
  @Input() away: string;

  @Input() rotations: Array<Rotation>;

  @Output() update: EventEmitter<TeamPlayers> = new EventEmitter();

  homePlayers: Array<string>;
  awayPlayers: Array<string>;

  constructor() { }

  ngOnInit() {
    this.homePlayers = sortPlayers(this.home, this.rotations);
    this.awayPlayers = sortPlayers(this.away, this.rotations);
  }

  movePlayer(player: string, isHome: boolean, isUp: boolean) {
    const players = isHome ? this.homePlayers : this.awayPlayers,
      fromIndex = players.indexOf(player),
      toIndex = fromIndex + (isUp ? -1 : 1);

    if (fromIndex >= 0 && !(fromIndex == 0 && isUp) && !(fromIndex == players.length - 1 && !isUp)) {
      players.splice(toIndex, 0, players.splice(fromIndex, 1)[0]);
    }
  }

  updatePlayerOrder() {
    const players = new TeamPlayers(this.homePlayers, this.awayPlayers);
    this.update.emit(players);
  }

}
