export class QuarterFilter {
    q1: boolean;
    q2: boolean;
    q3: boolean;
    q4: boolean;
    ot: boolean;

    constructor(secondsPlayed: number) {
        this.q1 = true;
        this.q2 = secondsPlayed > 720;
        this.q3 = secondsPlayed > 1440;
        this.q4 = secondsPlayed > 2160;
        this.ot = secondsPlayed > 2880;
    }
}