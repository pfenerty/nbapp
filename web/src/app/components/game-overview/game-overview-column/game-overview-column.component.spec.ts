import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameOverviewColumnComponent } from './game-overview-column.component';

describe('GameOverviewColumnComponent', () => {
  let component: GameOverviewColumnComponent;
  let fixture: ComponentFixture<GameOverviewColumnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameOverviewColumnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameOverviewColumnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
