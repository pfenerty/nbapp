import { Component, OnInit, Input } from '@angular/core';
import { TeamOverviewData } from '../team-overview-data.model';

@Component({
  selector: 'game-overview-column',
  templateUrl: './game-overview-column.component.html',
  styleUrls: ['../game-overview.component.css']
})
export class GameOverviewColumnComponent implements OnInit {

  @Input() team: string;
  @Input() data: TeamOverviewData;

  constructor() { }

  ngOnInit() {
  }

}
