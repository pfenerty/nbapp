import { TeamOverviewStat } from "./team-overview-stat.model";
import { GameEvent } from "src/app/models/data/game-event.model";

export class TeamOverviewData {
    secondsPlayed: TeamOverviewStat;
    possessions: TeamOverviewStat;
    timeToFirstShot: TeamOverviewStat;
    ortg: TeamOverviewStat;

    oreb: TeamOverviewStat;
    secondChancePoints: TeamOverviewStat;

    tov: TeamOverviewStat;
    liveTov: TeamOverviewStat;
    deadTov: TeamOverviewStat;
    pointsOffTov: TeamOverviewStat;

    tsa: TeamOverviewStat;
    tsPct: TeamOverviewStat;
    shots: TeamOverviewStat;
    efgPct: TeamOverviewStat;
    ft: TeamOverviewStat;
    ftPct: TeamOverviewStat;

    liveBallPoss: TeamOverviewStat;
    liveBallPossTime: TeamOverviewStat;
    deadBallPoss: TeamOverviewStat;
    deadBallPossTime: TeamOverviewStat;

    constructor(events: Array<GameEvent>, secondsPlayed: number) {
        const pctOfGamePlayed: number = secondsPlayed / (12 * 60 * 4);

        this.secondsPlayed = new TeamOverviewStat(
            secondsPlayed,
            pctOfGamePlayed,
            0, 1
        );

        const technicalFts = events.filter((e: GameEvent) => e.zone === 'Technical');
        const usageEvents = events.filter((e: GameEvent) =>
            (e.type === 'shot' || e.type === 'ft' || e.type === 'tov') && e.zone !== 'Technical'
        );

        const numPossessions: number = Array.from(new Set(usageEvents
            .filter((event: GameEvent) => !event.is_after_oreb)
            .map((event: GameEvent) => event.possession)
        )).length;
        this.possessions = new TeamOverviewStat(
            numPossessions,
            numPossessions / (secondsPlayed / 60) * 48,
            95, 115
        );

        const timeToFirstShot: number = usageEvents.length > 0 ?
            usageEvents.filter((e: GameEvent) => !e.is_after_oreb)
                .map((e: GameEvent) => e.time_since_possession_change)
                .reduce((a: number, b: number) => a + b) / usageEvents.length
            : 0;
        this.timeToFirstShot = new TeamOverviewStat(
            timeToFirstShot,
            timeToFirstShot,
            15, 7
        );

        const points: number = usageEvents.length > 0 ?
            usageEvents.map((event: GameEvent) => Math.abs(event.score_change)).reduce((a: number, b: number) => a + b)
            : 0;
        this.ortg = new TeamOverviewStat(
            points / numPossessions * 100,
            points / numPossessions,
            .7, 1.4
        );

        const secondChanceUsageEvents = usageEvents.filter((e: GameEvent) => e.is_after_oreb),
            missedShots = events.filter((event: GameEvent) => event.type === 'shot' && event.shot_value === 0);
        this.oreb = new TeamOverviewStat(
            secondChanceUsageEvents.length,
            secondChanceUsageEvents.length / missedShots.length,
            .05, .5
        );

        const numSecondPoints: number = secondChanceUsageEvents.length > 0 ?
            secondChanceUsageEvents.filter((event: GameEvent) => (event.type === 'shot' || event.type === 'ft'))
                .map((event: GameEvent) => Math.abs(event.score_change))
                .reduce((a: number, b: number) => a + b)
            : 0;
        this.secondChancePoints = new TeamOverviewStat(
            numSecondPoints,
            numSecondPoints / numPossessions,
            0.05, 0.2
        );

        const tov = events.filter((event: GameEvent) => event.type === 'tov');
        this.tov = new TeamOverviewStat(
            tov.length,
            tov.length / numPossessions,
            0.3, 0.1
        );

        const liveTov = tov.filter((event: GameEvent) => event.player2);
        this.liveTov = new TeamOverviewStat(
            liveTov.length,
            liveTov.length / numPossessions,
            0.15, 0.05
        );

        const deadTov = tov.filter((event: GameEvent) => !event.player2);
        this.deadTov = new TeamOverviewStat(
            deadTov.length,
            deadTov.length / numPossessions,
            0.15, 0.05
        );

        const transitionOffLiveTov = usageEvents.filter(
            (e: GameEvent) => e.previous_possession_end === 'Live Ball Tov' &&
                e.time_since_possession_change <= 7),
            pointOffTov = transitionOffLiveTov.length > 0 ?
                transitionOffLiveTov.map((e: GameEvent) => Math.abs(e.score_change))
                    .reduce((a: number, b: number) => a + b)
                : 0;
        this.pointsOffTov = new TeamOverviewStat(
            pointOffTov,
            pointOffTov / numPossessions,
            0.05, 0.2
        );


        const tsaEvents = usageEvents.filter((e: GameEvent) => e.type !== 'tov');
        this.tsa = new TeamOverviewStat(
            tsaEvents.length,
            tsaEvents.length / numPossessions,
            .7, .95
        );

        this.tsPct = new TeamOverviewStat(
            points / tsaEvents.length / 2 * 100,
            points / tsaEvents.length / 2,
            .4, .75
        );

        const shots = usageEvents.filter((event: GameEvent) => event.type === 'shot');
        this.shots = new TeamOverviewStat(
            shots.length,
            shots.length / numPossessions,
            .6, .85
        );

        const pointsOnShots = shots.length > 0 ?
            shots.map((event: GameEvent) => Math.abs(event.shot_value)).reduce((a: number, b: number) => a + b)
            : 0;
        this.efgPct = new TeamOverviewStat(
            pointsOnShots / shots.length / 2 * 100,
            pointsOnShots / shots.length / 2,
            .35, .7
        );

        const ft = usageEvents.filter((event: GameEvent) => event.type === 'ft'),
            fta = ft.length > 0 ?
                ft.map((event: GameEvent) => event.fts_taken).reduce((a: number, b: number) => a + b)
                : 0,
            ftm = ft.length > 0 ?
                ft.map((event: GameEvent) => event.fts_made).reduce((a: number, b: number) => a + b)
                : 0;
        this.ft = new TeamOverviewStat(
            fta,
            fta / numPossessions,
            .15, .35
        );

        this.ftPct = new TeamOverviewStat(
            ftm / fta * 100,
            ftm / fta,
            .4, 1
        );

        const liveBallPoss = usageEvents.filter((e: GameEvent) => e.previous_possession_end.includes('Live') && !e.is_after_oreb),
            deadBallPoss = usageEvents.filter((e: GameEvent) => !e.previous_possession_end.includes('Live') && !e.is_after_oreb),
            liveBallPossTime = liveBallPoss.length > 0 ?
                liveBallPoss.map((e: GameEvent) => e.time_since_possession_change)
                    .reduce((a: number, b: number) => a + b) / liveBallPoss.length
                : 0,
            deadBallPossTime = deadBallPoss.length > 0 ?
                deadBallPoss.map((e: GameEvent) => e.time_since_possession_change)
                    .reduce((a: number, b: number) => a + b) / deadBallPoss.length
                : 0;

        this.liveBallPoss = new TeamOverviewStat(
            liveBallPoss.length,
            liveBallPoss.length / numPossessions,
            0.2, 0.6
        );

        this.liveBallPossTime = new TeamOverviewStat(
            liveBallPossTime,
            liveBallPossTime,
            17, 8
        );

        this.deadBallPoss = new TeamOverviewStat(
            deadBallPoss.length,
            deadBallPoss.length / numPossessions,
            0.2, 0.6
        );

        this.deadBallPossTime = new TeamOverviewStat(
            deadBallPossTime,
            deadBallPossTime,
            20, 12
        );
    }
}