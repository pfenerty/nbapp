import { Component, OnInit, Input } from '@angular/core';
import { TeamOverviewStat } from '../team-overview-stat.model';

@Component({
  selector: 'game-overview-stat',
  templateUrl: './game-overview-stat.component.html',
  styleUrls: ['../game-overview.component.css']
})
export class GameOverviewStatComponent implements OnInit {

  @Input() stat: TeamOverviewStat;

  constructor() { }

  ngOnInit() {
  }

}
