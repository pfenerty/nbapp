import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameOverviewStatComponent } from './game-overview-stat.component';

describe('GameOverviewStatComponent', () => {
  let component: GameOverviewStatComponent;
  let fixture: ComponentFixture<GameOverviewStatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameOverviewStatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameOverviewStatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
