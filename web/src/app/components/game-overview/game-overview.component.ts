import { Component, OnInit, Input } from '@angular/core';
import { GameEvent } from 'src/app/models/data/game-event.model';
import { QuarterFilter } from './quarter-filter.model';
import { TeamOverviewData } from './team-overview-data.model';

@Component({
  selector: 'game-overview',
  templateUrl: './game-overview.component.html',
  styleUrls: ['./game-overview.component.css']
})
export class GameOverviewComponent implements OnInit {

  @Input() events: Array<GameEvent>;
  @Input() secondsPlayed: number;
  @Input() homeTeam: string;
  @Input() awayTeam: string;

  quarterFilter: QuarterFilter;

  homeData: TeamOverviewData;
  awayData: TeamOverviewData;

  glossaryColumns: String[] = ['name', 'desc'];
  glossaryData: any = [
    {'name': 'Poss', 'desc': 'Number of Offensive Possessions'},
    {'name': 'O-Rtg', 'desc': 'Offensive Rating - Points Scored Per 100 Possessions'},
    {'name': 'Time to Shot', 'desc': 'Average number of seconds between possession change and the first shot / foul drawn / tov of the possession'},
    {'name': 'TSA', 'desc': 'True Shooting Attempts - Field Goal Attempts + Trips to the Free Throw Line'},
    {'name': 'TS%', 'desc': 'True Shooting Percentage - Points / (True Shooting Attemps * 2)'},
    {'name': 'Shots', 'desc': 'Field Goal Attempts'},
    {'name': 'EFG%', 'desc': 'Points on Field Goal Attempts / (Field Goal Attempts * 2)'},
    {'name': 'FTA', 'desc': 'Free Throw Attempts'},
    {'name': 'FT%', 'desc': 'Free Throw Percentage'},
    {'name': 'O-Rebs', 'desc': 'Offensive Rebounds'},
    {'name': '2nd Pts', 'desc': '2nd Chance Points - Points scored after on Offensive Rebound'},
    {'name': 'Tovs', 'desc': 'Turnovers'},
    {'name': 'Dead', 'desc': 'Turnovers which result in a dead ball (Out of bounds, violations, offensive fouls)'},
    {'name': 'Live', 'desc': 'Turonvers which result in a live ball change of possession (Steals)'},
    {'name': 'Pts Off', 'desc': 'Points off of Turnovers - filtered to live turnovers resulting in transition baskets the other way'},
    {'name': 'Live Poss Change', 'desc': 'Offensive Possessions when the opponent\'s previous possession ended in a live ball tov or defensive rebound'},
    {'name': 'Dead Poss Change', 'desc': 'Offensive Possessions when the opponent\'s previous possession ended in a dead ball tov or made shot'},
  ];

  constructor() { }

  ngOnInit() {
    this.quarterFilter = new QuarterFilter(this.secondsPlayed);
    this.updateData(this.events);
  }

  quarterFilterChange() {
    const filteredEvents = this.events.filter(
      (e: GameEvent) => (this.quarterFilter.q1 && e.time <= 720) ||
      (this.quarterFilter.q2 && e.time > 720 && e.time <= 1440) ||
      (this.quarterFilter.q3 && e.time > 1440 && e.time <= 2160) ||
      (this.quarterFilter.q4 && e.time > 2160 && e.time <= 2880) ||
      (this.quarterFilter.ot && e.time > 2880)
    );
    this.updateData(filteredEvents);
  }

  updateData(newEvents: Array<GameEvent>) {
    this.homeData = new TeamOverviewData(
      newEvents.filter((event: GameEvent) => event.offensive_team === this.homeTeam), this.secondsPlayed
    );
    this.awayData = new TeamOverviewData(
      newEvents.filter((event: GameEvent) => event.offensive_team === this.awayTeam), this.secondsPlayed
    );
  }

}
