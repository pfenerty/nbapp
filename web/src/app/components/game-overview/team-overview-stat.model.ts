import * as d3 from 'd3';

const minColor = '#0000fc',
    maxColor = '#fc0000';

export class TeamOverviewStat {
    magnitude: number;
    velocity: number;
    color: string;

    constructor(value, velocity, min, max) {
        this.magnitude = value;
        this.velocity = velocity;
        this.color = d3.scaleLinear().domain([min, max]).range([minColor, maxColor])(velocity);
    }
}