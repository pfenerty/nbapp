import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { MenuService } from 'src/app/services/menu.service';

export class TeamSeason {
  year: number;
  type: string;
  team: string;
  opponent: string;
  player: string;

  constructor(year: number = null, type: string = null, team: string = null, opponent: string = null, player: string = null) {
    this.year = year;
    this.type = type;
    this.team = team;
    this.opponent = opponent;
    this.player = player;
  }
}

@Component({
  selector: 'season-team-controls',
  templateUrl: './season-team-controls.component.html',
  styleUrls: ['./season-team-controls.component.css']
})
export class SeasonTeamControlsComponent implements OnInit {

  @Input() showPlayerList = false;
  @Input() showOpponent = false;
  @Input() isPlayType = false;
  @Output() update: EventEmitter<TeamSeason> = new EventEmitter();

  private _menuData: any;

  teamList: Array<string>;
  seasonList: Array<number>;
  seasonTypeList: Array<string> = ['Regular Season', 'Playoffs'];
  playerList: Array<string>;

  data = new TeamSeason();

  constructor(
    private _menuService: MenuService
  ) { }

  ngOnInit() {
    this._menuService.getSeasons().subscribe(
      (seasons: Array<number>) => {
        this.seasonList = seasons;
        this.data = new TeamSeason(
          seasons[seasons.length - 1],
          'Regular Season',
          null,
          null,
          null
        );
        this.updateTeams();
      }
    );
  }

  updateTeams(): void {
    this._menuService.getTeams(this.data.year, this.data.type).subscribe(
      (teams: Array<string>) => {
        this.teamList = teams;
        this.data.team = this.teamList.indexOf(this.data.team) >= 0 ? this.data.team : null;
        this.data.opponent = this.teamList.indexOf(this.data.opponent) >= 0 ? this.data.opponent : null;
        this.updatePlayers();
        this.update.emit(this.data);
      }
    );
  }

  updatePlayers(): void {
    if (this.showPlayerList) {
      this._menuService.getPlayers(this.data.year, this.data.type, this.data.team).subscribe(
        (players: Array<string>) => {
          this.playerList = players;
          this.data.player = this.playerList.indexOf(this.data.player) >= 0 ? this.data.player : null;
        }
      );
    }
    this.update.emit(this.data);
  }

  emit(): void {
    this.update.emit(this.data);
  }



}
