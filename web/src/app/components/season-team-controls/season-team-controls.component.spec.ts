import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeasonTeamControlsComponent } from './season-team-controls.component';

describe('SeasonTeamControlsComponent', () => {
  let component: SeasonTeamControlsComponent;
  let fixture: ComponentFixture<SeasonTeamControlsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeasonTeamControlsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeasonTeamControlsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
