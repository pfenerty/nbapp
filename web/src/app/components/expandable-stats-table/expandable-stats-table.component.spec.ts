import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpandableStatsTableComponent } from './expandable-stats-table.component';

describe('ExpandableStatsTableComponent', () => {
  let component: ExpandableStatsTableComponent;
  let fixture: ComponentFixture<ExpandableStatsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpandableStatsTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpandableStatsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
