import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort } from '@angular/material';
import { GameStats, PlayerGameStats } from 'src/app/models/data/game-details.model';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'expandable-stats-table',
  templateUrl: './expandable-stats-table.component.html',
  styleUrls: ['./expandable-stats-table.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class ExpandableStatsTableComponent implements OnInit {

  constructor() { }

  @Input() stats: Array<any>;
  @Input() team: string;
  @ViewChild(MatSort) sort: MatSort;
  dataSource = new MatTableDataSource([]);
  initialSortCol = 'scoring.points';
  initialSortDir = 'desc';
  columnsToDisplay = ['player', 'min', 'points', 'ast', 'reb'];

  expandedElement: GameStats | null;
  objectKeys = Object.keys;

  ngOnInit() {
    this.stats.sort((a, b) => b.points - a.points);
    this.stats = this.stats.filter(s => s.player !== this.team);
    this.dataSource = new MatTableDataSource(this.stats);
    this.dataSource.sort = this.sort;
  }

  getProperty = (obj, path) => (path.split('.').reduce((o, p) => o && o[p], obj));

  getColumnName = (path) => {
    if (path === 'rebounding.total') {
      return 'Rebounds';
    } else {
      const pathSplit = path.split('.'),
            columnName = pathSplit[pathSplit.length - 1];
      return columnName.charAt(0).toUpperCase() + columnName.slice(1);
    }
  }

  formatMakeAttempts(makes: number, attempts: number): string {
    if (attempts === 0) {
      return '0/0 (0%)';
    } else {
      return makes + '/' + attempts + ' (' + Math.round(makes / attempts * 1000) / 10 + '%)';
    }
  }

  formatPlayerAssisted(playersAssisted: Array<any>) {
    return String(playersAssisted['player']) + ': ' + String(playersAssisted['assists']);
  }

  isLeftAlignedColumn(column: string) {
    return column === 'player' || column === 'min';
  }

}
