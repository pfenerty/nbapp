import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon('person', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/person.svg'));
    iconRegistry.addSvgIcon('email', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/email.svg'));
    iconRegistry.addSvgIcon('twitter', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/twitter.svg'));
    iconRegistry.addSvgIcon('github', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/code.svg'));
    iconRegistry.addSvgIcon('arrow.up', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/arrow.up.svg'));
    iconRegistry.addSvgIcon('reader', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/reader.svg'));
    iconRegistry.addSvgIcon('tune', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/tune.svg'));
    iconRegistry.addSvgIcon('grain', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/grain.svg'));
    iconRegistry.addSvgIcon('blur', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/blur.svg'));
    iconRegistry.addSvgIcon('home', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/home.svg'));
    iconRegistry.addSvgIcon('clock', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/clock.svg'));
    iconRegistry.addSvgIcon('play-type', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/play-type.svg'));
  }

  ngOnInit() {
  }
}
