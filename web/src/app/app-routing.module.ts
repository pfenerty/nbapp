import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { GamesComponent } from './views/game/games/games.component';
import { GameDetailComponent } from './views/game/game-detail/game-detail.component';
import { ConsistencyComponent } from 'src/app/views/consistency/consistency.component';
import { RotationsViewComponent } from './views/rotations-view/rotations-view.component';
import { ShotsComponent } from './views/shots/shots.component';
import { ShotTimeComponent } from './views/shot-time/shot-time.component';
import { PlayTypeComponent } from './views/play-type/play-type.component';

const routes: Routes = [
  { path: '', redirectTo: '/games', pathMatch: 'full' },
  { path: 'games', component: GamesComponent },
  { path: 'gameDetail/:date/:id/:hTeam/:vTeam', component: GameDetailComponent },
  { path: 'game_by_game', component: ConsistencyComponent },
  { path: 'rotations', component: RotationsViewComponent },
  { path: 'shots', component: ShotsComponent },
  { path: 'shot-time', component: ShotTimeComponent },
  { path: 'play-type', component: PlayTypeComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
