import { Component, OnInit, AfterViewInit } from '@angular/core';
import { TeamSeason } from 'src/app/components/season-team-controls/season-team-controls.component';
import { API } from 'src/app/services/api.service';
import * as d3 from 'd3';
import { TEAM_COLORS } from 'src/app/enums';
import { Dimensions, Margins } from 'src/app/models/util/dimensions';
import { averages } from './league-averages';

@Component({
  selector: 'app-shot-time',
  templateUrl: './shot-time.component.html',
  styleUrls: ['./shot-time.component.css']
})
export class ShotTimeComponent implements OnInit, AfterViewInit {

  private leagueAverages = { "efficiency": { "0": 1.343652282676673, "1": 1.2513661202185793, "2": 1.335550628233555, "3": 1.3005109150023224, "4": 1.2600088476000886, "5": 1.1971554683668464, "6": 1.170693686671863, "7": 1.155218821652876, "8": 1.1344453376205788, "9": 1.1523738734553564, "10": 1.1250550806380542, "11": 1.122878590078329, "12": 1.0945967358580257, "13": 1.1191407748369773, "14": 1.0956937799043063, "15": 1.0702720473411687, "16": 1.0866246560752642, "17": 1.0745268138801263, "18": 1.076, "19": 1.022847522847523, "20": 1.0371452420701168, "21": 1.0008190008190008, "22": 0.9941792782305006, "23": 0.932643733461631, "24": 0.9355964467005076, "25": 0.8936170212765957, "26": 0.8809073724007561, "27": 0.8723004694835681, "28": 0.9119170984455959, "29": 0.9047619047619048, "30": 0.9211356466876972, "31": 0.7985611510791367, "32": 0.967391304347826, "33": 0.9411764705882353, "34": 1.0909090909090908, "35": 1.0344827586206897, "36": 1.1333333333333333, "37": 1.2, "38": 0.5454545454545454, "39": 0.7142857142857143, "40": 1.75, "41": 0.0, "42": 1.25, "43": 0.0, "44": 0.75, "46": 0.0, "48": 2.0 }, "volume": { "0": 3198, "1": 2013, "2": 4059, "3": 6459, "4": 9042, "5": 10195, "6": 10264, "7": 10031, "8": 9952, "9": 10763, "10": 11347, "11": 12256, "12": 12622, "13": 13035, "14": 12749, "15": 12167, "16": 11267, "17": 10144, "18": 9250, "19": 8316, "20": 7188, "21": 6105, "22": 5154, "23": 4157, "24": 3152, "25": 2303, "26": 1587, "27": 1065, "28": 772, "29": 441, "30": 317, "31": 139, "32": 92, "33": 68, "34": 55, "35": 29, "36": 30, "37": 15, "38": 11, "39": 7, "40": 4, "41": 3, "42": 4, "43": 1, "44": 4, "46": 1, "48": 1 } }

  public teamSeason: TeamSeason = new TeamSeason();

  private chartContainer: any;
  private svg: any;

  private dimensions: Dimensions = new Dimensions(50, 100, new Margins(0, 15, 5, 5));
  private legendHeight = 10;

  public initialized = false;

  private x: any;
  private volY: any;
  private effY: any;

  public liveBall = true;
  public deadBall = true;

  constructor(
    private _api: API
  ) { }

  ngOnInit() { }

  ngAfterViewInit() {
    this.chartContainer = d3.select('#shot-timing-chart');
    this.svg = this.chartContainer
      .append('svg')
      .attr('viewBox', '0, 0, ' + this.dimensions.width + ', ' + this.dimensions.height);
    this.setScales();
    this.createAxes();
    this.createLegend();
    this.updateAverages();
  }

  updateTeamSeason(teamSeason: TeamSeason) {
    this.teamSeason = teamSeason;
  }

  updateChart(): void {
    if (this.teamSeason.team !== null) {
      this._api.getShotTiming(
        this.teamSeason.year, this.teamSeason.type, this.teamSeason.team,
        this.teamSeason.player, this.liveBall, this.deadBall)
        .subscribe((data: any) => {
          const colors = TEAM_COLORS[this.teamSeason.team];
          this.updateAverages();
          this.plot(data, colors[0], colors[1], 0.7, 'main');
        });
    } else {
      this.updateAverages();
    }
  }

  updateAverages(): void {
    const data = this.getLeagueAverageData();
    this.plot(data, 'black', 'black', 0.3, 'average');
    this.initialized = true;
  }

  setScales(): void {
    this.x = d3.scaleLinear().range(this.dimensions.xAxisRange);
    this.x.domain([0, 25]);

    this.volY = d3.scaleLinear().range(this.dimensions.yAxisRange.reverse());
    this.volY.domain([0, 14]);

    this.effY = d3.scaleLinear().range(this.dimensions.yAxisRange.reverse());
    this.effY.domain([0.8, 1.6]);
  }

  createAxes(): void {
    const times = [];
    for (let i = 0; i <= 24; i++) {
      times.push(i);
    }
    const bottomAxis = this.svg.selectAll(`.bottomAxisLabel`).data(times);

    bottomAxis
      .enter()
      .append('text')
      .text(d => d)
      .classed('mono', true)
      .classed('bottomAxisLabel', true)
      .attr('x', d => this.x(d + 0.5))
      .attr('y', d => this.dimensions.height - this.legendHeight - 2)
      .style('font-size', '1px')
      .style('text-anchor', 'middle')

    this.svg
      .append('text')
      .style('font-size', '1px')
      .attr('x', 50)
      .attr('y', this.dimensions.height - this.legendHeight)
      .style('text-anchor', 'middle')
      .text('Seconds After Possession Change');

    this.svg.append('g')
      .style('font-size', '1px')
      .call(d3.axisLeft(this.effY).tickSize(0).ticks(5).tickFormat(d => String(d / 2 * 100) + '%'))
      .attr('transform', 'translate(7,0)');

    this.svg
      .append('text')
      .style('font-size', '1px')
      .attr('x', 1)
      .attr('y', this.dimensions.chartHeight / 2)
      .attr('text-anchor', 'middle')
      .attr('transform', 'rotate(-90, 1, ' + this.dimensions.chartHeight / 2 + ')')
      .text('TS%');

    this.svg.append('g')
      .style('font-size', '1px')
      .call(d3.axisRight(this.volY).tickSize(0).ticks(5).tickFormat(d => String(d) + '%'))
      .attr('transform', 'translate(93,0)');

    this.svg
      .append('text')
      .style('font-size', '1px')
      .attr('x', 100)
      .attr('y', this.dimensions.chartHeight / 2)
      .attr('transform', 'rotate(-90, 99, ' + this.dimensions.chartHeight / 2 + ')')
      .attr('text-anchor', 'middle')
      .text('Percentage of Shots');
  }

  createLegend() {
    this.svg
      .append('rect')
      .classed(`volumeBar-legend`, true)
      .attr('x', this.x(0))
      .attr('width', this.dimensions.chartWidth / 26)
      .attr('y', this.dimensions.height)
      .attr('height', 0)
      .style('opacity', 0.4)
      .transition().duration(2000)
      .attr('y', d => this.dimensions.height - (this.legendHeight * .75))
      .attr('height', d => this.legendHeight * .75)
      .style('fill', 'black');

    this.svg
      .append('text')
      .style('font-size', '1px')
      .attr('x', this.x(0.025))
      .attr('y', this.dimensions.height - (this.legendHeight * .5 * .75))
      .style('text-anchor', 'start')
      .text('Volume');

    this.svg
      .append('circle')
      .classed(`effDot-legend`, true)
      .attr('cx', (d, i) => this.x(1.5))
      .attr('cy', d => this.dimensions.height + 5)
      .attr('r', 0.5)
      .style('opacity', 0.4)
      .transition().duration(2000)
      .attr('cy', d => this.dimensions.height - (this.legendHeight * .5 * .75) - 0.25)
      .style('fill', 'black');

    this.svg
      .append('text')
      .style('font-size', '1px')
      .attr('x', this.x(1.8))
      .attr('y', this.dimensions.height - (this.legendHeight * .5 * .75))
      .style('text-anchor', 'start')
      .text('Efficiency');
  }

  plot(data: any, color1: string, color2: string, opacity: number, type: string): void {
    let plot_data = [];
    let totalShots = 0;
    let maxShots = 0;

    for (let i = 0; i <= 24; i++) {
      plot_data[i] = {};
      plot_data[i]['time'] = i;
      plot_data[i]['volume'] = data.volume[i] || 0;
      plot_data[i]['efficiency'] = data.efficiency[i] || 0;
      totalShots += data.volume[i] || 0;
      maxShots = (data.volume[i] || 0) > maxShots ? (data.volume[i] || 0) : maxShots;
    }

    const volumeBars = this.svg.selectAll(`.volumeBar-${type}`).data(plot_data, d => d.time);

    volumeBars
      .enter()
      .append('rect')
      .classed(`volumeBar-${type}`, true)
      .attr('x', d => this.x(d.time))
      .attr('width', this.dimensions.chartWidth / 26)
      .attr('y', d => this.volY(0))
      .attr('height', d => 0)
      .style('opacity', opacity)
      .transition().duration(2000)
      .attr('y', d => this.volY(d.volume / totalShots * 100) || 0)
      .attr('height', d => this.volY(0) - this.volY(d.volume / totalShots * 100) || 0)
      .style('fill', color1);

    volumeBars
      .transition().duration(2000)
      .attr('y', d => this.volY(d.volume / totalShots * 100) || 0)
      .attr('height', d => this.volY(0) - this.volY(d.volume / totalShots * 100) || 0)
      .style('fill', color1);

    const effDots = this.svg.selectAll(`.effDot-${type}`).data(plot_data, d => d.time);

    effDots
      .enter()
      .append('circle')
      .classed(`effDot-${type}`, true)
      .attr('cx', (d, i) => this.x(d.time + 0.5))
      .attr('cy', d => this.effY(3))
      .attr('r', 0.5)
      .style('opacity', opacity)
      .transition().duration(2000)
      .attr('cy', d => this.effY(Math.max(0.8, Math.min(d.efficiency, 1.5))))
      .style('fill', color2);

    effDots
      .transition().duration(2000)
      .attr('cy', d => this.effY(Math.max(0.8, Math.min(d.efficiency, 1.5))))
      .style('fill', color2);

    //   const effLine = d3.line()
    //                     .curve(d3.curveMonotoneX)
    //                     .x(d => this.x(d.time + 0.5))
    //                     .y(d => this.effY(Math.max(0.75, Math.min(d.efficiency, 1.5))));

    //   this.svg.append('path')
    //           .datum(plot_data)
    //           .attr('d', effLine)
    //           .attr('fill', 'none')
    //           .attr('stroke', color2)
    //           .attr('stroke-width', 0.3);
  }

  getLeagueAverageData(): any {
    if (this.liveBall && this.deadBall) {
      return averages['all'];
    } else if (this.liveBall) {
      return averages['live'];
    } else {
      return averages['dead'];
    }
  }

}
