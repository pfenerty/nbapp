import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShotTimeComponent } from './shot-time.component';

describe('ShotTimeComponent', () => {
  let component: ShotTimeComponent;
  let fixture: ComponentFixture<ShotTimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShotTimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShotTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
