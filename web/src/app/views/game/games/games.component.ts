import { Component, OnInit } from '@angular/core';

import { MatIconRegistry } from '@angular/material';

import { DomSanitizer } from '@angular/platform-browser';
import { API } from 'src/app/services/api.service';
import { TeamSeason } from 'src/app/components/season-team-controls/season-team-controls.component';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})
export class GamesComponent implements OnInit {

  currentDate: Date = new Date();
  minDate = new Date(2016, 9, 1);
  maxDate = new Date(2019, 5, 10);

  displayedCols = ['home.team', 'away.team', 'game_page', 'tv', 'status'];
  games: any[] = [];
  gamesDisplayedByDate = true;

  teamSeasonSearch = new TeamSeason();

  isLoading = true;

  constructor(
    private _api: API,
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer
  ) {
    iconRegistry.addSvgIcon('next', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/next.svg'));
    iconRegistry.addSvgIcon('previous', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/previous.svg'));
    iconRegistry.addSvgIcon('tv', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/tv.svg'));
    iconRegistry.addSvgIcon('chart', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/chart.svg'));
  }

  ngOnInit() {
    this.setCurrentDate();
    this.getGames();
  }

  setCurrentDate() {
    const d = new Date();
    if (d.getHours() < 17) {
      d.setDate(d.getDate() - 1);
    }
    this.currentDate = d;
  }

  getGames(): void {
    this.isLoading = true;
    this.gamesDisplayedByDate = true;
    this._api.getGames(this.currentDate).subscribe(data => {
      this.games = data.games;
      this.isLoading = false;
    });
  }

  changeDate(days: number): void {
    const newDate = new Date(this.currentDate);
    newDate.setDate(newDate.getDate() + days);
    this.currentDate = newDate;
    this.getGames();
  }

  statusMap(game: any): string {
    switch (game.statusNum) {
      case 1: return game.startTimeEastern;
      case 2: return 'Q' + game.period.current + ' - ' + (game.period.isEndOfPeriod ? 'End' : game.clock);
      case 3: return 'Final' + (game.period.current > 4 ? ' - OT' + (game.period.current - 4).toString() : '');
    }
  }

  teamDisplay(game: any, isHome: boolean): string {
    const team = isHome ? game.hTeam : game.vTeam;
    if (game.statusNum === 1) {
      if (game.playoffs) {
        return '';
      } else {
        return ' ' + team.win + '-' + team.loss;
      }
    } else {
      return ' ' + team.score;
    }
  }

  getProperty = (obj, path) => (path.split('.').reduce((o, p) => o && o[p], obj));

  compareDates(a: Date, b: Date) {
    return a.getFullYear() === b.getFullYear() && a.getMonth() === b.getMonth() && a.getDate() === b.getDate();
  }

  updateTeamSearch(teamSeason: TeamSeason) {
    this.teamSeasonSearch = teamSeason;
  }

  search() {
    this.isLoading = true;
    this.gamesDisplayedByDate = false;
    this._api.searchGames(this.teamSeasonSearch).subscribe(data => {
      this.games = data;
      this.isLoading = false;
    });
  }

}
