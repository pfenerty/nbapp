import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GameDetails } from 'src/app/models/data/game-details.model';
import { API } from 'src/app/services/api.service';
import { Team } from 'src/app/models/data/team.model';

@Component({
  selector: 'app-game-detail',
  templateUrl: './game-detail.component.html',
  styleUrls: ['./game-detail.component.css']
})
export class GameDetailComponent implements OnInit {

  home: Team = new Team();
  away: Team = new Team();
  gameId: string;

  game: GameDetails;

  isLoading = true;

  constructor(
    private route: ActivatedRoute,
    private _api: API
  ) { }

  ngOnInit() {
    this.getGame();
  }

  getGame(): void {
    this.isLoading = true;
    const params = this.route.snapshot.paramMap;
    this.gameId = params.get('id');
    this.home.name = params.get('hTeam');
    this.away.name = params.get('vTeam');

    this._api.getGame(this.gameId, this.home.name, this.away.name).subscribe(data => {
      this.game = data;
      this.home.score = data.scoreboard.home.score;
      this.away.score = data.scoreboard.away.score;
      this.isLoading = false;
    });
  }
}
