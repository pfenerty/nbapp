import { Component, OnInit, Input } from '@angular/core';
import { Team } from 'src/app/models/data/team.model';
import { TEAM_COLORS } from '../../../../enums';

@Component({
  selector: 'scoreboard',
  templateUrl: './scoreboard.component.html',
  styleUrls: ['./scoreboard.component.css']
})
export class ScoreboardComponent implements OnInit {

  @Input() home: Team;
  @Input() away: Team;
  @Input() time: string;
  teamColors = TEAM_COLORS;

  constructor() { }

  ngOnInit() {
  }

}
