import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { API } from 'src/app/services/api.service';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import * as d3 from 'd3';

@Component({
  selector: 'lineups',
  templateUrl: './lineups.component.html',
  styleUrls: ['./lineups.component.css']
})
export class LineupsComponent implements OnInit {
  @Input() gameId: string;
  @Input() home: string;
  @Input() away: string;

  data: Array<any>;
  dataSource: MatTableDataSource<any> = new MatTableDataSource();

  team: string = "both";
  players: Array<string> = [];
  player1: string;
  player2: string;
  player3: string;
  player4: string;
  player5: string;

  @ViewChild(MatSort) sort: MatSort;
  displayedColumns: string[] = ['player1', 'player2', 'player3', 'player4', 'player5', 'team', 'def_possessions', 'def_points', 'off_possessions', 'off_points', 'diff']

  constructor(private _api: API) { }

  diffColorScale = d3.scaleLinear().domain([-20, 20]).range(['#0000fc', '#fc0000']);

  ngOnInit() {
    this._api.getLineups(this.gameId, this.home, this.away).subscribe(
      data => {
        const formattedData = data.map(d => {
          let f = d;
          f['player1'] = d.lineup[0];
          f['player2'] = d.lineup[1];
          f['player3'] = d.lineup[2];
          f['player4'] = d.lineup[3];
          f['player5'] = d.lineup[4];
          return f;
        });
        this.data = formattedData;
        this.dataSource.data = formattedData;
        this.dataSource.sort = this.sort;
      }
    )
  }

  teamChange(event: any) {
    if (event.value === "both") {
      this.dataSource.data = this.data;
      this.players = [];
    } else {
      this.dataSource.data = this.data.filter(d => d.team === event.value);
      this.players = this.getPlayers(this.dataSource.data);
    }
  }

  getPlayers(events: Array<any>) {
    const players = []
    events.forEach(e => {
      players.push(e.player1);
      players.push(e.player2);
      players.push(e.player3);
      players.push(e.player4);
      players.push(e.player5);
    });
    return Array.from(new Set(players));
  }

  playerChange() {
    this.dataSource.data = this.data.filter(d => d.team === this.team);
    this.dataSource.data = this.filterByPlayer(this.dataSource.data, this.player1);
    this.dataSource.data = this.filterByPlayer(this.dataSource.data, this.player2);
    this.dataSource.data = this.filterByPlayer(this.dataSource.data, this.player3);
    this.dataSource.data = this.filterByPlayer(this.dataSource.data, this.player4);
    this.dataSource.data = this.filterByPlayer(this.dataSource.data, this.player5);
  }

  filterByPlayer(data, player) {
    if (player != null) {
      return data.filter(d => [d.player1, d.player2, d.player3, d.player4, d.player5].includes(player));
    } else {
      return data;
    }
  }

}
