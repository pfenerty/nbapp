import { Component, OnInit, Input, ViewChildren, QueryList } from '@angular/core';
import { TeamSeason } from 'src/app/components/season-team-controls/season-team-controls.component';
import { API } from 'src/app/services/api.service';
import { PlayType } from 'src/app/models/data/play-type.model';
import { PlayerSeason } from 'src/app/models/util/player-season.model';
import { PlayTypeChartComponent } from './play-type-chart/play-type-chart.component';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { TEAM_COLORS } from 'src/app/enums';
import * as d3 from 'd3';

export class PlayTypePoint {
  player: PlayerSeason;
  color: string;

  constructor(player: PlayerSeason, color: string) {
    this.player = player;
    this.color = color;
  }

  equals(b: PlayTypePoint) {
    return this.player.equals(b.player);
  }
}

@Component({
  selector: 'play-type',
  templateUrl: './play-type.component.html',
  styleUrls: ['./play-type.component.css']
})
export class PlayTypeComponent implements OnInit {

  @ViewChildren(PlayTypeChartComponent) charts: QueryList<PlayTypeChartComponent>;

  public teamSeason: TeamSeason = new TeamSeason();
  public playerPoints: Array<PlayTypePoint> = [];
  public playTypeData: Array<PlayType>;

  public playTypes = ['Transition', 'Isolation', 'PRBallHandler', 'PRRollman', 'Postup', 'Spotup', 'Handoff', 'Cut',
    'OffScreen', 'OffRebound'];

  private unusedColors: Array<string> = [ '#1b70fc', '#faff16', '#d50527', '#f898fd', '#24c9d7', '#cb9b64',
  '#866888', '#22e67a', '#e509ae', '#9dabfa', '#437e8a', '#b21bff', '#ff7b91', '#94aa05',
  '#ac5906', '#82a68d', '#fe6616', '#7a7352', '#f9bc0f', '#b65d66', '#07a2e6', '#c091ae',
  '#8a91a7', '#88fc07', '#ea42fe', '#9e8010', '#10b437', '#c281fe', '#f92b75', '#07c99d',
  '#a946aa', '#bfd544', '#16977e', '#ff6ac8', '#a88178', '#5776a9', '#678007', '#fa9316',
  '#85c070', '#6aa2a9', '#989e5d', '#fe9169', '#cd714a', '#6ed014', '#c5639c', '#c23271',
  '#698ffc', '#678275', '#c5a121', '#a978ba', '#ee534e', '#d24506', '#59c3fa', '#ca7b0a',
  '#6f7385', '#9a634a', '#48aa6f', '#ad9ad0', '#d7908c', '#6a8a53', '#8c46fc', '#8f5ab8',
  '#fd1105', '#7ea7cf', '#d77cd1', '#a9804b', '#0688b4', '#6a9f3e', '#ee8fba', '#a67389',
  '#9e8cfe', '#bd443c', '#6d63ff', '#d110d5', '#798cc3', '#df5f83', '#b1b853', '#bb59d8',
  '#1d960c', '#867ba8', '#18acc9', '#25b3a7', '#f3db1d', '#938c6d', '#936a24', '#a964fb',
  '#92e460', '#a05787', '#9c87a0', '#20c773', '#8b696d', '#78762d', '#e154c6', '#40835f',
  '#d73656', '#1afd5c', '#c4f546', '#3d88d8', '#bd3896', '#1397a3', '#f940a5', '#66aeff',
  '#d097e7', '#fe6ef9', '#d86507', '#8b900a', '#d47270', '#e8ac48', '#cf7c97', '#cebb11',
  '#718a90', '#e78139', '#ff7463', '#bea1fd'];

  constructor(
    private _api: API,
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer
  ) {
    iconRegistry.addSvgIcon('remove', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/remove.svg'));
  }

  ngOnInit() {
    this._api.getAllPlayTypeData()
      .subscribe((response: Array<PlayType>) => this.playTypeData = response);
  }

  updateTeamSeason(ts: TeamSeason): void {
    this.teamSeason = ts;
  }

  addPlayer(): void {
    const playerSeason: PlayerSeason = new PlayerSeason(this.teamSeason.player, this.teamSeason.year, this.teamSeason.team);
    const color: string = this.unusedColors.shift();
    const playTypePoint: PlayTypePoint = new PlayTypePoint(playerSeason, color);
    if (!this.playerAlreadySelected(playTypePoint)) {
      this.playerPoints.push(playTypePoint);
      this.updateCharts();
    }
  }

  removePlayer(player: PlayTypePoint): void {
    this.playerPoints = this.playerPoints.filter((p: PlayTypePoint) => !p.equals(player));
    this.unusedColors.unshift(player.color);
    this.updateCharts();
  }

  updateCharts(): void {
    this.charts.forEach((chart: PlayTypeChartComponent) => {
      chart.updatePlayers(this.playerPoints);
    });
  }

  getDataByPlayType(playType: string) {
    return this.playTypeData.filter((pt: PlayType) => pt.play_type === playType);
  }

  private playerAlreadySelected(playTypePoint: PlayTypePoint): boolean {
    for (let i = 0; i < this.playerPoints.length; i++) {
      if (playTypePoint.equals(this.playerPoints[i])) {
        return true;
      }
    }
    return false;
  }

}
