import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayTypeChartComponent } from './play-type-chart.component';

describe('PlayTypeChartComponent', () => {
  let component: PlayTypeChartComponent;
  let fixture: ComponentFixture<PlayTypeChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayTypeChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayTypeChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
