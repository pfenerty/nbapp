import { Component, OnInit, AfterViewInit, ElementRef, Input } from '@angular/core';
import * as d3 from 'd3';
import { Dimensions, Margins } from 'src/app/models/util/dimensions';
import { PlayType } from 'src/app/models/data/play-type.model';
import { PlayerSeason } from 'src/app/models/util/player-season.model';
import { PlayTypePoint } from '../play-type.component';

@Component({
  selector: 'play-type-chart',
  templateUrl: './play-type-chart.component.html',
  styleUrls: ['./play-type-chart.component.css']
})
export class PlayTypeChartComponent implements OnInit, AfterViewInit {

  @Input() playType: string;
  @Input() showXAxis: boolean;
  @Input() showYAxis: boolean;

  @Input() data: Array<PlayType>;

  private host: any;
  private svg: any;
  private dimensions: Dimensions = new Dimensions(200, 200, new Margins(15, 15, 15, 15));

  private xAxis: any;
  private yAxis: any;

  private xAverage: number;
  private yAverage: number;

  private allOpacity = 0.05;
  private allRadius = 2;
  private allFill = 'black';
  private allStroke = 'none';
  private allStrokeWidth = 0.7;
  private focusedOpacity = 1;
  private focusedRadius = 3.5;
  private focusedStroke = 'black';
  public colorScale: any = d3.scaleOrdinal(d3.schemeSet1);

  constructor(private element: ElementRef) {
    this.host = d3.select(this.element.nativeElement);
  }

  ngOnInit() {
    this.data = this.data.filter((d: PlayType) => d.poss >= 10);
  }

  ngAfterViewInit() {
    this.initializeChart();
    this.plotAverages();
    this.plotAllData();
  }

  initializeChart() {
    this.svg = this.host.append('svg')
      .attr('viewBox', '0, 0, ' + this.dimensions.width + ', ' + this.dimensions.height)
      .attr('width', '100%')
      .attr('height', '90%');

    this.xAxis = d3.scaleLinear().range(this.dimensions.xAxisRange);
    this.xAxis.domain([0, 60]);

    this.yAxis = d3.scaleLinear().range(this.dimensions.yAxisRange);
    this.yAxis.domain([2, 0]);

    if (this.showXAxis) {
      this.svg.append('g')
        .style('font-size', '7px')
        .call(d3.axisRight(this.yAxis).tickSize(0).ticks(3));
    }

    if (this.showYAxis) {
      this.svg.append('g')
        .style('font-size', '7px')
        .call(d3.axisBottom(this.xAxis).tickSize(0).ticks(3).tickFormat(d => String(d) + '%'))
        .attr('transform', `translate(0, ${this.dimensions.height - 12})`);
    }
  }

  plotAllData() {
    const dataPoints = this.svg.selectAll('.dataPoint').data(this.data);

    dataPoints
      .enter()
      .append('circle')
      .classed('dataPoint', true)
      .attr('cx', (d: PlayType) => this.xAxis(d.poss_pct * 100))
      .attr('cy', (d: PlayType) => this.yAxis(d.ppp))
      .attr('r', this.allRadius)
      .style('opacity', this.allOpacity)
      .style('fill', this.allFill)
      .style('stroke-width', this.allStrokeWidth);
  }

  plotAverages() {
    this.xAverage = this.data.map((d: PlayType) => d.poss_pct).reduce((a: number, b: number) => a + b) / this.data.length;
    this.yAverage = this.data.map((d: PlayType) => d.ppp).reduce((a: number, b: number) => a + b) / this.data.length;

    this.svg.append('line')
      .attr('y1', this.dimensions.yAxisRange[0])
      .attr('y2', this.dimensions.yAxisRange[1])
      .attr('x1', this.xAxis(this.xAverage * 100))
      .attr('x2', this.xAxis(this.xAverage * 100))
      .attr('stroke-opacity', 1)
      .style('stroke-width', 0.2)
      .style('stroke', 'black')
      .style('fill', 'none')
      .style('stroke-dasharray', 3);

    this.svg.append('line')
      .attr('x1', this.dimensions.xAxisRange[0])
      .attr('x2', this.dimensions.xAxisRange[1])
      .attr('y1', this.yAxis(this.yAverage))
      .attr('y2', this.yAxis(this.yAverage))
      .attr('stroke-opacity', 1)
      .style('stroke-width', 0.2)
      .style('stroke', 'black')
      .style('fill', 'none')
      .style('stroke-dasharray', 3);
  }

  updatePlayers(playerList: Array<PlayTypePoint>) {
    const dataPoints = this.svg.selectAll('.dataPoint').data(this.data);

    dataPoints
      .transition().duration(2000)
      .style('fill', (d: PlayType) => this.isFocused(playerList, d) ? this.getColor(playerList, d) : this.allFill)
      .style('opacity', (d: PlayType) => this.isFocused(playerList, d) ? this.focusedOpacity : this.allOpacity)
      .style('filter', (d: PlayType) => this.isFocused(playerList, d) ? 'brightness(130%)' : '')
      .style('stroke', (d: PlayType) => this.isFocused(playerList, d) ? this.focusedStroke : this.allStroke)
      .attr('r', (d: PlayType) => this.isFocused(playerList, d) ? this.focusedRadius : this.allRadius);
  }

  private isFocused(playerList: Array<PlayTypePoint>, dataPoint: PlayType): boolean {
    for (let i = 0; i < playerList.length; i++) {
      if (this.playTypeEqualsPoint(dataPoint, playerList[i])) {
        return true;
      }
    }
    return false;
  }

  private getColor(playerList: Array<PlayTypePoint>, dataPoint: PlayType): string {
    return playerList.find((p: PlayTypePoint) => this.playTypeEqualsPoint(dataPoint, p)).color;
  }

  private playTypeEqualsPoint(playType: PlayType, point: PlayTypePoint): boolean {
    return point.player.player === playType.player_name &&
    point.player.year === playType.season &&
    point.player.team === playType.team;
  }

}
