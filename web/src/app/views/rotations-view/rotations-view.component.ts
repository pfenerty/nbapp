import { Component, OnInit, ViewChild } from '@angular/core';
import { Rotation } from 'src/app/models/data/rotation.model';
import { API } from 'src/app/services/api.service';
import { MatIconRegistry, MatSelect, MatOption } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { SeasonRotationsViz2Component } from 'src/app/components/viz/rotations/season-rotations-viz2/season-rotations-viz2.component';
import { TeamSeason } from 'src/app/components/season-team-controls/season-team-controls.component';


export class Starters {
  players: Array<string>;
  games: Array<number>;

  constructor(players: Array<string>, games: Array<number>) {
    this.players = players;
    this.games = games;
  }
}


@Component({
  selector: 'app-rotations-view',
  templateUrl: './rotations-view.component.html',
  styleUrls: ['./rotations-view.component.css']
})
export class RotationsViewComponent implements OnInit {

  private data: Array<Rotation>;

  teamSeason: TeamSeason = new TeamSeason();
  startersList: Array<Starters> = [];
  selectedStarters: Array<Starters> = [];

  @ViewChild('chart') private rotationsComponent: SeasonRotationsViz2Component;
  @ViewChild('selectAllStarters') private selectAllStarters: MatOption;
  @ViewChild('startersSelect') private startersSelect: MatSelect;

  constructor(
    private _api: API,
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer
  ) {
    iconRegistry.addSvgIcon('cancel', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/cancel.svg'));
  }

  ngOnInit() { }

  getData(): void {
    this._api.getRotations(
      this.teamSeason.year, this.teamSeason.type, this.teamSeason.team,
      [].concat.apply([],
        this.selectedStarters
            .filter((s: Starters) => s !== null && s !== undefined)
            .map((s: Starters) => s.games)
      )
    ).subscribe(
      (rotations: Array<Rotation>) => {
        this.data = rotations;
        this.rotationsComponent.init(rotations);
      });
  }

  updateStarters(ts: TeamSeason): void {
    this.teamSeason = ts;
    if (ts.team != null) {
      this._api.getStartingLineups(
        ts.year, ts.type, ts.team
      ).subscribe(
        (starters: Array<Starters>) => {
          this.startersList = starters.sort(
            (a: Starters, b: Starters) => b.games.length - a.games.length
          );
          this.selectedStarters = [];
        }
      );
    }
  }

  displayStarters(starters: Starters): string {
    return `${starters.players.join(', ')} - ${starters.games.length} Games`;
  }

  toggleAllStarters(): void {
    this.startersSelect.options.forEach(
      (option: MatOption) => this.selectAllStarters.selected ? option.select() : option.deselect()
    );
  }

  getStarterSelectDisplay(): string {
    if (this.selectedStarters === undefined || this.selectAllStarters === null || this.selectedStarters.length === 0) {
      return 'No Starting Lineups Selected';
    } else if (this.selectedStarters.length === 1) {
      return this.displayStarters(this.selectedStarters[0]);
    } else if (this.selectAllStarters.selected) {
      return 'All Starting Lineups Selected';
    } else {
      const games = this.selectedStarters
                        .map((starters: Starters) => starters.games.length)
                        .reduce((a: number, b: number) => a + b);
      return `${this.selectedStarters.length} Starting Lineups Selected - ${games} Games`;
    }
  }

}
