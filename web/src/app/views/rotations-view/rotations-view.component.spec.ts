import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RotationsViewComponent } from './rotations-view.component';

describe('RotationsViewComponent', () => {
  let component: RotationsViewComponent;
  let fixture: ComponentFixture<RotationsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RotationsViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RotationsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
