import { Component, OnInit } from '@angular/core';
import { plotConsistency, clearChart } from 'src/app/d3/consistnecy';
import { MenuService } from 'src/app/services/menu.service';
import { API } from 'src/app/services/api.service';

@Component({
  selector: 'consistency',
  templateUrl: './consistency.component.html',
  styleUrls: ['./consistency.component.css']
})
export class ConsistencyComponent implements OnInit {

  private _menuData: any;

  isTeam = false;
  specific = false;

  seasons: Array<number>;
  teams: Array<string>;
  players: any;

  numPlayerOptions = [5, 10, 15, 20];
  statOptions = [
    { 'display': 'Points', 'path': 'pts' },
    { 'display': 'Assists', 'path': 'ast' },
    { 'display': 'Assist Points', 'path': 'ast_pts' },
    { 'display': 'Rebounds', 'path': 'reb' },
    { 'display': 'Offensive Rebounds', 'path': 'oreb' },
    { 'display': 'Defensive Rebounds', 'path': 'dreb' },
    { 'display': 'Turnovers', 'path': 'tov' },
    { 'display': 'Steals', 'path': 'stl' },
    { 'display': 'Blocks', 'path': 'blk' },
    { 'display': '3 Point Attempts', 'path': 'fg3a' },
    { 'display': '3 Point Makes', 'path': 'fg3m' },
    // {'display': '3 Point Percentage', 'path': 'fg3_pct'},
    { 'display': '2 Point Attempts', 'path': 'fg2a' },
    { 'display': '2 Point Makes', 'path': 'fg2m' },
    // {'display': '2 Point Percentage', 'path': 'fg2_pct'},
    { 'display': 'Free Throw Attempts', 'path': 'fta' },
    { 'display': 'Free Throw Makes', 'path': 'ftm' },
    // {'display': 'Free Throw Percentage', 'path': 'ft_pct'},
    // {'display': '2 Point Fouls Drawn', 'path': 'ft2a'},
    // {'display': '3 Point Fouls Drawn', 'path': 'ft3a'},
    { 'display': 'Possessions Played', 'path': 'poss' },
    { 'display': 'Minutes Played', 'path': 'min' },
    { 'display': 'Plus Minus', 'path': 'plus_minus' },
    { 'display': 'Offensive Rating', 'path': 'ortg' },
    { 'display': 'Defensive Rating', 'path': 'drtg' },
    { 'display': 'Net Rating', 'path': 'net_rtg' }
  ];

  isLoading = true;
  disableSeasonType = true;

  season = 2018;
  seasonType = 'Regular Season'
  team = '';
  numPlayers = 10;
  stat = this.statOptions[0];

  constructor(
    private _api: API
  ) { }

  ngOnInit() {
    // this.getData();
  }

  getData() {
    clearChart();
    this.isLoading = true;
    this._api.getPlayerConsistency(
      this.season, this.seasonType, this.isTeam ? '' : this.team,
      this.numPlayers, this.stat.path, this.isTeam)
      .subscribe(data => {
        plotConsistency(data);
        this.isLoading = false;
      });
  }

  getPlayersForTeam(team: string) {
    return this._menuData[this.season][this.team];
  }

  updateSeason() {
    this.disableSeasonType = this.season === 2018;
    if (this.season === 2018) { this.seasonType = 'Regular Season'; }
    this.players = this._menuData[this.season].map(x => x.value());
  }

}
