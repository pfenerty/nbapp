import { Component, OnInit, ViewChild } from '@angular/core';
import { API } from 'src/app/services/api.service';
import { MenuService } from 'src/app/services/menu.service';
import { GameEvent } from 'src/app/models/data/game-event.model';
import { TeamSeason } from 'src/app/components/season-team-controls/season-team-controls.component';
import { ScatterShotChartComponent } from 'src/app/components/viz/shots/scatter-shot-chart/scatter-shot-chart.component';
import { ShotChartSettings } from 'src/app/components/viz/shots/game-shot-settings/game-shot-settings.component';

@Component({
  selector: 'shots',
  templateUrl: './shots.component.html',
  styleUrls: ['./shots.component.css']
})
export class ShotsComponent implements OnInit {

  @ViewChild('chart') chartComponent: ScatterShotChartComponent;

  private _menuData: any;
  teamSeason: TeamSeason = new TeamSeason();
  player: string;

  events: Array<GameEvent> = [new GameEvent()];

  constructor(
    private _api: API,
    private _menuService: MenuService
  ) { }

  ngOnInit() { }

  updateDropDowns(ts: TeamSeason) {
    this.teamSeason = ts;
  }

  getShots() {
    this._api.getShots(
      this.teamSeason.year,
      this.teamSeason.type,
      this.teamSeason.team,
      this.player
    ).subscribe(d => {
      const settings = new ShotChartSettings(this.player, false, true, false, false, false, 'view');
      this.chartComponent.update(d, settings);
    });
  }

}
